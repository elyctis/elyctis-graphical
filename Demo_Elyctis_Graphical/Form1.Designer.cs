﻿namespace Demo_Elyctis_Graphical
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer_main = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel_mainApp = new System.Windows.Forms.TableLayoutPanel();
            this.panel_program = new System.Windows.Forms.Panel();
            this.button_save = new System.Windows.Forms.Button();
            this.groupBox_other = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.bt_oth_derivate = new System.Windows.Forms.Button();
            this.groupBox_mix = new System.Windows.Forms.GroupBox();
            this.bt_mix_adaptiveThresholding = new System.Windows.Forms.Button();
            this.bt_mix_adaptiveThresholdingI = new System.Windows.Forms.Button();
            this.bt_mix_backgroundCor = new System.Windows.Forms.Button();
            this.bt_mix_contrastScaling = new System.Windows.Forms.Button();
            this.bt_mix_getBytes = new System.Windows.Forms.Button();
            this.label_imgOutput = new System.Windows.Forms.Label();
            this.label_imgInput = new System.Windows.Forms.Label();
            this.groupBox_gpu = new System.Windows.Forms.GroupBox();
            this.bt_gpu_adaptiveThresholding = new System.Windows.Forms.Button();
            this.bt_gpu_adaptiveThresholdingI = new System.Windows.Forms.Button();
            this.bt_gpu_getBytes = new System.Windows.Forms.Button();
            this.bt_gpu_backgroundCor = new System.Windows.Forms.Button();
            this.bt_gpu_contrastScaling = new System.Windows.Forms.Button();
            this.groupBox_cpu = new System.Windows.Forms.GroupBox();
            this.bt_cpu_adaptiveThresholding = new System.Windows.Forms.Button();
            this.bt_cpu_backgroundCor = new System.Windows.Forms.Button();
            this.bt_cpu_contrastScaling = new System.Windows.Forms.Button();
            this.bt_cpu_getBytes = new System.Windows.Forms.Button();
            this.pictureBox_output = new System.Windows.Forms.PictureBox();
            this.pictureBox_input = new System.Windows.Forms.PictureBox();
            this.button_reCompile = new System.Windows.Forms.Button();
            this.richTextBox_log = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer_main)).BeginInit();
            this.splitContainer_main.Panel1.SuspendLayout();
            this.splitContainer_main.Panel2.SuspendLayout();
            this.splitContainer_main.SuspendLayout();
            this.tableLayoutPanel_mainApp.SuspendLayout();
            this.panel_program.SuspendLayout();
            this.groupBox_other.SuspendLayout();
            this.groupBox_mix.SuspendLayout();
            this.groupBox_gpu.SuspendLayout();
            this.groupBox_cpu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_output)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_input)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer_main
            // 
            this.splitContainer_main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer_main.Location = new System.Drawing.Point(0, 0);
            this.splitContainer_main.Name = "splitContainer_main";
            this.splitContainer_main.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer_main.Panel1
            // 
            this.splitContainer_main.Panel1.Controls.Add(this.tableLayoutPanel_mainApp);
            // 
            // splitContainer_main.Panel2
            // 
            this.splitContainer_main.Panel2.Controls.Add(this.richTextBox_log);
            this.splitContainer_main.Size = new System.Drawing.Size(989, 610);
            this.splitContainer_main.SplitterDistance = 486;
            this.splitContainer_main.TabIndex = 2;
            // 
            // tableLayoutPanel_mainApp
            // 
            this.tableLayoutPanel_mainApp.ColumnCount = 3;
            this.tableLayoutPanel_mainApp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_mainApp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 880F));
            this.tableLayoutPanel_mainApp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_mainApp.Controls.Add(this.panel_program, 1, 0);
            this.tableLayoutPanel_mainApp.Controls.Add(this.button_reCompile, 2, 1);
            this.tableLayoutPanel_mainApp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel_mainApp.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel_mainApp.Name = "tableLayoutPanel_mainApp";
            this.tableLayoutPanel_mainApp.RowCount = 2;
            this.tableLayoutPanel_mainApp.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 450F));
            this.tableLayoutPanel_mainApp.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel_mainApp.Size = new System.Drawing.Size(989, 486);
            this.tableLayoutPanel_mainApp.TabIndex = 2;
            // 
            // panel_program
            // 
            this.panel_program.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_program.Controls.Add(this.button_save);
            this.panel_program.Controls.Add(this.groupBox_other);
            this.panel_program.Controls.Add(this.groupBox_mix);
            this.panel_program.Controls.Add(this.label_imgOutput);
            this.panel_program.Controls.Add(this.label_imgInput);
            this.panel_program.Controls.Add(this.groupBox_gpu);
            this.panel_program.Controls.Add(this.groupBox_cpu);
            this.panel_program.Controls.Add(this.pictureBox_output);
            this.panel_program.Controls.Add(this.pictureBox_input);
            this.panel_program.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_program.Location = new System.Drawing.Point(57, 3);
            this.panel_program.Name = "panel_program";
            this.panel_program.Size = new System.Drawing.Size(874, 444);
            this.panel_program.TabIndex = 0;
            // 
            // button_save
            // 
            this.button_save.Location = new System.Drawing.Point(773, 205);
            this.button_save.Name = "button_save";
            this.button_save.Size = new System.Drawing.Size(75, 23);
            this.button_save.TabIndex = 8;
            this.button_save.Text = "Save";
            this.button_save.UseVisualStyleBackColor = true;
            this.button_save.Click += new System.EventHandler(this.button_save_Click);
            // 
            // groupBox_other
            // 
            this.groupBox_other.Controls.Add(this.button1);
            this.groupBox_other.Controls.Add(this.bt_oth_derivate);
            this.groupBox_other.Location = new System.Drawing.Point(594, 235);
            this.groupBox_other.Name = "groupBox_other";
            this.groupBox_other.Size = new System.Drawing.Size(181, 204);
            this.groupBox_other.TabIndex = 7;
            this.groupBox_other.TabStop = false;
            this.groupBox_other.Text = "Other methods";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(100, 134);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 65);
            this.button1.TabIndex = 10;
            this.button1.Text = "debug test";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button_debugTest_Click);
            // 
            // bt_oth_derivate
            // 
            this.bt_oth_derivate.Location = new System.Drawing.Point(6, 22);
            this.bt_oth_derivate.Name = "bt_oth_derivate";
            this.bt_oth_derivate.Size = new System.Drawing.Size(166, 31);
            this.bt_oth_derivate.TabIndex = 9;
            this.bt_oth_derivate.Text = "Derivate Image";
            this.bt_oth_derivate.UseVisualStyleBackColor = true;
            this.bt_oth_derivate.Click += new System.EventHandler(this.button_otherMethod_Click);
            // 
            // groupBox_mix
            // 
            this.groupBox_mix.Controls.Add(this.bt_mix_adaptiveThresholding);
            this.groupBox_mix.Controls.Add(this.bt_mix_adaptiveThresholdingI);
            this.groupBox_mix.Controls.Add(this.bt_mix_backgroundCor);
            this.groupBox_mix.Controls.Add(this.bt_mix_contrastScaling);
            this.groupBox_mix.Controls.Add(this.bt_mix_getBytes);
            this.groupBox_mix.Location = new System.Drawing.Point(399, 235);
            this.groupBox_mix.Name = "groupBox_mix";
            this.groupBox_mix.Size = new System.Drawing.Size(181, 206);
            this.groupBox_mix.TabIndex = 6;
            this.groupBox_mix.TabStop = false;
            this.groupBox_mix.Text = "Mix commands:";
            // 
            // bt_mix_adaptiveThresholding
            // 
            this.bt_mix_adaptiveThresholding.Location = new System.Drawing.Point(6, 168);
            this.bt_mix_adaptiveThresholding.Name = "bt_mix_adaptiveThresholding";
            this.bt_mix_adaptiveThresholding.Size = new System.Drawing.Size(167, 31);
            this.bt_mix_adaptiveThresholding.TabIndex = 8;
            this.bt_mix_adaptiveThresholding.Text = "Adaptive Thresholding";
            this.bt_mix_adaptiveThresholding.UseVisualStyleBackColor = true;
            this.bt_mix_adaptiveThresholding.Click += new System.EventHandler(this.button_mix_Click);
            // 
            // bt_mix_adaptiveThresholdingI
            // 
            this.bt_mix_adaptiveThresholdingI.Location = new System.Drawing.Point(6, 133);
            this.bt_mix_adaptiveThresholdingI.Name = "bt_mix_adaptiveThresholdingI";
            this.bt_mix_adaptiveThresholdingI.Size = new System.Drawing.Size(167, 31);
            this.bt_mix_adaptiveThresholdingI.TabIndex = 3;
            this.bt_mix_adaptiveThresholdingI.Text = "Adaptive Thresholding";
            this.bt_mix_adaptiveThresholdingI.UseVisualStyleBackColor = true;
            this.bt_mix_adaptiveThresholdingI.Click += new System.EventHandler(this.button_mix_Click);
            // 
            // bt_mix_backgroundCor
            // 
            this.bt_mix_backgroundCor.Location = new System.Drawing.Point(6, 96);
            this.bt_mix_backgroundCor.Name = "bt_mix_backgroundCor";
            this.bt_mix_backgroundCor.Size = new System.Drawing.Size(167, 31);
            this.bt_mix_backgroundCor.TabIndex = 2;
            this.bt_mix_backgroundCor.Text = "Background correction";
            this.bt_mix_backgroundCor.UseVisualStyleBackColor = true;
            this.bt_mix_backgroundCor.Click += new System.EventHandler(this.button_mix_Click);
            // 
            // bt_mix_contrastScaling
            // 
            this.bt_mix_contrastScaling.Location = new System.Drawing.Point(6, 59);
            this.bt_mix_contrastScaling.Name = "bt_mix_contrastScaling";
            this.bt_mix_contrastScaling.Size = new System.Drawing.Size(167, 31);
            this.bt_mix_contrastScaling.TabIndex = 1;
            this.bt_mix_contrastScaling.Text = "Contrast scaling";
            this.bt_mix_contrastScaling.UseVisualStyleBackColor = true;
            this.bt_mix_contrastScaling.Click += new System.EventHandler(this.button_mix_Click);
            // 
            // bt_mix_getBytes
            // 
            this.bt_mix_getBytes.Location = new System.Drawing.Point(7, 22);
            this.bt_mix_getBytes.Name = "bt_mix_getBytes";
            this.bt_mix_getBytes.Size = new System.Drawing.Size(166, 31);
            this.bt_mix_getBytes.TabIndex = 0;
            this.bt_mix_getBytes.Text = "Get Bytes";
            this.bt_mix_getBytes.UseVisualStyleBackColor = true;
            this.bt_mix_getBytes.Click += new System.EventHandler(this.button_mix_Click);
            // 
            // label_imgOutput
            // 
            this.label_imgOutput.AutoSize = true;
            this.label_imgOutput.Location = new System.Drawing.Point(417, -1);
            this.label_imgOutput.Name = "label_imgOutput";
            this.label_imgOutput.Size = new System.Drawing.Size(93, 17);
            this.label_imgOutput.TabIndex = 5;
            this.label_imgOutput.Text = "Output image";
            // 
            // label_imgInput
            // 
            this.label_imgInput.AutoSize = true;
            this.label_imgInput.Location = new System.Drawing.Point(4, -2);
            this.label_imgInput.Name = "label_imgInput";
            this.label_imgInput.Size = new System.Drawing.Size(81, 17);
            this.label_imgInput.TabIndex = 4;
            this.label_imgInput.Text = "Input image";
            // 
            // groupBox_gpu
            // 
            this.groupBox_gpu.Controls.Add(this.bt_gpu_adaptiveThresholding);
            this.groupBox_gpu.Controls.Add(this.bt_gpu_adaptiveThresholdingI);
            this.groupBox_gpu.Controls.Add(this.bt_gpu_getBytes);
            this.groupBox_gpu.Controls.Add(this.bt_gpu_backgroundCor);
            this.groupBox_gpu.Controls.Add(this.bt_gpu_contrastScaling);
            this.groupBox_gpu.Location = new System.Drawing.Point(200, 232);
            this.groupBox_gpu.Name = "groupBox_gpu";
            this.groupBox_gpu.Size = new System.Drawing.Size(184, 209);
            this.groupBox_gpu.TabIndex = 3;
            this.groupBox_gpu.TabStop = false;
            this.groupBox_gpu.Text = "GPU commands:";
            // 
            // bt_gpu_adaptiveThresholding
            // 
            this.bt_gpu_adaptiveThresholding.Location = new System.Drawing.Point(6, 171);
            this.bt_gpu_adaptiveThresholding.Name = "bt_gpu_adaptiveThresholding";
            this.bt_gpu_adaptiveThresholding.Size = new System.Drawing.Size(167, 31);
            this.bt_gpu_adaptiveThresholding.TabIndex = 7;
            this.bt_gpu_adaptiveThresholding.Text = "Adaptive Thresholding";
            this.bt_gpu_adaptiveThresholding.UseVisualStyleBackColor = true;
            this.bt_gpu_adaptiveThresholding.Click += new System.EventHandler(this.button_gpu_Click);
            // 
            // bt_gpu_adaptiveThresholdingI
            // 
            this.bt_gpu_adaptiveThresholdingI.Location = new System.Drawing.Point(6, 133);
            this.bt_gpu_adaptiveThresholdingI.Name = "bt_gpu_adaptiveThresholdingI";
            this.bt_gpu_adaptiveThresholdingI.Size = new System.Drawing.Size(172, 31);
            this.bt_gpu_adaptiveThresholdingI.TabIndex = 6;
            this.bt_gpu_adaptiveThresholdingI.Text = "Adaptive Thresholding I";
            this.bt_gpu_adaptiveThresholdingI.UseVisualStyleBackColor = true;
            this.bt_gpu_adaptiveThresholdingI.Click += new System.EventHandler(this.button_gpu_Click);
            // 
            // bt_gpu_getBytes
            // 
            this.bt_gpu_getBytes.Location = new System.Drawing.Point(6, 22);
            this.bt_gpu_getBytes.Name = "bt_gpu_getBytes";
            this.bt_gpu_getBytes.Size = new System.Drawing.Size(166, 31);
            this.bt_gpu_getBytes.TabIndex = 1;
            this.bt_gpu_getBytes.Text = "Get Bytes";
            this.bt_gpu_getBytes.UseVisualStyleBackColor = true;
            this.bt_gpu_getBytes.Click += new System.EventHandler(this.button_gpu_Click);
            // 
            // bt_gpu_backgroundCor
            // 
            this.bt_gpu_backgroundCor.Location = new System.Drawing.Point(6, 96);
            this.bt_gpu_backgroundCor.Name = "bt_gpu_backgroundCor";
            this.bt_gpu_backgroundCor.Size = new System.Drawing.Size(167, 31);
            this.bt_gpu_backgroundCor.TabIndex = 5;
            this.bt_gpu_backgroundCor.Text = "Background correction";
            this.bt_gpu_backgroundCor.UseVisualStyleBackColor = true;
            this.bt_gpu_backgroundCor.Click += new System.EventHandler(this.button_gpu_Click);
            // 
            // bt_gpu_contrastScaling
            // 
            this.bt_gpu_contrastScaling.Location = new System.Drawing.Point(6, 59);
            this.bt_gpu_contrastScaling.Name = "bt_gpu_contrastScaling";
            this.bt_gpu_contrastScaling.Size = new System.Drawing.Size(167, 31);
            this.bt_gpu_contrastScaling.TabIndex = 4;
            this.bt_gpu_contrastScaling.Text = "Contrast scaling";
            this.bt_gpu_contrastScaling.UseVisualStyleBackColor = true;
            this.bt_gpu_contrastScaling.Click += new System.EventHandler(this.button_gpu_Click);
            // 
            // groupBox_cpu
            // 
            this.groupBox_cpu.Controls.Add(this.bt_cpu_adaptiveThresholding);
            this.groupBox_cpu.Controls.Add(this.bt_cpu_backgroundCor);
            this.groupBox_cpu.Controls.Add(this.bt_cpu_contrastScaling);
            this.groupBox_cpu.Controls.Add(this.bt_cpu_getBytes);
            this.groupBox_cpu.Location = new System.Drawing.Point(6, 235);
            this.groupBox_cpu.Name = "groupBox_cpu";
            this.groupBox_cpu.Size = new System.Drawing.Size(181, 206);
            this.groupBox_cpu.TabIndex = 2;
            this.groupBox_cpu.TabStop = false;
            this.groupBox_cpu.Text = "CPU commands:";
            // 
            // bt_cpu_adaptiveThresholding
            // 
            this.bt_cpu_adaptiveThresholding.Location = new System.Drawing.Point(6, 133);
            this.bt_cpu_adaptiveThresholding.Name = "bt_cpu_adaptiveThresholding";
            this.bt_cpu_adaptiveThresholding.Size = new System.Drawing.Size(167, 31);
            this.bt_cpu_adaptiveThresholding.TabIndex = 3;
            this.bt_cpu_adaptiveThresholding.Text = "Adaptive Thresholding";
            this.bt_cpu_adaptiveThresholding.UseVisualStyleBackColor = true;
            this.bt_cpu_adaptiveThresholding.Click += new System.EventHandler(this.button_cpu_Click);
            // 
            // bt_cpu_backgroundCor
            // 
            this.bt_cpu_backgroundCor.Location = new System.Drawing.Point(6, 96);
            this.bt_cpu_backgroundCor.Name = "bt_cpu_backgroundCor";
            this.bt_cpu_backgroundCor.Size = new System.Drawing.Size(167, 31);
            this.bt_cpu_backgroundCor.TabIndex = 2;
            this.bt_cpu_backgroundCor.Text = "Background correction";
            this.bt_cpu_backgroundCor.UseVisualStyleBackColor = true;
            this.bt_cpu_backgroundCor.Click += new System.EventHandler(this.button_cpu_Click);
            // 
            // bt_cpu_contrastScaling
            // 
            this.bt_cpu_contrastScaling.Location = new System.Drawing.Point(6, 59);
            this.bt_cpu_contrastScaling.Name = "bt_cpu_contrastScaling";
            this.bt_cpu_contrastScaling.Size = new System.Drawing.Size(167, 31);
            this.bt_cpu_contrastScaling.TabIndex = 1;
            this.bt_cpu_contrastScaling.Text = "Contrast scaling";
            this.bt_cpu_contrastScaling.UseVisualStyleBackColor = true;
            this.bt_cpu_contrastScaling.Click += new System.EventHandler(this.button_cpu_Click);
            // 
            // bt_cpu_getBytes
            // 
            this.bt_cpu_getBytes.Location = new System.Drawing.Point(7, 22);
            this.bt_cpu_getBytes.Name = "bt_cpu_getBytes";
            this.bt_cpu_getBytes.Size = new System.Drawing.Size(166, 31);
            this.bt_cpu_getBytes.TabIndex = 0;
            this.bt_cpu_getBytes.Text = "Get Bytes";
            this.bt_cpu_getBytes.UseVisualStyleBackColor = true;
            this.bt_cpu_getBytes.Click += new System.EventHandler(this.button_cpu_Click);
            // 
            // pictureBox_output
            // 
            this.pictureBox_output.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_output.Location = new System.Drawing.Point(394, 19);
            this.pictureBox_output.Name = "pictureBox_output";
            this.pictureBox_output.Size = new System.Drawing.Size(372, 210);
            this.pictureBox_output.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_output.TabIndex = 1;
            this.pictureBox_output.TabStop = false;
            // 
            // pictureBox_input
            // 
            this.pictureBox_input.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_input.Location = new System.Drawing.Point(4, 19);
            this.pictureBox_input.Name = "pictureBox_input";
            this.pictureBox_input.Size = new System.Drawing.Size(375, 210);
            this.pictureBox_input.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_input.TabIndex = 0;
            this.pictureBox_input.TabStop = false;
            this.pictureBox_input.DoubleClick += new System.EventHandler(this.pictureBox_input_DoubleClick);
            // 
            // button_reCompile
            // 
            this.button_reCompile.Location = new System.Drawing.Point(937, 453);
            this.button_reCompile.Name = "button_reCompile";
            this.button_reCompile.Size = new System.Drawing.Size(49, 23);
            this.button_reCompile.TabIndex = 1;
            this.button_reCompile.Text = "re-compile";
            this.button_reCompile.UseVisualStyleBackColor = true;
            this.button_reCompile.Click += new System.EventHandler(this.button_reCompile_Click);
            // 
            // richTextBox_log
            // 
            this.richTextBox_log.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox_log.Location = new System.Drawing.Point(0, 0);
            this.richTextBox_log.Name = "richTextBox_log";
            this.richTextBox_log.Size = new System.Drawing.Size(989, 120);
            this.richTextBox_log.TabIndex = 0;
            this.richTextBox_log.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(989, 610);
            this.Controls.Add(this.splitContainer_main);
            this.Name = "Form1";
            this.Text = "Elyctis Graphical Demo ";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.splitContainer_main.Panel1.ResumeLayout(false);
            this.splitContainer_main.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer_main)).EndInit();
            this.splitContainer_main.ResumeLayout(false);
            this.tableLayoutPanel_mainApp.ResumeLayout(false);
            this.panel_program.ResumeLayout(false);
            this.panel_program.PerformLayout();
            this.groupBox_other.ResumeLayout(false);
            this.groupBox_mix.ResumeLayout(false);
            this.groupBox_gpu.ResumeLayout(false);
            this.groupBox_cpu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_output)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_input)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.SplitContainer splitContainer_main;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_mainApp;
        private System.Windows.Forms.RichTextBox richTextBox_log;
        private System.Windows.Forms.Panel panel_program;
        private System.Windows.Forms.Label label_imgOutput;
        private System.Windows.Forms.Label label_imgInput;
        private System.Windows.Forms.GroupBox groupBox_gpu;
        private System.Windows.Forms.GroupBox groupBox_cpu;
        private System.Windows.Forms.PictureBox pictureBox_output;
        private System.Windows.Forms.PictureBox pictureBox_input;
        private System.Windows.Forms.Button bt_gpu_getBytes;
        private System.Windows.Forms.Button bt_cpu_getBytes;
        private System.Windows.Forms.Button bt_cpu_backgroundCor;
        private System.Windows.Forms.Button bt_cpu_contrastScaling;
        private System.Windows.Forms.Button bt_cpu_adaptiveThresholding;
        private System.Windows.Forms.Button bt_gpu_adaptiveThresholding;
        private System.Windows.Forms.Button bt_gpu_adaptiveThresholdingI;
        private System.Windows.Forms.Button bt_gpu_backgroundCor;
        private System.Windows.Forms.Button bt_gpu_contrastScaling;
        private System.Windows.Forms.GroupBox groupBox_mix;
        private System.Windows.Forms.Button bt_mix_adaptiveThresholdingI;
        private System.Windows.Forms.Button bt_mix_backgroundCor;
        private System.Windows.Forms.Button bt_mix_contrastScaling;
        private System.Windows.Forms.Button bt_mix_getBytes;
        private System.Windows.Forms.Button bt_mix_adaptiveThresholding;
        private System.Windows.Forms.Button button_save;
        private System.Windows.Forms.GroupBox groupBox_other;
        private System.Windows.Forms.Button bt_oth_derivate;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button_reCompile;
    }
}

