﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

using Elyctis_Graphical;
using Elyctis_Graphical.CPU;
using Elyctis_Graphical.GPU;

namespace Demo_Elyctis_Graphical
{
    public partial class Form1 : Form
    {
        #region Delegate

        /// <summary>
        /// Delegate used to display a picture from its bitmap object.
        /// </summary>
        /// <param name="img">Image to display.</param>
        /// <param name="picture">Where to display.</param>
        private delegate void DelegateUpdatePictureFromBmp(Bitmap img, PICTURE picture = PICTURE.OUTPUT);

        #endregion

        #region Const

        /// <summary>
        /// Enum used to select picture type.
        /// </summary>
        private enum PICTURE
        {
            /// <summary>
            /// Input image.
            /// </summary>
            INPUT,
            /// <summary>
            /// Output image.
            /// </summary>
            OUTPUT
        }

        #endregion

        #region Field

        private int _logLevel;

        private Thread _saveImageTh;
        private Thread _compilrGpu;

        private Bitmap _inputImg;
        private Bitmap _outputImg;

        private CPU_Processing _cpuProcessing;
        private GPU_Processing _gpuProcessing;

        private EG_Utility.DelegateInitialization _delegateInitialization;
        private EG_Utility.DelegateLogData _delegateLogData;

        private DelegateUpdatePictureFromBmp _delegateUpdatePictureFromBmp;

        #endregion

        #region Constructor

        /// <summary>
        /// Standard constructor for <see cref="Form1"/>.
        /// </summary>
        public Form1()
        {
            InitializeComponent();

            _delegateInitialization = new EG_Utility.DelegateInitialization(UpdateInitialization);
            _delegateLogData = new EG_Utility.DelegateLogData(UpdateLogData);
            _delegateUpdatePictureFromBmp = new DelegateUpdatePictureFromBmp(UpdateImage);

            _saveImageTh = null;
            _compilrGpu = null;

            _inputImg = null;
            _outputImg = null;

            _logLevel = (int)EG_Utility.LOG_LEVEL.VERBOSE;

            _cpuProcessing = new CPU_Processing(_delegateLogData, _delegateInitialization);            
            _gpuProcessing = new GPU_Processing(_delegateLogData, _delegateInitialization);

            _cpuProcessing.LogLevel = _logLevel;
            _cpuProcessing.WriteConsole = false;

            _gpuProcessing.LogLevel = _logLevel;
            _gpuProcessing.WriteConsole = false;

        }

        /// <summary>
        /// Finalyzer for <see cref="Form1"/>.
        /// </summary>
        ~Form1()
        {
            if (_inputImg != null)
                _inputImg.Dispose();

            if (_outputImg != null)
                _outputImg.Dispose();
        }

        #endregion

        #region Method

        /// <summary>
        /// Delegate method used to update the displayed picture.
        /// </summary>
        /// <param name="img">Picture to display.</param>
        /// <param name="picture">Where to display picture.</param>
        private void UpdateImage(Bitmap img, PICTURE picture = PICTURE.OUTPUT)
        {
            if(img == null)
            {
                return;
            }
            else if (InvokeRequired)
            {
                Invoke(_delegateUpdatePictureFromBmp, new object[] { img, picture });
                return;
            }

            switch (picture)
            {
                case PICTURE.INPUT:
                    pictureBox_input.Image = img;
                    break;
                case PICTURE.OUTPUT:
                    pictureBox_output.Image = img;
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Delegate method that handle initialization events when triggered.
        /// </summary>
        /// <remarks>
        /// This method should be updated accordingly with the context.
        /// </remarks>
        /// <param name="name">initialization name.</param>
        /// <param name="res">Initialization result.</param>
        /// <param name="msg">Additional message.</param>
        private void UpdateInitialization(string name, bool res, string msg)
        {
            if (InvokeRequired)
            {
                Invoke(_delegateInitialization, new object[] { name, res, msg });
                return;
            }

            switch (name)
            {
                // Get the set up initialization status
                case EG_Utility.INI_GPU_SETUP:
                    if (res)
                    {
                        Thread th = new Thread(() => {
                            try
                            {
                                LogData("UpdateInitialization [INI_GPU_SETUP]", "Set up successful.", EG_Utility.LOG_LEVEL.INFO);
                                _gpuProcessing.CreatePrograms();
                            }
                            finally
                            {
                                th = null;
                            }
                        });
                        th.Start();
                    }
                    else
                    {
                        LogData("UpdateInitialization [INI_GPU_SETUP]", "Unable to initialyze the GPU processing class: " + msg, EG_Utility.LOG_LEVEL.ERROR);
                        MessageBox.Show("Unable to initialyze the GPU processing class: " + msg, "GPU class generation");
                    }

                    break;

                // Get the image formating compilation status.
                case EG_Utility.INI_GPU_IMG_FORMATING:
                    if (res)
                    {
                        LogData("UpdateInitialization [INI_GPU_IMG_FORMATING]", "IMG_FORMATING is compilled.", EG_Utility.LOG_LEVEL.INFO);
                    }
                    else
                    {
                        LogData("UpdateInitialization [INI_GPU_IMG_FORMATING]", "Failed to compile : IMG_FORMATING" + msg, EG_Utility.LOG_LEVEL.WARNING);
                    }
                    break;

                // Get the image processing compilation status.
                case EG_Utility.INI_GPU_IMG_PROCESSING:
                    if (res)
                    {
                        LogData("UpdateInitialization [INI_GPU_IMG_PROCESSING]", "IMG_PROCESSING is compilled.", EG_Utility.LOG_LEVEL.INFO);
                    }
                    else
                    {
                        LogData("UpdateInitialization [INI_GPU_IMG_PROCESSING]", "Failed to compile : IMG_PROCESSING" + msg, EG_Utility.LOG_LEVEL.WARNING);
                    }
                    break;

                // Get the image processing compilation status.
                case EG_Utility.INI_GPU_CANNY_FILTER:
                    if (res)
                    {
                        LogData("UpdateInitialization [INI_GPU_CANNY_FILTER]", "IMG_PROCESSING is compilled.", EG_Utility.LOG_LEVEL.INFO);
                    }
                    else
                    {
                        LogData("UpdateInitialization [INI_GPU_CANNY_FILTER]", "Failed to compile : IMG_PROCESSING" + msg, EG_Utility.LOG_LEVEL.WARNING);
                    }
                    break;

                // Get the initialization end data.
                case EG_Utility.INI_GPU_INITIALIZATION_END:
                    if (res)
                    {
                        this.groupBox_gpu.Enabled = true;
                        this.groupBox_mix.Enabled = true;
                        this.groupBox_other.Enabled = true;
                        LogData("UpdateInitialization [INI_GPU_INITIALIZATION_END]", "GPU processing class is initialized.", EG_Utility.LOG_LEVEL.INFO);                        
                    }
                    else
                    {
                        LogData("UpdateInitialization [INI_GPU_INITIALIZATION_END]", "Unable to initialyze the GPU processing class.", EG_Utility.LOG_LEVEL.ERROR);
                        MessageBox.Show("Unable to initialyze the GPU processing class.", "GPU class generation");
                    }
                    break;
            }
        }

        /// <summary>
        /// Delegate method that handle loging event.
        /// <para>
        /// Level: [c] => 0 | [e] => 1 | [w] => 2 | [i] => 3 | [v] => 4 | [d] => 5 | [n] => -1.
        /// (see: <see cref="LOG_LEVEL"/>).
        /// </para>
        /// </summary>
        /// <remarks>
        /// This method should be updated accordingly with the context.
        /// </remarks>
        /// <param name="className">Which class write the message.</param>
        /// <param name="methodName">Which method write the message.</param>
        /// <param name="data">Message to write.</param>
        /// <param name="level">Log level.</param>
        private void UpdateLogData(string className, string methodName, string data, int level)
        {
            if (InvokeRequired)
            {
                Invoke(_delegateLogData, new object[] { className, methodName, data, level });
                return;
            }

            string text = string.Format("{0} [{1}] : {2} {3}\n", DateTime.Now, ((EG_Utility.LOG_LEVEL)level).ToString().Substring(0, 1), methodName, data);

            switch (level)
            {
                case (int)EG_Utility.LOG_LEVEL.CRITICAL:
                    richTextBox_log.SelectionColor = Color.Red;
                    break;
                case (int)EG_Utility.LOG_LEVEL.ERROR:
                    richTextBox_log.SelectionColor = Color.Red;
                    break;
                case (int)EG_Utility.LOG_LEVEL.WARNING:
                    richTextBox_log.SelectionColor = Color.Goldenrod;
                    break;
                case (int)EG_Utility.LOG_LEVEL.INFO:
                    richTextBox_log.SelectionColor = Color.Green;
                    break;
                case (int)EG_Utility.LOG_LEVEL.VERBOSE:
                    richTextBox_log.SelectionColor = Color.Blue;
                    break;
                case (int)EG_Utility.LOG_LEVEL.DEBUG:
                    richTextBox_log.SelectionColor = Color.Black;
                    break;
                default:
                    richTextBox_log.SelectionColor = Color.Black;
                    text = "";
                    break;
            }
            if (text != "")
            {
                richTextBox_log.AppendText(text);
                richTextBox_log.Select(richTextBox_log.Text.Length, 0);
                richTextBox_log.ScrollToCaret();
            }
                

        }

        /// <summary>
        /// Perform <see cref="UpdateLogData"/> with class name as 'Form1'.
        /// </summary>
        /// <param name="methodName">Which method write the message.</param>
        /// <param name="data">Message to write.</param>
        /// <param name="level">Log level.</param>
        private void LogData(string methodName, string data, int level)
        {
            UpdateLogData("Form1", methodName, data, level);
        }

        /// <summary>
        /// Perform <see cref="UpdateLogData"/> with class name as 'Form1'.
        /// </summary>
        /// <param name="methodName">Which method write the message.</param>
        /// <param name="data">Message to write.</param>
        /// <param name="level">Log level.</param>
        private void LogData(string methodName, string data, EG_Utility.LOG_LEVEL level)
        {
            UpdateLogData("Form1", methodName, data, (int)level);
        }
        
        /// <summary>
        /// Save the ouput image.
        /// </summary>
        private void SaveOuputImage()
        {
            if (true)
            {
                try
                {
                    using (SaveFileDialog sfd = new SaveFileDialog())
                    {

                        sfd.Filter = "Bitmap Image|*.bmp";
                        sfd.Title = "Save an Image File";
                        sfd.ShowDialog();

                        // If the file name is not an empty string open it for saving.
                        if (sfd.FileName != "")
                        {
                            // Saves the Image via a FileStream created by the OpenFile method.
                            System.IO.FileStream fs = (System.IO.FileStream)sfd.OpenFile();
                            pictureBox_output.Image.Save(fs, System.Drawing.Imaging.ImageFormat.Bmp);
                            fs.Close();
                        }
                    }
                }
                catch (Exception e)
                {
                    LogData("SaveOuputImage", "An exception occured: " + e.Message, EG_Utility.LOG_LEVEL.WARNING);
                }
            } else

            if(null == _saveImageTh)
            {
                _saveImageTh = new Thread(() =>
                {
                    try
                    {
                        using (SaveFileDialog sfd = new SaveFileDialog())
                        {

                            sfd.Filter = "Bitmap Image|*.bmp";
                            sfd.Title = "Save an Image File";
                            sfd.ShowDialog();

                            // If the file name is not an empty string open it for saving.
                            if (sfd.FileName != "")
                            {
                                // Saves the Image via a FileStream created by the OpenFile method.
                                System.IO.FileStream fs = (System.IO.FileStream)sfd.OpenFile();
                                pictureBox_output.Image.Save(fs, System.Drawing.Imaging.ImageFormat.Bmp);
                                fs.Close();
                            }
                        }
                    }
                    catch(Exception e)
                    {
                        LogData("SaveOuputImage", "An exception occured: " + e.Message, EG_Utility.LOG_LEVEL.WARNING);
                    }
                    finally
                    {
                        _saveImageTh = null;
                    }
                });

                _saveImageTh.Start();
            }
            else
            {
                LogData("SaveOuputImage", "The image saving thread is already running.", EG_Utility.LOG_LEVEL.VERBOSE);
            }
        }

        #endregion

        #region Event Handler

        /// <summary>
        /// Form loading event.
        /// </summary>
        private void Form1_Load(object sender, EventArgs e)
        {
            this.Text += Application.ProductVersion;
            this.groupBox_gpu.Enabled = false;
            this.groupBox_mix.Enabled = false;
            this.groupBox_other.Enabled = false;

            // Thread is used to start the GPU initialization. It can be done synchronously but is takes litle time.
            _compilrGpu = new Thread(() =>
            {
                try
                {
                    // Perform Set up in different thread because it can take litle time to be initialized.
                    // It will trigger the method 'UpdateInitialization' with param : INI_GPU_SETUP to start compilling programs
                    _gpuProcessing.Setup();
                    //_gpuProcessing.CreatePrograms(); // Not perform there becaus it is done in method 'this.UpdateInitialization' if setup is successfull.
                }
                catch (Exception ex)
                {
                    LogData("Form1.Constrcutor", "An exception occured while performing the GPU object setup: " + ex.Message, EG_Utility.LOG_LEVEL.CRITICAL);
                }
                finally
                {
                    _compilrGpu = null;
                }
            });
            _compilrGpu.Start();
        }

        /// <summary>
        /// Double click on input picture box to select input picture.
        /// </summary>
        private void pictureBox_input_DoubleClick(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "All files (*.*)|*.*|Lensfile (*lens.jpg)|*lens.jpg|jpeg file (*.jpg)|*.jpg|Bitmap (*.bmp)|*.bmp";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    _inputImg = new Bitmap(openFileDialog.FileName);
                    UpdateImage(_inputImg, PICTURE.INPUT);
                }
                catch (Exception ex)
                {
                    LogData("pictureBox_input_DoubleClick", ex.Message, EG_Utility.LOG_LEVEL.ERROR);
                }
            }
        }

        /// <summary>
        /// Save the ouput image
        /// </summary>
        private void button_save_Click(object sender, EventArgs e)
        {
            SaveOuputImage();
        }

        /// <summary>
        /// Event method on click of a GPU buttons.
        /// </summary>
        private void button_gpu_Click(object sender, EventArgs e)
        {
            Button bt = null;
            Stopwatch sw = new Stopwatch();
            long[] times;
            byte[] data;

            if (!_gpuProcessing.IsInitialized)
                return;

            try
            {
                bt = (Button)sender;

                if(bt_gpu_getBytes == bt)
                {
                    times = new long[2];

                    // Start timmer
                    sw.Start();
                    // Get byte data from input img
                    _gpuProcessing.GetImageBytes(_inputImg, out data);
                    // Save time
                    times[0] = sw.ElapsedMilliseconds;

                    // Convert byte data to image
                    _gpuProcessing.GetBmpFromBytes(data, _inputImg.Height, _inputImg.Width, out _outputImg);
                    // Save timing
                    times[1] = sw.ElapsedMilliseconds;
                    sw.Stop();

                    LogData("[GPU] Get Bytes", string.Format("Converting BMP to bytes : {0}ms | Converting bytes to BMP : {1}ms | total : {2}ms", times[0], times[1] - times[0], times[1]), EG_Utility.LOG_LEVEL.INFO);
                    UpdateImage(_outputImg, PICTURE.OUTPUT);

                }
                else if (bt_gpu_contrastScaling == bt)
                {
                    times = new long[3];
                    // Start timmer
                    sw.Start();
                    // Get byte data from input img
                    _gpuProcessing.GetImageBytes(_inputImg, out data);
                    // Save time
                    times[0] = sw.ElapsedMilliseconds;

                    // Perform the ocntrast scaling.
                    //_gpuProcessing.ContrastScaling(data, 50, 50, out data);
                    _gpuProcessing.ContrastScaling(data, 50, 50, out data);
                    // Save time
                    times[1] = sw.ElapsedMilliseconds;

                    // Convert byte data to image
                    _gpuProcessing.GetBmpFromBytes(data, _inputImg.Height, _inputImg.Width, out _outputImg);
                    // Save timing
                    times[2] = sw.ElapsedMilliseconds;
                    sw.Stop();

                    LogData("[GPU] Get Bytes", string.Format("Converting BMP to bytes : {0}ms | Contrast Scaling : {1}ms | Converting bytes to BMP : {2}ms  | total : {3}ms", times[0], times[1] - times[0], times[2] - times[1], times[2]), EG_Utility.LOG_LEVEL.INFO);
                    UpdateImage(_outputImg, PICTURE.OUTPUT);

                }
                else if (bt_gpu_backgroundCor == bt)
                {
                    times = new long[3];
                    // Create a correction matrix equals to 0
                    byte[] cor = new byte[_inputImg.Height * _inputImg.Width];
                    for (int i = 0; i < cor.Length; i++)
                        cor[i] = 127;
                    // Start timmer
                    sw.Start();
                    // Get byte data from input img
                    _gpuProcessing.GetImageBytes(_inputImg, out data);
                    // Save time
                    times[0] = sw.ElapsedMilliseconds;

                    // Perform the background correction.
                    _gpuProcessing.BackGroundCorrection(data, cor, 127, out data);
                    // Save time
                    times[1] = sw.ElapsedMilliseconds;

                    // Convert byte data to image
                    _gpuProcessing.GetBmpFromBytes(data, _inputImg.Height, _inputImg.Width, out _outputImg);
                    // Save timing
                    times[2] = sw.ElapsedMilliseconds;
                    sw.Stop();

                    LogData("[GPU] Get Bytes", string.Format("Converting BMP to bytes : {0}ms | Background correction : {1}ms | Converting bytes to BMP : {2}ms  | total : {3}ms", times[0], times[1] - times[0], times[2] - times[1], times[2]), EG_Utility.LOG_LEVEL.INFO);
                    UpdateImage(_outputImg, PICTURE.OUTPUT);
                }
                else if (bt_gpu_adaptiveThresholdingI == bt)
                {
                    times = new long[3];
                    // Start timmer
                    sw.Start();
                    // Get byte data from input img
                    _gpuProcessing.GetImageBytes(_inputImg, out data);
                    // Save time
                    times[0] = sw.ElapsedMilliseconds;

                    // Perform the adpative thresholding with integral 
                    _gpuProcessing.AdaptiveThresholdI(data, _inputImg.Width, _inputImg.Height, 10, 10, out data);
                    // Save time
                    times[1] = sw.ElapsedMilliseconds;

                    // Convert byte data to image
                    _gpuProcessing.GetBmpFromBytes(data, _inputImg.Height, _inputImg.Width, out _outputImg);
                    // Save timing
                    times[2] = sw.ElapsedMilliseconds;
                    sw.Stop();

                    LogData("[GPU] Get Bytes", string.Format("Converting BMP to bytes : {0}ms | Adaptive thresholding (integral): {1}ms | Converting bytes to BMP : {2}ms  | total : {3}ms", times[0], times[1] - times[0], times[2] - times[1], times[2]), EG_Utility.LOG_LEVEL.INFO);
                    UpdateImage(_outputImg, PICTURE.OUTPUT);
                }
                else if (bt_gpu_adaptiveThresholding == bt)
                {
                    times = new long[3];
                    // Start timmer
                    sw.Start();
                    // Get byte data from input img
                    _gpuProcessing.GetImageBytes(_inputImg, out data);
                    // Save time
                    times[0] = sw.ElapsedMilliseconds;

                    // Perform the adpative thresholding with integral 
                    _gpuProcessing.AdaptiveThreshold(data, _inputImg.Width, _inputImg.Height, 10, 10, out data);
                    // Save tim1
                    times[1] = sw.ElapsedMilliseconds;

                    // Convert byte data to image
                    _gpuProcessing.GetBmpFromBytes(data, _inputImg.Height, _inputImg.Width, out _outputImg);
                    // Save timing
                    times[2] = sw.ElapsedMilliseconds;
                    sw.Stop();

                    LogData("[GPU] Get Bytes", string.Format("Converting BMP to bytes : {0}ms | Adaptive thresholding : {1}ms | Converting bytes to BMP : {2}ms  | total : {3}ms", times[0], times[1] - times[0], times[2] - times[1], times[2]), EG_Utility.LOG_LEVEL.INFO);
                    UpdateImage(_outputImg, PICTURE.OUTPUT);
                }
                else
                {

                }

            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Event method on click of a CPU buttons.
        /// </summary>
        private void button_cpu_Click(object sender, EventArgs e)
        {
            Button bt = null;
            Stopwatch sw = new Stopwatch();
            long[] times;
            byte[] data;

            try
            {
                bt = (Button)sender;

                if (bt_cpu_getBytes == bt)
                {
                    times = new long[2];

                    // Start timmer
                    sw.Start();
                    // Get byte data from input img
                    data = _cpuProcessing.GetImageBytes(_inputImg);
                    // Save time
                    times[0] = sw.ElapsedMilliseconds;

                    // Convert byte data to image
                    _outputImg = _cpuProcessing.GetBmpFromBytes(data, _inputImg.Width, _inputImg.Height);
                    // Save timing
                    times[1] = sw.ElapsedMilliseconds;
                    sw.Stop();

                    LogData("[CPU] Get Bytes", string.Format("Converting BMP to bytes : {0}ms | Converting bytes to BMP : {1}ms | total : {2}ms", times[0], times[1] - times[0], times[1]), EG_Utility.LOG_LEVEL.INFO);
                    UpdateImage(_outputImg, PICTURE.OUTPUT);
                }                
                else if (bt_cpu_contrastScaling == bt)
                {
                    times = new long[3];
                    // Start timmer
                    sw.Start();
                    // Get byte data from input img
                    data = _cpuProcessing.GetImageBytes(_inputImg);
                    // Save time
                    times[0] = sw.ElapsedMilliseconds;

                    // Perform the ocntrast scaling.
                    data = _cpuProcessing.ContrastScaling(data, 50, 50);
                    // Save time
                    times[1] = sw.ElapsedMilliseconds;

                    // Convert byte data to image
                    _outputImg = _cpuProcessing.GetBmpFromBytes(data, _inputImg.Width, _inputImg.Height);
                    // Save timing
                    times[2] = sw.ElapsedMilliseconds;
                    sw.Stop();

                    LogData("[CPU] Get Bytes", string.Format("Converting BMP to bytes : {0}ms | Contrast Scaling : {1}ms | Converting bytes to BMP : {2}ms  | total : {3}ms", times[0], times[1] - times[0], times[2] - times[1], times[2]), EG_Utility.LOG_LEVEL.INFO);
                    UpdateImage(_outputImg, PICTURE.OUTPUT);
                }
                else if (bt_cpu_backgroundCor == bt)
                {
                    times = new long[3];
                    // Create a correction matrix equals to 0
                    byte[] cor = new byte[_inputImg.Height * _inputImg.Width];
                    for (int i = 0; i < cor.Length; i++)
                        cor[i] = 127;
                    // Start timmer
                    sw.Start();
                    // Get byte data from input img
                    data = _cpuProcessing.GetImageBytes(_inputImg);
                    // Save time
                    times[0] = sw.ElapsedMilliseconds;

                    // Perform the ocntrast scaling.
                    data = _cpuProcessing.BackGroundCorrection(data, cor, 127);
                    // Save time
                    times[1] = sw.ElapsedMilliseconds;

                    // Convert byte data to image
                    _outputImg = _cpuProcessing.GetBmpFromBytes(data, _inputImg.Width, _inputImg.Height);
                    // Save timing
                    times[2] = sw.ElapsedMilliseconds;
                    sw.Stop();

                    LogData("[CPU] Get Bytes", string.Format("Converting BMP to bytes : {0}ms | Background correction : {1}ms | Converting bytes to BMP : {2}ms  | total : {3}ms", times[0], times[1] - times[0], times[2] - times[1], times[2]), EG_Utility.LOG_LEVEL.INFO);
                    UpdateImage(_outputImg, PICTURE.OUTPUT);
                }
                else if (bt_cpu_adaptiveThresholding == bt)
                {
                    times = new long[3];
                    // Start timmer
                    sw.Start();
                    // Get byte data from input img
                    data = _cpuProcessing.GetImageBytes(_inputImg);
                    // Save time
                    times[0] = sw.ElapsedMilliseconds;

                    // Perform the adaptive thresholding.
                    data = _cpuProcessing.AdaptiveThreshold(data, _inputImg.Width, _inputImg.Height, 10, 10);
                    // Save time
                    times[1] = sw.ElapsedMilliseconds;

                    // Convert byte data to image
                    _outputImg = _cpuProcessing.GetBmpFromBytes(data, _inputImg.Width, _inputImg.Height);
                    // Save timing
                    times[2] = sw.ElapsedMilliseconds;
                    sw.Stop();

                    LogData("[CPU] Get Bytes", string.Format("Converting BMP to bytes : {0}ms | Adaptive thresholding : {1}ms | Converting bytes to BMP : {2}ms  | total : {3}ms", times[0], times[1] - times[0], times[2] - times[1], times[2]), EG_Utility.LOG_LEVEL.INFO);
                    UpdateImage(_outputImg, PICTURE.OUTPUT);
                }
                else
                {

                }

            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Event method on click for mix GPU/CPU methods buttons.
        /// </summary>
        private void button_mix_Click(object sender, EventArgs e)
        {
            Button bt = null;
            Stopwatch sw = new Stopwatch();
            long[] times;
            byte[] data;

            if (!_gpuProcessing.IsInitialized)
                return;

            try
            {
                bt = (Button)sender;

                if (bt_mix_getBytes == bt)
                {
                    times = new long[2];

                    // Start timmer
                    sw.Start();
                    // Get byte data from input img
                    _gpuProcessing.GetImageBytes(_inputImg, out data);
                    // Save time
                    times[0] = sw.ElapsedMilliseconds;

                    // Convert byte data to image
                    _outputImg = _cpuProcessing.GetBmpFromBytes(data, _inputImg.Width, _inputImg.Height);
                    // Save timing
                    times[1] = sw.ElapsedMilliseconds;
                    sw.Stop();

                    LogData("[MIX] Get Bytes", string.Format("Converting BMP to bytes : {0}ms | Converting bytes to BMP : {1}ms | total : {2}ms", times[0], times[1] - times[0], times[1]), EG_Utility.LOG_LEVEL.INFO);
                    UpdateImage(_outputImg, PICTURE.OUTPUT);

                }
                else if (bt_mix_contrastScaling == bt)
                {
                    times = new long[3];
                    // Start timmer
                    sw.Start();
                    // Get byte data from input img
                    _gpuProcessing.GetImageBytes(_inputImg, out data);
                    // Save time
                    times[0] = sw.ElapsedMilliseconds;

                    // Perform the ocntrast scaling.
                    _gpuProcessing.ContrastScaling(data, 50, 50, out data);
                    // Save time
                    times[1] = sw.ElapsedMilliseconds;

                    // Convert byte data to image
                    _outputImg = _cpuProcessing.GetBmpFromBytes(data, _inputImg.Width, _inputImg.Height);
                    // Save timing
                    times[2] = sw.ElapsedMilliseconds;
                    sw.Stop();

                    LogData("[MIX] Get Bytes", string.Format("Converting BMP to bytes : {0}ms | Contrast Scaling : {1}ms | Converting bytes to BMP : {2}ms  | total : {3}ms", times[0], times[1] - times[0], times[2] - times[1], times[2]), EG_Utility.LOG_LEVEL.INFO);
                    UpdateImage(_outputImg, PICTURE.OUTPUT);

                }
                else if (bt_mix_backgroundCor == bt)
                {
                    times = new long[3];
                    // Create a correction matrix equals to 0
                    byte[] cor = new byte[_inputImg.Height * _inputImg.Width];
                    for (int i = 0; i < cor.Length; i++)
                        cor[i] = 127;
                    // Start timmer
                    sw.Start();
                    // Get byte data from input img
                    _gpuProcessing.GetImageBytes(_inputImg, out data);
                    // Save time
                    times[0] = sw.ElapsedMilliseconds;

                    // Perform the background correction.
                    _gpuProcessing.BackGroundCorrection(data, cor, 127, out data);
                    // Save time
                    times[1] = sw.ElapsedMilliseconds;

                    // Convert byte data to image
                    _outputImg = _cpuProcessing.GetBmpFromBytes(data, _inputImg.Width, _inputImg.Height);
                    // Save timing
                    times[2] = sw.ElapsedMilliseconds;
                    sw.Stop();

                    LogData("[MIX] Get Bytes", string.Format("Converting BMP to bytes : {0}ms | Background correction : {1}ms | Converting bytes to BMP : {2}ms  | total : {3}ms", times[0], times[1] - times[0], times[2] - times[1], times[2]), EG_Utility.LOG_LEVEL.INFO);
                    UpdateImage(_outputImg, PICTURE.OUTPUT);
                }
                else if (bt_mix_adaptiveThresholdingI == bt)
                {
                    times = new long[3];
                    // Start timmer
                    sw.Start();
                    // Get byte data from input img
                    _gpuProcessing.GetImageBytes(_inputImg, out data);
                    // Save time
                    times[0] = sw.ElapsedMilliseconds;

                    // Perform the adpative thresholding with integral 
                    _gpuProcessing.AdaptiveThresholdI(data, _inputImg.Width, _inputImg.Height, 10, 10, out data);
                    // Save time
                    times[1] = sw.ElapsedMilliseconds;

                    // Convert byte data to image
                    _outputImg = _cpuProcessing.GetBmpFromBytes(data, _inputImg.Width, _inputImg.Height);
                    // Save timing
                    times[2] = sw.ElapsedMilliseconds;
                    sw.Stop();

                    LogData("[MIX] Get Bytes", string.Format("Converting BMP to bytes : {0}ms | Adaptive thresholding (integral): {1}ms | Converting bytes to BMP : {2}ms  | total : {3}ms", times[0], times[1] - times[0], times[2] - times[1], times[2]), EG_Utility.LOG_LEVEL.INFO);
                    UpdateImage(_outputImg, PICTURE.OUTPUT);
                }
                else if (bt_mix_adaptiveThresholding == bt)
                {
                    times = new long[3];
                    // Start timmer
                    sw.Start();
                    // Get byte data from input img
                    _gpuProcessing.GetImageBytes(_inputImg, out data);
                    // Save time
                    times[0] = sw.ElapsedMilliseconds;

                    // Perform the adpative thresholding with integral 
                    _gpuProcessing.AdaptiveThreshold(data, _inputImg.Width, _inputImg.Height, 10, 10, out data);
                    // Save time
                    times[1] = sw.ElapsedMilliseconds;

                    // Convert byte data to image
                    _outputImg = _cpuProcessing.GetBmpFromBytes(data, _inputImg.Width, _inputImg.Height);
                    // Save timing
                    times[2] = sw.ElapsedMilliseconds;
                    sw.Stop();

                    LogData("[MIX] Get Bytes", string.Format("Converting BMP to bytes : {0}ms | Adaptive thresholding : {1}ms | Converting bytes to BMP : {2}ms  | total : {3}ms", times[0], times[1] - times[0], times[2] - times[1], times[2]), EG_Utility.LOG_LEVEL.INFO);
                    UpdateImage(_outputImg, PICTURE.OUTPUT);
                }
                else
                {

                }

            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Event method click for other methods.
        /// </summary>
        private void button_otherMethod_Click(object sender, EventArgs e)
        {
            Button bt = null;
            Stopwatch sw = new Stopwatch();
            long[] times;
            byte[] data;

            if (!_gpuProcessing.IsInitialized)
                return;

            try
            {
                bt = (Button)sender;

                if (bt_oth_derivate == bt)
                {
                    times = new long[1];

                    // Start timmer
                    sw.Start();
                    // Get byte data from input img
                    _gpuProcessing.DerivateImage(_inputImg, out _outputImg);
                    // Save time
                    times[0] = sw.ElapsedMilliseconds;
                    sw.Stop();

                    LogData("[MIX] Get Bytes", string.Format("Image derivation : {0}ms ", times[0]), EG_Utility.LOG_LEVEL.INFO);
                    UpdateImage(_outputImg, PICTURE.OUTPUT);

                }
                else
                {

                }

            }
            catch (Exception)
            {

            }
        }


        #endregion


        private void button_debugTest_Click(object sender, EventArgs e)
        {
            Stopwatch sw = new Stopwatch();
            long[] times;
            int[] args = new int[] { 95, 20, 0 };

            _outputImg = new Bitmap(_inputImg.Width, _inputImg.Height);


            if (!_gpuProcessing.IsInitialized)
                return;

            times = new long[4] { 0, 0, 0, 0 };
            // Start timmer
            sw.Start();
            // Convert byte data to image
            //_gpuProcessing.ConvolutionProduct_3x3(_inputImg, gausst, out _outputImg);

            //_gpuProcessing.CannyFilter_1(_inputImg, args, out _outputImg);

            //_gpuProcessing.CannyFilter(_inputImg, args, out _outputImg);
            //times[0] = sw.ElapsedMilliseconds;

            _gpuProcessing.GetImageBytes(_inputImg, out byte[] temp);
            //_gpuProcessing.MedianFilter_5_5_(temp, _inputImg.Width, _inputImg.Height, out temp);
            times[0] = sw.ElapsedMilliseconds;
            //_gpuProcessing.CannyFilter_2(temp, _inputImg.Width, _inputImg.Height, out byte[] res);
            _gpuProcessing.CannyFilter(temp, _inputImg.Width, _inputImg.Height, out byte[] res);
            //byte[] res = _cpuProcessing.MedianFileter(temp, _inputImg.Width, _inputImg.Height, 5);
            times[1] = sw.ElapsedMilliseconds;
            
            //_gpuProcessing.AdaptiveThreshold(res, _inputImg.Width, _inputImg.Height, 10, 10, out res);
            times[2] = sw.ElapsedMilliseconds;
            _outputImg = _cpuProcessing.GetBmpFromBytes(res, _inputImg.Width, _inputImg.Height);
            times[3] = sw.ElapsedMilliseconds;

            //_gpuProcessing.HilightColor(_outputImg, 5, new byte[] { 0, 5, 10, 20, 30, 40 },out _outputImg);
            //_gpuProcessing.AdaptiveThreshold(temp, _outputImg.Width, _outputImg.Height, 8, 8, out temp);
            //times[2] = sw.ElapsedMilliseconds;
            //_outputImg = _cpuProcessing.GetBmpFromBytes(temp, _outputImg.Width, _outputImg.Height);
            //times[3] = sw.ElapsedMilliseconds;
            // Save timing

            //_gpuProcessing.DerivateImage(_inputImg, out _outputImg);
            //times[1] = sw.ElapsedMilliseconds;
            sw.Stop();

            LogData("[GPU] Get Bytes", string.Format("Convonlution BMP : {0}ms | {1}ms | {2}ms | {3}ms", times[0], times[1] - times[0], times[2] - times[1], times[3] - times[2]), EG_Utility.LOG_LEVEL.INFO);
            UpdateImage(_outputImg, PICTURE.OUTPUT);
        }

        private void button_reCompile_Click(object sender, EventArgs e)
        {
            if (_compilrGpu == null)
            {
                _compilrGpu = new Thread(() =>
                    {
                        try
                        {
                    // Perform Set up in different thread because it can take litle time to be initialized.
                    // It will trigger the method 'UpdateInitialization' with param : INI_GPU_SETUP to start compilling programs
                    _gpuProcessing.Setup();
                    //_gpuProcessing.CreatePrograms(); // Not perform there becaus it is done in method 'this.UpdateInitialization' if setup is successfull.
                }
                        catch (Exception ex)
                        {
                            LogData("Form1.Constrcutor", "An exception occured while performing the GPU object setup: " + ex.Message, EG_Utility.LOG_LEVEL.CRITICAL);
                        }
                        finally
                        {
                            _compilrGpu = null;
                        }
                    });
                _compilrGpu.Start(); 
            }
        }
    }
}
