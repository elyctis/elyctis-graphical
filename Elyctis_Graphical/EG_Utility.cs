﻿using System;

namespace Elyctis_Graphical
{
    /// <summary>
    /// Utility class.
    /// </summary>
    public static class EG_Utility
    {
        #region Delegate

        /// <summary>
        /// Delegate used to log data if needed.
        /// <para>
        /// Level: [c] => 0 | [e] => 1 | [w] => 2 | [i] => 3 | [v] => 4 | [d] => 5 | [n] => -1.
        /// (see: <see cref="LOG_LEVEL"/>).
        /// </para>
        /// </summary>
        /// <param name="className">Which class write the message.</param>
        /// <param name="methodName">Which method write the message.</param>
        /// <param name="data">Message to write.</param>
        /// <param name="level">Log level.</param>
        public delegate void DelegateLogData(string className, string methodName, string data, int level);

        /// <summary>
        /// Delegate used to notify initialization.
        /// </summary>
        /// <param name="name">initialization name.</param>
        /// <param name="res">Initialization result.</param>
        /// <param name="msg">Additional message.</param>
        public delegate void DelegateInitialization(string name, bool res, string msg = "");

        #endregion

        #region Const

        /// <summary>
        /// Log level used.
        /// <para>
        /// Level: [c] => 0 | [e] => 1 | [w] => 2 | [i] => 3 | [v] => 4 | [d] => 5 | [n] => -1.
        /// </para>
        /// </summary>
        [Flags]
        public enum LOG_LEVEL : int
        {
            /// <summary>
            /// Don't log annything
            /// </summary>
            NONE = -1,
            /// <summary>
            /// Notify a critical error.
            /// </summary>
            CRITICAL = 0,
            /// <summary>
            /// Notify a error.
            /// </summary>
            ERROR = 1,
            /// <summary>
            /// Notify a warning.
            /// </summary>
            WARNING = 2,
            /// <summary>
            /// Notify an information.
            /// </summary>
            INFO = 3,
            /// <summary>
            /// Notify a data.
            /// </summary>
            VERBOSE = 4,
            /// <summary>
            /// Notify debugging information.
            /// </summary>
            DEBUG = 5

        }

        /// <summary>
        /// Initialization name for GPU set up.
        /// </summary>
        public const string INI_GPU_SETUP = "GPU.Setup";

        /// <summary>
        /// 
        /// </summary>
        public const string INI_GPU_IMG_FORMATING = "GPU.img_formating";

        /// <summary>
        /// 
        /// </summary>
        public const string INI_GPU_IMG_PROCESSING = "GPU.img_processing";

        /// <summary>
        /// 
        /// </summary>
        public const string INI_GPU_IMG_COMPUTING = "GPU.img_computing";

        /// <summary>
        /// 
        /// </summary>
        public const string INI_GPU_CANNY_FILTER = "GPU.img_CannyFilter";

        /// <summary>
        /// 
        /// </summary>
        public const string INI_GPU_INITIALIZATION_END = "GPU.INI_end";

        #endregion

        #region Class

        public abstract class Hardware
        {
            #region Const

            /// <summary>
            /// Default message to display when an exception occured.
            /// </summary>
            protected const string DEFAULT_EXCEPTION_MSG = "An exception occured: ";

            #endregion

            #region Getter/Setter

            /// <summary>
            /// Access to the write console bit.
            /// </summary>
            public bool WriteConsole { get { return _writeConsole; } set { _writeConsole = value; } }

            /// <summary>
            /// Get the initialization state.
            /// </summary>
            public bool IsInitialized { get { return _isInitialized; } }

            /// <summary>
            /// Access to the throw exception bit. It is used to throw or not exception if some occure.
            /// <para>
            /// !!! NOT USED !!!
            /// </para>
            /// </summary>
            public bool ThrowException { get { return _throwException; } set { _throwException = value; } }

            /// <summary>
            /// Access to the log level.
            /// </summary>
            public int LogLevel { get { return _logLevel; } set { _logLevel = value; } }

            /// <summary>
            /// Access to the logging method.
            /// </summary>
            public static DelegateLogData LogMethod { get { return _delegateLogData; } set { _delegateLogData = value; } }

            /// <summary>
            /// Access to the initialization delegate.
            /// </summary>
            public static DelegateInitialization DelegateInitialization { get { return _delegateInitialization; } set { _delegateInitialization = value; } }

            #endregion

            #region Field

            protected bool _writeConsole = true;
            protected bool _isInitialized = false;
            protected bool _throwException = false;

            protected int _logLevel = -1;

            protected string _className = "Hardware"; //this.GetType().Name;

            protected static DelegateLogData _delegateLogData = null;
            protected static DelegateInitialization _delegateInitialization = null;

            #endregion

            #region Method

            /// <summary>
            /// Log data to the provided delegate and to the console.
            /// </summary>        
            /// <param name="data">Data to write.</param>
            /// <param name="method">Which method send the message.</param>
            /// <param name="level">Debugging level (see: <see cref="LOG_LEVEL"/>).</param>
            protected void LogData(string data, string method = "", int level = -1)
            {
                if (level < (int)LOG_LEVEL.NONE)
                {
                    level = (int)LOG_LEVEL.NONE;
                }
                else if (level > (int)LOG_LEVEL.DEBUG)
                {
                    level = (int)LOG_LEVEL.DEBUG;
                }

                if(method != "")
                {
                    method = _className + "." + method;
                }

                if (_writeConsole)
                {
                    Console.WriteLine(string.Format("{0} [{1}] : {2} {3}", DateTime.Now, ((LOG_LEVEL)level).ToString().Substring(0, 1), method, data));
                }

                if (level <= _logLevel)
                {
                    // Log data
                    _delegateLogData?.Invoke(_className, method, data, level);
                    
                }
            }

            /// <summary>
            /// Log data to the provided delegate and to the console.
            /// </summary>
            /// <param name="data">Data to write.</param>
            /// <param name="level">Debugging level.</param>
            protected void LogData(string data, string method, LOG_LEVEL level)
            {
                LogData(data, method, (int)level);
            }

            /// <summary>
            /// Send message concerning initialization.
            /// </summary>
            /// <param name="initializationName">Initialization name.</param>
            /// <param name="result">Initialization result.</param>
            /// <param name="msg">Additional message.</param>
            protected void InitilizationUpdate(string initializationName, bool result, string msg = "")
            {
                _delegateInitialization?.Invoke(initializationName, result, msg);
            }

            #endregion

        }

        #endregion

        #region Method

        /// <summary>
        /// Get the mean value of provided data.
        /// </summary>
        /// <param name="data">Data to compute.</param>
        /// <returns>Computed value.</returns>
        public static float GetMean(byte[] data)
        {
            float res = 0;

            if (data.Length < 1)
                return res;

            for (int i = 0; i < data.Length; i++)
                res += data[i];

            res /= data.Length;

            return res;
        }

        /// <summary>
        /// Add the provided value to the top of provided array and moves all other values.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="values"></param>
        public static void Queuevalue(int value, ref int[] values)
        {
            for (int i = values.Length - 1; i > 0; i--)
            {
                values[i] = values[i - 1];
            }
            values[0] = value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="res"></param>
        public static void GetMinMax(byte[] data, ref int[] res, int mod = 1)
        {
            int length = data.Length;
            res[0] = 255;   // Min
            res[1] = 0;     // Max
            

            for(int i = 0; i< length; i++)
            {
                if(i % mod != 0) continue;

                if(data[i] < res[0])
                {
                    res[0] = data[i];
                    continue;
                }

                if(data[i] > res[1])
                {
                    res[1] = data[i];
                }
            }
        }

        #endregion

    }
}
