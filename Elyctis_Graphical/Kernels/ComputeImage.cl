﻿/*
* Those kernels are used to perform calculs on images.
*/

/**
 * @brief Perform the data swap of the 2 provided data.
 * @param a [local uchar*] First data to swap.
 * @param b [local uchar*] Second data to swap.
 */
inline void swapLocal(__local uchar *a, __local uchar *b) {
	uchar tmp;
	tmp = *b;
	*b = *a;
	*a = tmp;
}


/**
* Create a standard sample with natural coordonate.
*/
__constant sampler_t DEFAULT_SMP = CLK_NORMALIZED_COORDS_FALSE | //Natural coordinates
    CLK_ADDRESS_MIRRORED_REPEAT | //Reapeat image
    CLK_FILTER_LINEAR;
	
/**
* @brief Constant used to normalize devivative computed with matrix 'DERIVATIVE_MATRIX'.
* Normalization of the derivative matrix : Res[x] =((f[x] - f[x-1]) / sum(derivation Matrix elements) )
* sum(derivation Matrix elements) : dme =  4 * 1 + 4 * sqrt(2) = 9.65685424949f
* Normalization of derivative operation : Res[x] = ((f[x] - f[x-1]) / 2) 
* Final normalization : (((f[x] - f[x-1])/2) / dme) = 127 + ((f[x] - f[x-1]) * (2 * dme))
*/
__constant float DERIVATIVE_NOMALIZATION = 19.31370849898476f;
	
/**
* @brief Multioplication matrix used for derivation.
* Circular matrix .
* |sqrt(2)		1 		 sqrt(2)|
* |   1			0			1	|
* |sqrt(2)		1 		 sqrt(2)|
*/
__constant float DERIVATIVE_MATRIX[3][3] =
{
	{M_SQRT2_F,	1.0f ,	M_SQRT2_F},
	{	1.0f,	0.0f, 	1.0f},
	{M_SQRT2_F,	1.0f,	M_SQRT2_F}
};


/**
* @brief Constant used compute the lumlinance from RBG. Order is BGR. 
* Luminance calculation : Y = 0.0722B + 0.7152G + 0.2126R .
*/
__constant float Y_COMPUTATION[3] = {0.0722f, 0.7152f, 0.2126f};

/**
* @brief Get the data byte histogram.*
* @param data [uchar*] Input data to work on.
* @param ouput [uint*] Result array.
*/
__kernel void GetHistogram(__global uchar* data, __global uint* ouput ){

	//ouput[get_global_id(0)]++;
	//atomic_inc(ouput[get_global_id(0)]); // Slow and should only works with integer 
}

/**
* @param Get the local extremum.
* @brief srcImg Input image to analyse.
* @param Args Arguments to provide. [0] =>
* @param destImg Result image
*/ 
__kernel void GetExtremum(__read_only  image2d_t srcImg, __constant int* args, __write_only  image2d_t destImg ){

	// !!!!! Not coded !!!!! //
}

/**
 * @brief Derivate the input matrix with a circulare patern [3*3].
 * @param srcImg Input Image.
 * @param destImg Output image.
 */
__kernel void DerivateImage(__read_only  image2d_t srcImg, __write_only  image2d_t destImg){
	
	int x = -1;
	int y = -1;	
	
	int2 coord = (int2)(get_global_id(0), get_global_id(1));
	int2 workingCoord = coord;
	int4 bgra = read_imagei(srcImg, DEFAULT_SMP, coord); //The byte order is BGRA
	int4 workingBgra;
	float4 tempBgra = (float4)(0.0f,0.0f,0.0f,255.0f);	
	
	float luminance = bgra[0] * Y_COMPUTATION[0] + bgra[1] * Y_COMPUTATION[1] + bgra[2] * Y_COMPUTATION[2];
	float workingLumiannce = 0.0f;
	
	// Loops on derivative matrix
	for (x = 0; x < 3; x++){
		workingCoord.x = coord.x + (x-1);
		for(y=0; y<3;y++){
			workingCoord.y = coord.y + (y-1);
			// Get current pixel
			workingBgra = read_imagei(srcImg, DEFAULT_SMP, workingCoord);
			// Get current pixel luminance
			workingLumiannce = workingBgra[0] * Y_COMPUTATION[0] + workingBgra[1] * Y_COMPUTATION[1] + workingBgra[2] * Y_COMPUTATION[2];
			// Compute the ponderate 
			workingLumiannce = DERIVATIVE_MATRIX[x][y] * (luminance - workingLumiannce);
			
			tempBgra[0] += workingLumiannce;
			tempBgra[1] += workingLumiannce;
			tempBgra[2] += workingLumiannce;
		}
	}	
	
	
	workingBgra = (int4)(
	tempBgra[0] / DERIVATIVE_NOMALIZATION,
	tempBgra[1] / DERIVATIVE_NOMALIZATION,
	tempBgra[2] / DERIVATIVE_NOMALIZATION,
	255
	);
	
	write_imagei(destImg, coord, workingBgra);
}

/**
 * @brief Perform convolution product with provided image and 3*3 matrix.
 * @param srcImg [read only] Input image.
 * @param covMatt_3x3 [const] 3*3 floating matrix to apply.
 * @param destImg [write only] Output image.
 */
__kernel void ConvolutionImage(__read_only image2d_t srcImg, __constant float* covMatt_3x3, __write_only image2d_t destImg){

	int x = -1;
	int y = -1;	
	
	int2 coord = (int2)(get_global_id(0), get_global_id(1));
	int2 workingCoord = coord;
	int4 workingBgra;
	float4 tempBgra = (float4)(0.0f,0.0f,0.0f,255.0f);	
	
	// Loops on 3 * 3 convolution matrix
	for (x = 0; x < 3; x++){
		workingCoord.x = coord.x + (x - 1);
		for(y=0; y < 3; y++){
			workingCoord.y = coord.y + (y - 1);

			// Get current pixel
			workingBgra = read_imagei(srcImg, DEFAULT_SMP, workingCoord);
			// Apply to convolution matrix to each colors
			tempBgra[0] += workingBgra[0] * (float)covMatt_3x3[3*x + y];
			tempBgra[1] += workingBgra[1] * (float)covMatt_3x3[3*x + y];
			tempBgra[2] += workingBgra[2] * (float)covMatt_3x3[3*x + y];
		}
	}
	
	workingBgra = (int4)(
	tempBgra[0] ,
	tempBgra[1] ,
	tempBgra[2] ,
	255
	);
	
	write_imagei(destImg, coord, workingBgra);
}

/**
* @brief Perform the normalization on the provided array.
* @param srcData [uchar*] Inpute data.
* @param Args [float*] Arguments to provide. [0] => Min(input)  | [1] => multiplier (255 / max(input) | [2] => where to apply normalization (index%[2] => 1 all, 4 only first...)
* @param destImg [uchar*] Result data.
*/ 
__kernel void NormalizeBytes(__constant uchar* srcData, __constant float* args, __global uchar* destData ){

	int coord = get_global_id(0);

	float temp = srcData[coord];
	if(coord % (int)args[3] == 0){
		temp = (temp - args[0]) * args[1];
	}

	destData[coord] = (uchar)temp;
}

/**
 * @brief Perform a median filter on 5*5 array basis.
 * @param srcImg [uchar*] Source image data.
 * @param args [int*] Median filter argument. [0]=> input image width.
 * @param destImg [uchar*] Output image.
 */
__kernel void MedianFilter_5_5 (__constant uchar* srcImg, __constant int* args, __global  uchar* destImg){

	// Allocate and compute a median filter
	size_t ind = get_global_id(0);
	int workingInd = 0;
	__local uchar medd[25];

	for (int y = 0; y < 5; y++){
		workingInd = ind - 2 + args[0] * (y-2) ; // Get the pixel on raw (y-2 ) and col - 2 to use the '++'  operator
		for(int x=0; x < 5; x++){
			medd[x + y * 5] = srcImg[workingInd];
			workingInd ++;
		}
	}
	
	for(int i = 1; i <25; i++){
		int j = i;
		while(j > 0 && medd[j-1] > medd[j]){
			swapLocal(medd + j, medd + j - 1);
			j--; 
		}
	}
	destImg[ind] = medd[12];
}