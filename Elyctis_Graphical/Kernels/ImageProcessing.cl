﻿/*
* Those kernels are used to perform image processing.
*/

/**
* Create a standard sample with natural coordonate.
*/
__constant sampler_t DEFAULT_SMP = CLK_NORMALIZED_COORDS_FALSE | //Natural coordinates
    CLK_ADDRESS_MIRRORED_REPEAT |	// Reapeat image
    CLK_FILTER_LINEAR;				// 

/**
* @brief provide colors(byte order is BGRA).
* [0]: green.
* [1]: pink.
* [2]: blue.
* [3]: orange.
* [4]: yellow.
*/	
/*__constant int4 COLORS[] = {
	(int4)(0, 255, 0, 255),		// green
	(int4)(127,0,127,255),		// pink
	(int4)(210,80,60, 255),		// blue
	(int4)(21, 145, 255, 255),	// orange
	(int4)(21, 255, 255, 255)	// yellow
	};

__constant uint COLOR_COUNT = 5; ///< Collor count in 'COLORS'.*/

/*__constant int4 COLORS[] = {
	(int4)(0, 255, 0, 255),
	(int4)(25, 229, 0, 255),
	(int4)(51, 204, 0, 255),
	(int4)(76, 178, 0, 255),
	(int4)(102, 153, 0, 255),
	(int4)(127, 127, 0, 255),
	(int4)(153, 102, 0, 255),
	(int4)(178, 76, 0, 255),
	(int4)(204, 51, 0, 255),
	(int4)(229, 25, 0, 255),
	(int4)(255, 0, 0, 255)
};*/

__constant int4 COLORS[] = {
	(int4)(0x77, 0x9e,0x1b, 255),
	(int4)(0x02, 0x5f, 0xd9, 255),
	(int4)(0xb3,0x70,0x75, 255),
	(int4)(0x8A,0x29, 0xE7, 255),
	(int4)(0x1E,0xA6,0x66, 255),
	(int4)(0x02,0xAB, 0xE6, 255),
	(int4)(0x1D,0x76,0xA6, 255),
	(int4)(0x66,0x66,0x66, 255)
};
__constant uint COLOR_COUNT = 8; ///< Collor count in 'COLORS'.*/
/**
* @brief Perform a contrast scaling.
* @param data [uchar*] Input data.
* @param args [float*] Arguments used for the contrast scalling. [0]Offseset (mean value - depth) | [1]Scaling (256 / ( 2 * deepth)).
* @param output [uchar*] Result array.
*/
__kernel void ContrastScaling(__constant uchar* data, __constant float* args ,__global uchar* output ){

	int ind = get_global_id(0);

	uchar temp = (data[ind] - args[0]) * args[1];	
	
	output[ind] = temp;
}

/**
* @brief Perform a background correction from a reference.
* @param ref [uchar*] Reference white target data.
* @param data [uchar*] Input data.
* @param arg [int*] Arguments used for ofsetting. [0] White taget (reference max value) | [1] offset to remove on final picture data.
* @param output [uchar*] Result array.
*/
__kernel void BackGroundCorrection(__constant uchar* ref, __constant uchar* data, __constant int* args ,__global uchar* output ){

	uint ind = get_global_id(0);
	output[ind] = (uchar)(args[0] - ref[ind] + data[ind] - args[1]);
}

/**
* @brief Perform an adaptive thresholding.
* @param data [uchar*] Input data.
* @param args [int*] Arguments used for the contrast scalling.\
* [0] Picture width [px]. |\
* [1] Picture height. |\
* [2] size_by2 (used to get the square surounding pixel). |\
* [3] Percentage (100 - percentageValue[0-100]). |\
* [4] SizeLenght (2 * size_by2) * (2 * size_by2) * 100 (used to normalize result).
* @param output [uchar*] Result array.
*/
__kernel void AdaptiveThresholding(__constant char* data, __constant int* args , __global char* output ){

	int ind = get_global_id(0);
	// Compute result coordonne
	int2 coord = (int2)(ind % args[0], ind / args[0]);	
	int sum = 0;
	int i,j;
	int tmp;
	
	// Get square surrounding points
	int xLtPoint = coord.x - args[2]; // X Left top corner
	int yLtPoint = coord.y - args[2]; // Y Left top corner
	
	int xRbPoint = coord.x + args[2]; // X Right bottom corner
	int yRbPoint = coord.y + args[2]; // Y Right bottom corner
	
	
	// Check 'X' range
	if(xLtPoint < 0) xLtPoint = 0;
	if(xRbPoint  > args[0] - 1) xRbPoint = args[0] -1;
	// Check 'Y' range
	if(yLtPoint < 0) yLtPoint = 0;
	if(yRbPoint > args[1] - 0) yRbPoint = args[1] - 1;	
	
	
	// Compute the mean of surrounding pixels.
	for(j = yLtPoint; j < yRbPoint; j++){
		tmp = j * args[0];
		for(i = xLtPoint; i < xRbPoint; i++){		 
			sum += data[i + tmp];
		}
	}
	
	tmp = (xRbPoint - xLtPoint)*( yRbPoint- yLtPoint)* 100;
	// Compute the color selection
	output[ind] = (data[ind] * tmp < sum * args[3])? 0x00: 0xFF;
	
}

/**
* @brief Perform an adaptive thresholding from the pseudo integral image.
* @param data [uchar*] Input data.
* @param integral [int*] Pseudo integral of the picture.
* @param args [int*] Arguments used for the contrast scalling.\
* [0] Picture width [px]. |\
* [1] Picture height. |\
* [2] size_by2 (used to get the square surounding pixel). |\
* [3] Percentage (100 - percentageValue[0-100]). |\
* [4] SizeLenght (2 * size_by2) * (2 * size_by2) * 100 (used to normalize result).
* @param output [uchar*] Result array.
*/
__kernel void AdaptiveThresholdingI(__constant uchar* data, __constant int* integral, __constant int* args , __global uchar* output ){
	
	int ind = get_global_id(0);
	// Compute result coordonne
	int2 coord = (int2)(ind % args[0], ind / args[0]);	
	int sum;
	int tmp;
	
	// Get square surrounding points
	int xLtPoint = coord.x - args[2]; // X Left top corner
	int yLtPoint = coord.y - args[2]; // Y Left top corner
	
	int xRbPoint = coord.x + args[2]; // X Right bottom corner
	int yRbPoint = coord.y + args[2]; // Y Right bottom corner
	
	// Check 'X' range
	if(xLtPoint -1 < 0) xLtPoint = 1;
	if(xRbPoint > args[0] - 1) xRbPoint = args[0] - 1;
	// Check 'Y' range
	if(yLtPoint -1 < 0) yLtPoint = 1;
	if(yRbPoint > args[1] - 1) yRbPoint = args[1] - 1;
	
	tmp = (xRbPoint - xLtPoint)*( yRbPoint- yLtPoint)* 100;
	
	// Compute the value
	//sum = data[x2      + y2        *   width] - data[    x2    + (y1        - 1) * width]   - data[x1        - 1 + y2        * width]   + data[x1        - 1 + (y1        - 1) * width];
	//sum = integral[xRbPoint + yRbPoint * args[0]] - integral[xRbPoint + (yLtPoint - 1) * args[0]] - integral[xLtPoint - 1 + yRbPoint * args[0]] + integral[xLtPoint - 1 + (yLtPoint - 1) * args[0]];
	sum = integral[mad_sat(yRbPoint ,args[0], xRbPoint)] - integral[mad_sat( (yLtPoint - 1), args[0], xRbPoint)] - integral[mad_sat(yRbPoint, args[0],xLtPoint - 1)] + integral[mad_sat((yLtPoint - 1), args[0], xLtPoint - 1 )];
	
	// Compute the color selection
	output[ind] = (data[ind] * tmp < sum * args[3])? 0x00: 0xFF;
	
}

/**
* @brief This function set the same color to the pixels in range of provided values.
* @param srcImg [image2d_t] Input image.
* @param args [int*] Agument to set.\
* [0] => Color range[0-255] divided by 2 in order to go in both directions. |\
* [1] => Number of colors.
* [2] => First color (gray [0-255]).
* [.] => Other color (gray [0-255]).
* [.] => ...
* @param destImg [image2d_t] result image.
*/
__kernel void HilightColors(__read_only  image2d_t srcImg, __constant int* args, __write_only  image2d_t destImg){
	
	// Get curent coordonate
	int2 coord = (int2)(get_global_id(0), get_global_id(1));
	// Get input pixel color (byte order is BGRA)
	int4 bgra = read_imagei(srcImg, DEFAULT_SMP, coord);
	
	// Compute the gray level.
	int gray = (bgra[0] + bgra[1] + bgra[2])/3;// * (float)(bgra[3] * 100 / 255);
	int i =0;
	
	// Loop on all provided colors
	for(; i < args[1]; i++){
		// Check if gray is in color range
		if( args[2+i] - args[0] <= gray && args[2+i] + args[0] >= gray){
			bgra = COLORS[i%COLOR_COUNT];
			break;
		}
	}
	// Write result
	write_imagei(destImg, coord, bgra);
}

//__kernel void Errosion(__constant char* data, __constant int* args , __global char* output ){}