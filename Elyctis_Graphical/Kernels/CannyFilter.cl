﻿#define COORD_TO_IND(X, Y, width) (X + Y * width)

/*
 * Those kernels are used to perform Canny edge detection.
 */

/**
 * Create a standard sample with natural coordonate.
 */
__constant sampler_t DEFAULT_SMP = CLK_NORMALIZED_COORDS_FALSE | //Natural coordinates
    CLK_ADDRESS_MIRRORED_REPEAT | //Reapeat image
    CLK_FILTER_LINEAR;
	
/**
 * @brief Constant used to normalize devivative computed with matrix 'DERIVATIVE_MATRIX'.
 * Normalization of the derivative matrix : Res[x] =((f[x] - f[x-1]) / sum(derivation Matrix elements) )
 * sum(derivation Matrix elements) : dme =  4 * 1 + 4 * sqrt(2) = 9.65685424949f
 * Normalization of derivative operation : Res[x] = ((f[x] - f[x-1]) / 2) 
 * Final normalization : (((f[x] - f[x-1])/2) / dme) = 127 + ((f[x] - f[x-1]) * (2 * dme))
 */
__constant float DERIVATIVE_NOMALIZATION = 19.31370849898476f;

/**
 * @brief Converion Radian to degree (180 / PI). 
 */
__constant float RAD_TO_DEG = 180.0f / M_PI_F;

/**
 * @brief Convertion degreee to radian (PI / 180).
 */
__constant float DEG_TO_RAD = M_PI_F / 180.0f;

/**
* @brief Convertion degreee ([0;360]) to byte [0;255].
*/
__constant float DEG_TO_BYTE = 255.0f / 360.0f;

/**
 * @brief Multioplication matrix used for derivation.
 * Circular matrix .
 * |sqrt(2)		1 		 sqrt(2)|
 * |   1			0			1	|
 * |sqrt(2)		1 		 sqrt(2)|
 */
__constant float DERIVATIVE_MATRIX[3][3] =
{
	{M_SQRT2_F,	1.0f ,	M_SQRT2_F},
	{	1.0f,	0.0f, 	1.0f},
	{M_SQRT2_F,	1.0f,	M_SQRT2_F}
};

/**
* @brief Constant used compute the lumlinance from RBG. Order is BGR. 
* Luminance calculation : Y = 0.0722B + 0.7152G + 0.2126R .
*/
__constant float Y_COMPUTATION[3] = {0.0722f, 0.7152f, 0.2126f};

/**
 * @brief Gaussian filter 3*3 with sigma = 1.
 * | 1/16		2/16		1/16
 * | 2/16		4/16		2/16
 * | 1/16		2/16		1/16
 */
__constant float GAUSSIAN_3x3_S1[3][3] = {
	{1.0f/16, 2.0f/16, 1.0f/16},
	{2.0f/16, 4.0f/16, 2.0f/16},
	{1.0f/16, 2.0f/16, 1.0f/16}
};

__constant float GAUSSIAN_5x5_S1[5][5] = {
	{2.0/159.0, 4.0/159.0, 5.0/159.0, 4.0/159.0, 2.0/159.0},
	{4.0/159.0, 9.0/150.0, 12.0/150.0, 9.0/150.0, 4.0/150.0},
	{5.0/150.0, 12.0/150.0, 15.0/150.0, 12.0/150.0, 5.0/150.0},
	{4.0/150.0, 9.0/150.0, 12.0/150.0, 9.0/150.0, 4.0/150.0},
	{2/159.0, 4/159.0, 5/159.0, 4/159.0, 2/159.0}
};

/**
 * @brief Sobel filter Kx (3*3).
 * | -1			  0			 1
 * | -2			  0			 2
 * | -1			  0			 1
 */
__constant float kx_3x3[3][3] = {
	{-1.0f, 0.0f, 1.0f},
	{-2.0f, 0.0f, 2.0f},
	{-1.0f, 0.0f, 1.0f}
};

/**
 * @brief Sobel filter Ky (3*3).
 * |  1			  2			 1
 * |  0			  0			 0
 * | -1			 -2			-1
 */
__constant float ky_3x3[3][3] = {
	{-1.0f, -2.0f, -1.0f},
	{0.0f, 0.0f, 0.0f},
	{1.0f, 2.0f, 1.0f}
};


/**
 * @brief Convert image to Gray scale and apply gaussain filter
 * @param srcImg Source Image.
 * @param args Int array arguments, reserved for futut use.
 * @param destImg Destination image.
 */
__kernel void Canny_Convert_1_1(__read_only image2d_t srcImg, __constant int* args, __write_only  image2d_t destImg){

    int x = -1;
	int y = -1;	
	
	int2 coord = (int2)(get_global_id(0), get_global_id(1));
	int2 workingCoord = coord;
	int4 bgra = read_imagei(srcImg, DEFAULT_SMP, coord); //The byte order is BGRA
	int4 workingBgra;
	float4 tempBgra = (float4)(0.0f,0.0f,0.0f,255.0f);	
	
	float luminance = bgra[0] * Y_COMPUTATION[0] + bgra[1] * Y_COMPUTATION[1] + bgra[2] * Y_COMPUTATION[2];
	float workingLumiannce = 0.0f;

	// Loops on gaussian filter matrix + gray scale
	#pragma unroll
	for (x = 0; x < 3; x++){
		workingCoord.x = coord.x + (x-1);
		#pragma unroll
		for(y=0; y<3;y++){
			workingCoord.y = coord.y + (y-1);
			// Get current pixel
			workingBgra = read_imagei(srcImg, DEFAULT_SMP, workingCoord);
			// Get current pixel luminance
			workingLumiannce = workingBgra[0] * Y_COMPUTATION[0] + workingBgra[1] * Y_COMPUTATION[1] + workingBgra[2] * Y_COMPUTATION[2];
			// Compute the convolution product
			workingLumiannce = (luminance - workingLumiannce) * GAUSSIAN_3x3_S1[x][y];
            // Stor result
			tempBgra[0] += workingLumiannce;
			//tempBgra[1] += workingLumiannce;
			//tempBgra[2] += workingLumiannce;
		}
	}
	// Convert result to integer 
    workingBgra = (int4){
        tempBgra[0],    // Blue
        tempBgra[0],    // Green
        tempBgra[0],    // Red
        255
    };

	write_imagei(destImg, coord, workingBgra);
}

/**
 * @brief Perform the sobel filter and return data of initila image, derivation dtata and angle information.
 * Note: the angle is provided converted to degree [0;255] instead of [0;359]
 * @param srcImg Source Image (Considered as gray scale).
 * @param args Int array arguments, reserved for futut use.
 * @param destImg Destination image containing information. [0] => Derivation result (G), [1] =>  angle information [0;255].
 */
__kernel void Canny_Sobel_2_1(__read_only image2d_t srcImg, __constant int* args, __write_only  image2d_t destImg){

    int x = -1;
	int y = -1;	
	
	int2 coord = (int2)(get_global_id(0), get_global_id(1));
	int2 workingCoord = coord;
	int4 workingBgra = read_imagei(srcImg, DEFAULT_SMP, coord);
	float4 tempBgra = (float4)(0.0f, 0.0f, 0.0f, 255.0f);	


    // Perform the sobel filter
	#pragma unroll
	for (x = 0; x < 3; x++){
		workingCoord.x = coord.x + (x-1);
		#pragma unroll
		for(y=0; y<3;y++){
			workingCoord.y = coord.y + (y-1);
			// Get current pixel
			workingBgra = read_imagei(srcImg, DEFAULT_SMP, workingCoord);
			
			// Compute convolution with Kx and Ky values
			tempBgra[0] += workingBgra[0] * kx_3x3[x][y];
			tempBgra[1] += workingBgra[0] * ky_3x3[x][y];
		}
	}

    // Compute the angle from radian to degreee [0;255]
    tempBgra[2] = atan2(tempBgra[1], tempBgra[0]) * RAD_TO_DEG;// Theta computation and convertion to deg.
    // Convert theta from [-180;180] to [0-360]
    if(0 > tempBgra[2]){
        tempBgra[2] += 180;
    }
    // Normalize theta from [0-360] to [0;255]
    tempBgra[2] *= DEG_TO_BYTE;

    // Normalize sqrt(x*x + y*y). max sqrt(255*255 +255 * 255 ) ~= 360 => we can use the DEG_TO_BYTE multiplicator
    tempBgra[3] = hypot(tempBgra[1], tempBgra[2]) * DEG_TO_BYTE;


    workingBgra = (int4)(
        tempBgra[3],                // Get the derivative information
        tempBgra[2],                // Get the angle information
        0,                			// 
        255                         // Set aplha to 255
    );
	
	write_imagei(destImg, coord, workingBgra);
}

/**
 * @brief Perform the non maximum selection.
 * @param srcImg Source Image (Considered as gray scale with values [blue] => Derivation result (G), [green] =>  angle information [0;255].).
 * @param args Args[0] max threshold [0;255], Args[1] min threshold [0;255].
 * @param destImg Destination image with 3 possibles states [255, 50, 0].
 */
__kernel void Canny_nonMaxSupp_3_1(__read_only image2d_t srcImg, __constant int* args, __write_only  image2d_t destImg){
    
    int2 coord = (int2)(get_global_id(0), get_global_id(1));
    int2 qr = (int2)(255,255);
    // Read image data. format is [0]: gray scale, [1]: derivative data, [2]: angle data ([0;360]deg to  [0;255])
    int4 bgra = read_imagei(srcImg, DEFAULT_SMP, coord);

    // Check angle
    // 0 deg (16 => 22.5 deg | 112 => 157.5 deg | 128 => 180 deg)
    if(16 > bgra[1] || (112 <= bgra[1] && 128 > bgra[1])){
        // q = img[x , y+1]
        qr[0] = read_imagei(srcImg, DEFAULT_SMP, (int2)(coord[0], coord[1]+1))[0];
        // r = img[x , y-1]
        qr[1] = read_imagei(srcImg, DEFAULT_SMP, (int2)(coord[0], coord[1]-1))[0];
    }
    // 45 deg (16 => 22.5 deg | 48 => 67.5 deg )
    else if( 16 <= bgra[1] && 48 > bgra[1]){
        // q = img[x+1 , y-1]
        qr[0] = read_imagei(srcImg, DEFAULT_SMP, (int2)(coord[0]+1, coord[1]-1))[0];
        // r = img[x-1 , y+1]
        qr[1] = read_imagei(srcImg, DEFAULT_SMP, (int2)(coord[0]-1, coord[1]+1))[0];
    }
    // 90 deg (48 => 67.5 deg | 80 => 112.5 deg )
    else if( 48 <= bgra[1] && 80 > bgra[1]){
        // q = img[x+1 , y]
        qr[0] = read_imagei(srcImg, DEFAULT_SMP, (int2)(coord[0]+1, coord[1]))[0];
        // r = img[x-1 , y]
        qr[1] = read_imagei(srcImg, DEFAULT_SMP, (int2)(coord[0]-1, coord[1]))[0];
    }
    // 135 deg (80 => 112.5 deg | 112 => 157.5 deg )
    else if( 80 <= bgra[1] && 112 > bgra[1]){
        // q = img[x-1 , y-1]
        qr[0] = read_imagei(srcImg, DEFAULT_SMP, (int2)(coord[0]-1, coord[1]-1))[0];
        // r = img[x+1 , y+1]
        qr[1] = read_imagei(srcImg, DEFAULT_SMP, (int2)(coord[0]+1, coord[1]+1))[0];
    }

	// Perform the thresholding 
    if(bgra[0] >= qr[0] && bgra[0] >= qr[1]){

		if(bgra[0] >= args[0] ){
			bgra[0] = 255;
		}
		else if (bgra[0] >= args[1]){
			bgra[0] = 50;
		}
		else{
			bgra[0] = 0;
		}
        //bgra[0] = bgra[0];
    }
    else{
        bgra[0] = 0;
    }

	bgra[1] = bgra[0];
	bgra[2] = bgra[0];    
	
	write_imagei(destImg, coord, bgra);
}

/**
 * @brief Perform the Edge tarcking.
 * @param srcImg Source Image with 3 possibles states [255, 50, 0].
 * @param args Int array arguments, reserved for futut use.
 * @param destImg Binarize destination image [0,255].
 */
__kernel void Canny_EdgeTracking_4_1(__read_only image2d_t srcImg, __constant int* args, __write_only  image2d_t destImg){

	int x = -1;
	int y = -1;	

	int2 coord = (int2)(get_global_id(0), get_global_id(1));
    int4 bgra = read_imagei(srcImg, DEFAULT_SMP, coord);

	int2 workingCoord = coord;
	int4 workingBgra;

	if(bgra[0] == 50){

		// Perform the sobel filter
	for (x = 0; x < 3; x++){
		workingCoord.x = coord.x + (x-1);
		for(y=0; y<3;y++){
			workingCoord.y = coord.y + (y-1);
			// Get current pixel
			workingBgra = read_imagei(srcImg, DEFAULT_SMP, workingCoord);

			if(workingBgra[0] == 255){
				goto end_test;
			}
		}
	}	
	
	bgra = (int4)(0,0,0,255);
	write_imagei(destImg, coord, bgra);
	return;

	end_test:
	bgra = (int4)(255,255,255,255);
	write_imagei(destImg, coord, bgra);

	}

}

/**
 * @brief Perform the gaussian filter 5*5 for the Canny filter as first step.
 * @param srcImg [uchar*] Input data.
 * @param args [int*] Arguments. [0]=> image width.
 * @param destImg [uchar*] Output data (image convolved with 5*5 gaussian filter).
 */
__kernel void CannyFilterOp_1(__constant uchar* srcImg, __constant int* args, __global  uchar* destImg){
	// Get current pixel data
	size_t ind = get_global_id(0);
	int workingInd = 0;
	float temp = 0;

	for (int y = 0; y < 5; y++){
		workingInd = ind - 2 + args[0] * (y-2) ; // Get the pixel on raw (y-2 ) and col - 2 to use the '++'  operator
		for(int x=0; x < 5; x++){
			temp +=  srcImg[workingInd] * GAUSSIAN_5x5_S1[x][y];
			workingInd ++;
		}
	}

	destImg[ind] = (uchar)temp;
}

/**
 * @brief Perform the 3*3 sobel filter for the Canny filter as second step.
 * The angle data are computed to return the degree values [0, 45, 90, 135].
 * @param srcImg [uchar*] Input data.
 * @param args [int*] Arguments. [0]=> image width.
 * @param destImg [uchar*] Derivative result data (quadratic difference betwwe Dx and Dy).
 * @param angle [uchar*] Angle results. Results are returned with the following degree values [0, 45, 90, 135].
 */
__kernel void CannyFilterOp_2(__constant uchar* srcImg, __constant int* args, __global  uchar* destImg, __global uchar* angle){

	__constant float _2PI = M_PI_F * 2;
	__constant float _PI_8 = M_PI_F / 8;

	// Get current pixel data
	size_t ind = get_global_id(0);
	int workingInd = 0;
	float4 data = (float4)(0.0f, 0.0f, 0.0f, 0.0f);

	// Perform the sobel filter
	for (int y = 0; y < 3; y++){
		workingInd = ind - 1 + args[0] * (y-1);
		for(int x=0; x<3; x++){
			// Compute convolution with Kx and Ky values
			data[0] += (srcImg[workingInd] * kx_3x3[x][y]);
			data[1] += (srcImg[workingInd] * ky_3x3[x][y]);			
			workingInd ++;
		}
	}

    // The output is now the square root of their squares, but they are
    // constrained to 0 <= value <= 255. Note that hypot is a built in function
    // defined as: hypot(x,y) = sqrt(x*x, y*y).
	data[3] = hypot(data[0],data[1]);
	// Compute the direction angle theta in radians
    // atan2 has a range of (-PI, PI) degrees
    data[2] = atan2(data[1],data[0]);

    // If the angle is negative, 
    // shift the range to (0, 2PI) by adding 2PI to the angle, 
    // then perform modulo operation of 2PI
    if (data[2] < 0)
    {
        data[2] = fmod((data[2] + _2PI),(_2PI));
    }

    // Round the angle to one of four possibilities: 0, 45, 90, 135 degrees
    // then store it in the theta buffer at the proper position
    angle[ind] = ((int)(degrees(data[2] * (_PI_8) + _PI_8-0.0001) / 45) * 45) % 180;
	destImg[ind] = (uchar)(data[3]);
}

/**
 * @brief Perform teh non maximum supression and the strong/ weak pixel sortage for the Canny filter as thirds step.
 * @param srcImg [uchar*] Input data as derivative image.
 * @param angle [uchar*] Input angles data.
 * @param args [int*] Arguments. [0]=> image width.
 * @param destImg [uchar*] Output data.
 */
__kernel void CannyFilterOp_3(__constant uchar* srcImg, __constant uchar* angle, __constant int* args ,__global  uchar* destImg){

	size_t ind = get_global_id(0);
	int2 coord = (int2)(ind % args[0], ind / args[0]);
	uchar data = srcImg[ind];

	switch(angle[ind]){

		case 0:
			// supress me if my neighbor has larger magnitude
			if(srcImg[ind] <= srcImg[COORD_TO_IND(coord.x,coord.y + 1 ,args[0])] ||	// East
				srcImg[ind] <= srcImg[COORD_TO_IND(coord.x,coord.y - 1 ,args[0])])	// Weast
			{
				data = 00;
			}
			break;
		
		case 45:
			// supress me if my neighbor has larger magnitude
			if(srcImg[ind] <= srcImg[COORD_TO_IND(coord.x -1,coord.y + 1 ,args[0])] ||	// Nord East
				srcImg[ind] <= srcImg[COORD_TO_IND(coord.x +1,coord.y - 1 ,args[0])])		// South Weast
			{
				data = 0;
			}
			break;
		
		case 90:
			// supress me if my neighbor has larger magnitude
			if(srcImg[ind] <= srcImg[COORD_TO_IND(coord.x -1,coord.y ,args[0])] ||	// Nord
				srcImg[ind] <= srcImg[COORD_TO_IND(coord.x +1,coord.y  ,args[0])])	// South
			{
				data = 0;
			}
			break;

		case 135:
			// supress me if my neighbor has larger magnitude
			if(srcImg[ind] <= srcImg[COORD_TO_IND(coord.x -1,coord.y -1 ,args[0])] ||	// Nord West
				srcImg[ind] <= srcImg[COORD_TO_IND(coord.x +1,coord.y +1  ,args[0])])	// South East
			{
				data = 0;
			}
			break;

		default:			
			break;
	}

	if(data >= args[1]){
		data = 255;
	}
	else if (data >= args[2] ){
		data = 127;
	}

	destImg[ind] = data;
}

/**
 * @brief Perform the Edge tracking with histeresis selection for the Canny filter as fourth step.
 * @param srcImg [uchar*] Input image data.
 * @param args [int*] Arguments. [0]=> image width | [1]=> High threshold | [2]=> Low threshold.
 * @param destImg [uchar*] Result data.
 */
__kernel void CannyFilterOp_4(__constant uchar* srcImg, __constant int* args ,__global  uchar* destImg){

	size_t ind = get_global_id(0);
	int workingInd;

	if(srcImg[ind] > 127){
		destImg[ind] = srcImg[ind];
		return;
	}
	else if(srcImg[0] == 127){

		// Perform the sobel filter
		for (int y = 0; y < 3; y++){
			workingInd = ind - 1 + args[0] * (y-1);
			for(int x=0; x<3; x++){
				if( srcImg[workingInd]  > 127){
					destImg[ind] = 255;
					return;
				}
				workingInd ++;
			}
		}
	}
	destImg[ind] = 0;
}