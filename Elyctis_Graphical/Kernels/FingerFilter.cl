﻿// Elyctis finger print detection algorythms


/**
 * @brief Converion Radian to degree (180 / PI). 
 */
__constant float RAD_TO_DEG = 180.0f / M_PI_F;

/**
 * @brief Convertion degreee to radian (PI / 180).
 */
__constant float DEG_TO_RAD = M_PI_F / 180.0f;

/**
* @brief Convertion degreee ([0;360]) to byte [0;255].
*/
__constant float DEG_TO_BYTE = 255.0f / 360.0f;

/**
 * @brief Multioplication matrix used for derivation.
 * Circular matrix .
 * |sqrt(2)		1 		 sqrt(2)|
 * |   1			0			1	|
 * |sqrt(2)		1 		 sqrt(2)|
 */
__constant float DERIVATIVE_MATRIX[3][3] =
{
	{M_SQRT2_F,	1.0f ,	M_SQRT2_F},
	{	1.0f,	0.0f, 	1.0f},
	{M_SQRT2_F,	1.0f,	M_SQRT2_F}
};

/**
* @brief Constant used compute the lumlinance from RBG. Order is BGR. 
* Luminance calculation : Y = 0.0722B + 0.7152G + 0.2126R .
*/
__constant float Y_COMPUTATION[3] = {0.0722f, 0.7152f, 0.2126f};

/**
 * @brief Gaussian filter 3*3 with sigma = 1.
 * | 1/16		2/16		1/16
 * | 2/16		4/16		2/16
 * | 1/16		2/16		1/16
 */
__constant float GAUSSIAN_3x3_S1[3][3] = {
	{1.0f/16, 2.0f/16, 1.0f/16},
	{2.0f/16, 4.0f/16, 2.0f/16},
	{1.0f/16, 2.0f/16, 1.0f/16}
};

__constant float GAUSSIAN_5x5_S1[5][5] = {
	{2.0/159.0, 4.0/159.0, 5.0/159.0, 4.0/159.0, 2.0/159.0},
	{4.0/159.0, 9.0/150.0, 12.0/150.0, 9.0/150.0, 4.0/150.0},
	{5.0/150.0, 12.0/150.0, 15.0/150.0, 12.0/150.0, 5.0/150.0},
	{4.0/150.0, 9.0/150.0, 12.0/150.0, 9.0/150.0, 4.0/150.0},
	{2/159.0, 4/159.0, 5/159.0, 4/159.0, 2/159.0}
};

/**
 * @brief Sobel filter Kx (3*3).
 * | -1			  0			 1
 * | -2			  0			 2
 * | -1			  0			 1
 */
__constant float kx_3x3[3][3] = {
	{-1.0f, 0.0f, 1.0f},
	{-2.0f, 0.0f, 2.0f},
	{-1.0f, 0.0f, 1.0f}
};

/**
 * @brief Sobel filter Ky (3*3).
 * |  1			  2			 1
 * |  0			  0			 0
 * | -1			 -2			-1
 */
__constant float ky_3x3[3][3] = {
	{-1.0f, -2.0f, -1.0f},
	{0.0f, 0.0f, 0.0f},
	{1.0f, 2.0f, 1.0f}
};

/**
 * @brief Create an elypsoidal mask with values 0 and 1 for convolmution operation.
 * @param srcImg [uchar*] Input image data.
 * @param args [uint*] Function arguments. [0]=> Image width | [1]=> Elypse X radius | [2]=> Elypse Y radius.
 * @param destImg [uchar*] Destination image.
 */
__kernel void mask(__constant uchar* srcImg, __constant int* args, __global uchar* destImg){

    int ind = get_blobal_id(0);
    int2 coord = ;

    

}