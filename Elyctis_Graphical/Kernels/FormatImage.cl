﻿/*
* Those kernels are used to perform image formating.
*/

/**
* Create a standard sample with natural coordonate.
*/
__constant sampler_t DEFAULT_SMP = CLK_NORMALIZED_COORDS_FALSE | //Natural coordinates
    CLK_ADDRESS_CLAMP_TO_EDGE | //Clamp to zeros
    CLK_FILTER_LINEAR;

/**
* @brief Get a byte array corresponding of the gray scale of the source image.
* @param srcImg Input image to convert.
* @param res [char*] Result byte array.
* @param Args [int*] Arguments to provide. [0] => Index of input image stride (img width).\
* [1] => picture width. 
*/
__kernel void GetPictureBytes(__read_only  image2d_t srcImg, __global char* res, __constant int* args){

	if(get_global_id(0) >= args[0])
		return;

	int2 coord = (int2)(get_global_id(0), get_global_id(1));
	int4 bgra = read_imagei(srcImg, DEFAULT_SMP, coord); //The byte order is BGRA
	
	int ind = coord.x + coord.y * args[1];

	int temp = max(bgra[0] , bgra[1]);
	temp = max(temp, bgra[2]);
	
	res[ind] = (uchar)temp;
}

/**
* @brief Generate an image file from the provide byte array and image width.
* @param inputData [uchar*] Input byte array corresponding to the gray scaled image.
* @param args [int*] Arguments to provide. [0] => Picture width.
* @param destImg Result image.
*/
__kernel void GetBmpFromBytes(__constant char* inputData, __constant int* args, __write_only  image2d_t destImg ){

	int2 coord = (int2)(get_global_id(0), get_global_id(1));

	int ind = mad_sat(coord.y, args[0], coord.x);

	// The byte order is BGRA
	int4 bgra = {(uchar)inputData[ind], (uchar)inputData[ind], (uchar)inputData[ind], 255};
	
	write_imagei(destImg, coord, bgra);
}