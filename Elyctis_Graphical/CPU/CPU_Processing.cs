﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Elyctis_Graphical.CPU
{
    /// <summary>
    /// Class used to perform CPU operation.
    /// </summary>
    public partial class CPU_Processing : EG_Utility.Hardware
    {

        #region Constructor

        /// <summary>
        /// Standard constructor for <see cref="CPU_Processing"/>.
        /// </summary>
        /// <remarks>
        /// Default log level is <see cref="EG_Utility.LOG_LEVEL.WARNING"/>.
        /// </remarks>
        /// <param name="delegateLogData">Delegate used to log data.</param>
        /// <param name="delegateInitialization">Delegate used to notify initialization events.</param>
        /// <param name="writeConsole">Does the logs should be write to the standard output.</param>
        /// <param name="throwException">Does exception should be thrown.</param>
        public CPU_Processing(EG_Utility.DelegateLogData delegateLogData = null, EG_Utility.DelegateInitialization delegateInitialization = null,
            bool writeConsole = false, bool throwException = false)
        {            
            _writeConsole = writeConsole;            
            _throwException = throwException;

            _logLevel = (int)EG_Utility.LOG_LEVEL.WARNING;

            _className = GetType().Name;

            _delegateLogData = delegateLogData;
            _delegateInitialization = delegateInitialization;

            _isInitialized = true; // No special initialzation are required.
        }

        #endregion

    }
}
