﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;

namespace Elyctis_Graphical.CPU
{
    public partial class CPU_Processing : EG_Utility.Hardware
    {
        /// <summary>
        /// Create a bitmap file from image byte array.
        /// </summary>
        /// <param name="img">Image data.</param>
        /// <param name="width">Image width.</param>
        /// <param name="height">Image height.</param>
        /// <returns>Generated Bitmap.</returns>
        public Bitmap GetBmpFromBytes(byte[] img, int width, int height)
        {
            Bitmap bmp = null;

            if (img.Length < 1)
            {
                return bmp;
            }

            try
            {
                bmp = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format8bppIndexed);
                System.Drawing.Imaging.ColorPalette colpal = bmp.Palette;
                Color[] grays = colpal.Entries;
                for (int i = 0; i < grays.Length; i++)
                    grays[i] = Color.FromArgb(i, i, i);
                bmp.Palette = colpal;

                System.Drawing.Imaging.BitmapData bmp_data = bmp.LockBits(
                                         new Rectangle(0, 0, bmp.Width, bmp.Height),
                                         System.Drawing.Imaging.ImageLockMode.WriteOnly, bmp.PixelFormat);

                //Copy the data from the byte array into BitmapData.Scan0
                Marshal.Copy(img, 0, bmp_data.Scan0, img.Length);

                //Unlock the pixels
                bmp.UnlockBits(bmp_data);
            }
            catch (Exception e)
            {
                LogData(DEFAULT_EXCEPTION_MSG + e.Message, "GetBmpFromBytes", EG_Utility.LOG_LEVEL.WARNING);
                if (_throwException)
                    throw e;
            }

            return bmp;
        }

        /// <summary>
        /// Create a byte array form a bitmap picture. (Return the max of RGB components).
        /// </summary>
        public byte[] GetImageBytes(Bitmap picture)
        {
            byte[] res = new byte[picture.Height * picture.Width];
            int value = 0;
            int lineOffset = 0;
            try
            {
                for (int y = 0; y < picture.Height; y++)
                {
                    lineOffset = y * picture.Width;
                    for (int x = 0; x < picture.Width; x++)
                    {
                        value = Math.Max(picture.GetPixel(x, y).B, picture.GetPixel(x, y).G);
                        value = Math.Max(value, picture.GetPixel(x, y).R);
                        res[x + lineOffset] = (byte)value;
                    }
                }

            }
            catch (Exception e)
            {
                LogData(DEFAULT_EXCEPTION_MSG + e.Message, "GetImageBytes", EG_Utility.LOG_LEVEL.WARNING);
                if (_throwException)
                    throw e;
            }

            return res;
        }
    }
}
