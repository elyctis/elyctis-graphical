﻿using System;

namespace Elyctis_Graphical.CPU
{
    
    public partial class CPU_Processing : EG_Utility.Hardware
    {
        /// <summary>
        /// Perform a contrast scaling algorithm.
        /// </summary>
        /// <param name="data">Data to compute.</param>
        /// <param name="mean">Value used to center the result.</param>
        /// <param name="scale">Scaling deepth.</param>
        /// <returs>Computed value.</returs>
        public byte[] ContrastScaling(byte[] data, int mean, int deepth = 50)
        {
            byte[] res = new byte[data.Length];
            float scaling = (float)256 / (2 * deepth);
            int temp = 0;
            int offset = mean - deepth;

            try
            {
                for (int i = 0; i < data.Length; i++)
                {
                    temp = (int)((data[i] - offset) * scaling);

                    if (temp < 0)
                    {
                        temp = 0;
                    }
                    else if (temp > 255)
                    {
                        temp = 255;
                    }
                    res[i] = (byte)temp;
                }
            }
            catch (Exception e)
            {
                LogData(DEFAULT_EXCEPTION_MSG + e.Message, "ContrastScaling", EG_Utility.LOG_LEVEL.WARNING);
                if (_throwException)
                    throw e;
            }

            return res;
        }

        /// <summary>
        /// Correct an image from the provided reference.
        /// </summary>
        /// This algorythm generate a correction matrix from the reference and the provided targeted value.
        /// <param name="reference">Reference image (White image taken with a white target).</param>
        /// <param name="capture">Capture picture.</param>
        /// <param name="target">White targeted value. Should be set to the maximum value of reference in order to avoid negative values.</param>
        /// <returns>Result array</returns>
        public byte[] BackGroundCorrection(byte[] reference, byte[] capture, byte target, byte offset = 0)
        {
            byte[] res = new byte[capture.Length];
            int i = 0;
            int temp = 0;

            try
            {
                if (reference.Length != capture.Length)
                    return res;

                // Compute reference data
                for (; i < reference.Length; i++)
                {
                    reference[i] = (byte)Math.Abs(target - reference[i]);
                    temp = (reference[i] + capture[i]) - offset;
                    temp = temp < 0 ? 0 : temp;
                    temp = temp > 255 ? 255 : temp;
                    res[i] = (byte)(temp);
                }
            }
            catch (Exception e)
            {
                LogData(DEFAULT_EXCEPTION_MSG + e.Message, "BackGroundCorrection", EG_Utility.LOG_LEVEL.WARNING);
                if (_throwException)
                    throw e;
            }

            return res;
        }

        /// <summary>
        /// Do an adapatative threshold with method of intergral image : 
        /// https://pdfs.semanticscholar.org/8d74/418ec3c4e2ff45b72e723ac0fbe5fcd58620.pdf
        /// </summary>
        /// <param name="data">Byte array of the picture.</param>
        /// <param name="pictureWidth">Width of the picture.</param>
        /// <param name="pictureHeight">Height of the picture.</param>
        /// <param name="percentage">Threshold percentage [0-100].</param>
        /// <param name="size_by2">Half size of the square comparaison matrix surounding the working pixel.</param>
        /// <returns></returns>
        public byte[] AdaptiveThreshold(byte[] data, int pictureWidth, int pictureHeight, int percentage = 2, int size_by2 = 22)
        {
            byte[] output = new byte[data.Length];
            int[] integral = new int[data.Length];

            int sum = 0;
            int pos = 0;
            int count = (2 * size_by2) * (2 * size_by2) * 100;
            percentage = (100 - percentage);
            int x1, x2, y1, y2;

            try
            {
                // Create the integral matrix
                for (int x = 0; x < pictureWidth; x++)
                {
                    sum = 0;
                    for (int y = 0; y < pictureHeight; y++)
                    {
                        pos = x + y * pictureWidth;
                        sum += data[pos];
                        if (x == 0)
                        {
                            integral[pos] = sum;
                        }
                        else
                        {
                            integral[pos] = integral[pos - 1] + sum;
                        }
                    }
                }

                // Compute the adaptative threshold
                for (int x = 0; x < pictureWidth; x++)
                {
                    x1 = x - size_by2;
                    x2 = x + size_by2;
                    for (int y = 0; y < pictureHeight; y++)
                    {
                        // Compute integral area

                        y1 = y - size_by2;
                        y2 = y + size_by2;

                        pos = x + y * pictureWidth;

                        if (y1 - 1 < 0 || x1 - 1 < 0 || y2 >= pictureHeight || x2 >= pictureWidth)
                        {
                            output[pos] = 0xFF;
                            continue;
                        }

                        // Get local sum
                        sum = integral[x2 + y2 * pictureWidth] - integral[x2 + (y1 - 1) * pictureWidth] - integral[x1 - 1 + y2 * pictureWidth] + integral[x1 - 1 + (y1 - 1) * pictureWidth];

                        // Do thresholding
                        if (data[x + y * pictureWidth] * count < sum * percentage)
                        {
                            output[pos] = 0x00;
                        }
                        else
                        {
                            output[pos] = 0xFF;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                LogData(DEFAULT_EXCEPTION_MSG + e.Message, "AdaptiveThreshold", EG_Utility.LOG_LEVEL.WARNING);
                if (_throwException)
                    throw e;
            }

            return output;
        }


        /// <summary>
        /// Performa a media filter.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="width"></param>
        /// <param name="heigth"></param>
        /// <param name="filterSize"></param>
        /// <returns></returns>
        public byte[] MedianFileter(byte[] data, int width, int heigth, uint filterSize = 5)
        {
            byte[] temp = new byte[filterSize* filterSize];
            byte[] res = new byte[data.Length];
            int workingInd = 0;
            int filterSize_2 = (int)(filterSize / 2);            

            try
            {

                for (int i = 0; i < data.Length; i++)
                {
                   
                    for (int y = 0; y < filterSize; y++)
                    {
                        workingInd = i - filterSize_2 + width * (y - filterSize_2); // Get the pixel on raw (y-2 ) and col - 2 to use the '++'  operator
                        for (int x = 0; x < filterSize; x++)
                        {
                            temp[x + y * 5] = data[CheckIndex(width, heigth, workingInd)];
                            workingInd++;
                        }
                    }
                    
                    for (int j = 1; j < filterSize * filterSize; j++)
                    {
                        int k = j;
                        while (k > 0 && temp[k - 1] > temp[k])
                        {
                            swapLocal(ref temp, k, k - 1);
                            k--;
                        }
                    }

                    
                    res[i] = temp[(filterSize * filterSize) / 2];

                }
            }
            catch (Exception e)
            {

               
            }


            void swapLocal(ref byte[] arr, int indA, int indB)
            {
                byte tmp;
                tmp = arr[indB];
                arr[indB] = arr[indA];
                arr[indA] = tmp;
            }

            return res;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="width"></param>
        /// <param name="heigth"></param>
        /// <param name="i"></param>
        /// <returns></returns>
        private uint CheckIndex(int width, int heigth, int i)
        {
            uint res = (uint)i;
            int x = 0;
            int y = 0;

            if(i < 0)
            {
                i = -i;

                x = i % width;
                y = i / width;

                res = (uint)(x + y * width);
            }
            else if (i >= width * heigth)
            {
                x = i % width;
                y = i / width;

                y = 2 * heigth - y- 1;

                res = (uint)(x + y * width);
            }

            return res;
        }
    }
}
