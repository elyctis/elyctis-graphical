﻿namespace Elyctis_Graphical.CPU
{
    public partial class CPU_Processing : EG_Utility.Hardware
    {
        /// <summary>
        /// Generate an histogram from the provided values.
        /// </summary>
        /// <param name="data">Inpute data.</param>
        /// <returns>Data histogram.</returns>
        public static int[] GetHistogram(byte[] data)
        {
            int[] res = new int[256];
            int i = 0;

            for (; i < data.Length; i++)
            {
                res[data[i]] += 1;
            }

            return res;
        }

        /// <summary>
        /// Perform the derivation of provided histogram with the 'smoothing' values.
        /// </summary>
        /// <param name="histo">Histogram.</param>
        /// <param name="smoothing">Values number to mean for smoothing.</param>
        /// <returns>Dervation of the provided histogram.</returns>
        public static int[] GetDerivateHistogram(int[] histo, byte smoothing = 4)
        {
            int i = 0;
            int[] res = new int[histo.Length];
            int[] smoothArray = new int[smoothing > 1 ? smoothing : 1]; // Get the smothing width, minimum value is 1

            //initialyze the smothing value with histogram first vaule
            for (; i < smoothing; i++)
            {
                smoothArray[i] = histo[0];
            }

            for (i = 0; i < histo.Length; i++)
            {
                if ((i > 0 && i < histo.Length - 2) && histo[i] == 0 && histo[i - 1] != 0 && histo[i + 1] != 0)
                    histo[i] = (histo[i - 1] + histo[i + 1]) / 2;

                // Add value to queue
                EG_Utility.Queuevalue(histo[i], ref smoothArray);
                // Compute the derivative
                res[i] = (int)ComputeMean(smoothArray, true);
            }

            return res;
        }


        /// <summary>
        /// Compute the mean (or derivative mean) of the provide array.
        /// </summary>
        /// <param name="values">Value to use.</param>
        /// <param name="derivative">Perform or not the derivation.</param>
        /// <returns>Calculated mean.</returns>
        private static float ComputeMean(int[] values, bool derivative = false)
        {
            float res = 0;

            if (derivative)
            {
                for (int i = 1; i < values.Length; i++)
                {
                    res += values[i] - values[i - 1];
                }
                res /= (values.Length - 1) > 0 ? (values.Length - 1) : 1;
            }
            else
            {
                for (int i = 0; i < values.Length; i++)
                {
                    res += values[i];
                }
                res /= values.Length > 0 ? values.Length : 1;
            }

            return res;
        }
    }
}
