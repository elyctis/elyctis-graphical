﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace Elyctis_Graphical.GPU
{
    public partial class GPU_Processing : EG_Utility.Hardware
    {
        // Get local minimum

        // get local maximum

        /// <summary>
        /// Devivate the input image with a circular patern.
        /// </summary>
        /// <param name="img">Input image.</param>
        /// <param name="result">Result image.</param>
        /// <returns>Execution result.</returns>
        public bool DerivateImage(Bitmap img, out Bitmap result)
        {
            bool res = false;

            OpenCL.Net.Kernel kernel = new OpenCL.Net.Kernel();
            OpenCL.Net.Event clevent = new OpenCL.Net.Event();
            OpenCL.Net.IMem inputImage2DBuffer = null;
            OpenCL.Net.IMem outputImage2DBuffer = null;
            OpenCL.Net.ErrorCode error;

            result = null;

            try
            {
                int intPtrSize = Marshal.SizeOf(typeof(IntPtr));
                int imageStride = 0;
                int imageSize = 0;
                //Image's RGBA data converted to an unmanaged[] array
                byte[] inputByteArray;
                byte[] outputByteArray;

                /*****************
                *Create Kernel object with 'precompiled' program
                * ************/
                //Create the required kernel (entry function)
                kernel = OpenCL.Net.Cl.CreateKernel(clPrograms[(int)PROGRAMS.IMAGE_COMPUTING], "DerivateImage", out error);
                if (!CheckErr(error, "Cl.CreateKernel"))
                {
                    return res;
                }

                /**************
                 * Read picture and prepare it to be usable for GPU
                 **************/
                OpenCL.Net.ImageFormat clImageFormat = new OpenCL.Net.ImageFormat(OpenCL.Net.ChannelOrder.RGBA, OpenCL.Net.ChannelType.Unsigned_Int8);

                //Get raw pixel data of the bitmap
                //The format should match the format of clImageFormat
                BitmapData bitmapData = img.LockBits(new Rectangle(0, 0, img.Width, img.Height),
                                     ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
                imageStride = bitmapData.Stride;

                imageSize = bitmapData.Stride * bitmapData.Height;
                //Copy the raw bitmap data to an unmanaged byte[] array
                inputByteArray = new byte[imageSize];
                outputByteArray = new byte[imageSize];
                Marshal.Copy(bitmapData.Scan0, inputByteArray, 0, imageSize);

                img.UnlockBits(bitmapData);

                // Allocate OpenCL image memory buffer
                inputImage2DBuffer = OpenCL.Net.Cl.CreateImage2D(_context, OpenCL.Net.MemFlags.CopyHostPtr | OpenCL.Net.MemFlags.ReadOnly, clImageFormat,
                                                    (IntPtr)img.Width, (IntPtr)img.Height,
                                                    (IntPtr)0, inputByteArray, out error);

                if (!CheckErr(error, "Cl.CreateImage2D input"))
                {
                    return res;
                }
                // Allocate OpenCL output image memory buffer
                outputImage2DBuffer = OpenCL.Net.Cl.CreateImage2D(_context, OpenCL.Net.MemFlags.CopyHostPtr |
                    OpenCL.Net.MemFlags.WriteOnly, clImageFormat, (IntPtr)img.Width,
                    (IntPtr)img.Height, (IntPtr)0, outputByteArray, out error);

                if (!CheckErr(error, "Cl.CreateImage2D outputImage2DBuffer"))
                {
                    return res;
                }

                /*************
                 * Prepare kernel
                 *************/
                // Set the memory buffers to the kernel function
                error = OpenCL.Net.Cl.SetKernelArg(kernel, 0, (IntPtr)intPtrSize, inputImage2DBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 0"))
                {
                    return res;
                }

                error = OpenCL.Net.Cl.SetKernelArg(kernel, 1, (IntPtr)intPtrSize, outputImage2DBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 1"))
                {
                    return res;
                }

                /*******
                 * load and execute program
                 ******/

                IntPtr[] originPtr = new IntPtr[] { (IntPtr)0, (IntPtr)0, (IntPtr)0 };    //x, y, z
                IntPtr[] regionPtr = new IntPtr[] { (IntPtr)img.Width, (IntPtr)img.Height, (IntPtr)1 };    //x, y, z
                IntPtr[] workGroupSizePtr = new IntPtr[] { (IntPtr)img.Width, (IntPtr)img.Height, (IntPtr)1 };
                error = OpenCL.Net.Cl.EnqueueWriteImage(_cmdQueue, inputImage2DBuffer, OpenCL.Net.Bool.True,
                   originPtr, regionPtr, (IntPtr)0, (IntPtr)0, inputByteArray, 0, null, out clevent);

                if (!CheckErr(error, "Cl.EnqueueWriteImage"))
                {
                    return res;
                }

                //Execute  kernel (OpenCL code)
                error = OpenCL.Net.Cl.EnqueueNDRangeKernel(_cmdQueue, kernel, 2, null, workGroupSizePtr, null, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueNDRangeKernel"))
                {
                    return res;
                }

                //Wait for completion of all calculations on the GPU.
                error = OpenCL.Net.Cl.Finish(_cmdQueue);
                CheckErr(error, "Cl.Finish");


                //Read the processed image from GPU to RGBA
                error = OpenCL.Net.Cl.EnqueueReadImage(_cmdQueue, outputImage2DBuffer, OpenCL.Net.Bool.True,
                   originPtr, regionPtr, (IntPtr)0, (IntPtr)0, outputByteArray, 0, null, out clevent);
                if (!CheckErr(error, "Cl.clEnqueueReadImage"))
                {
                    return res;
                }

                /**************
                 * Generate the picture.
                 **************/

                result = new Bitmap(img.Width, img.Height, PixelFormat.Format32bppArgb);

                bitmapData = result.LockBits(new Rectangle(0, 0, img.Width, img.Height),
                                     ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);

                Marshal.Copy(outputByteArray, 0, bitmapData.Scan0, outputByteArray.Length);

                result.UnlockBits(bitmapData);

                res = true;
            }
            catch (Exception e)
            {
                LogData(DEFAULT_EXCEPTION_MSG + e.Message, "DerivateImage", EG_Utility.LOG_LEVEL.WARNING);
                if (_throwException)
                    throw e;
            }
            finally
            {
                //Clean up memory
                OpenCL.Net.Cl.ReleaseKernel(kernel);

                OpenCL.Net.Cl.ReleaseEvent(clevent);
                if (inputImage2DBuffer != null)
                    OpenCL.Net.Cl.ReleaseMemObject(inputImage2DBuffer);
                if (outputImage2DBuffer != null)
                    OpenCL.Net.Cl.ReleaseMemObject(outputImage2DBuffer);
            }
            return res;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="output"></param>
        /// <param name="mod"></param>
        /// <returns></returns>
        public bool NomalizeByteArray(byte[] input, out byte[] outputByteArray, int mod = 0)
        {
            bool res = false;
            int[] temp = new int[2];
            float[] args = new float[3];

            outputByteArray = new byte[input.Length];

            EG_Utility.GetMinMax(input, ref temp, mod);
            
            // Not required
            if(temp[0] == 0 && temp[1] == 255)
            {
                outputByteArray = input;
                res = true;
                return res;
            }

            args[0] = temp[0];
            args[1] = 255.0f / temp[1];
            args[2] = mod;

            

            OpenCL.Net.Kernel kernel = new OpenCL.Net.Kernel();
            OpenCL.Net.Event clevent = new OpenCL.Net.Event();
            OpenCL.Net.IMem inputDataBuffer = null;
            OpenCL.Net.IMem outputDataBuffer = null;
            OpenCL.Net.IMem inputArguments = null;
            OpenCL.Net.ErrorCode error;

            //Image's RGBA data converted to an unmanaged[] array

            try
            {
                int intPtrSize = Marshal.SizeOf(typeof(IntPtr));
                int readOffset = 0;
                int sizeOfFloat = sizeof(float);


                /**************
                 * Create Kernel object with 'precompiled' program
                 *************/
                //Create the required kernel (entry function)
                kernel = OpenCL.Net.Cl.CreateKernel(clPrograms[(int)PROGRAMS.IMAGE_COMPUTING], "NormalizeBytes", out error);
                if (!CheckErr(error, "Cl.CreateKernel"))
                {
                    return res;
                }

                /**************
                 * Prepare memory to be usable for GPU
                 **************/


                outputDataBuffer = OpenCL.Net.Cl.CreateBuffer(_context, OpenCL.Net.MemFlags.WriteOnly, (IntPtr)outputByteArray.Length, out error);
                if (!CheckErr(error, "Cl.CreateImage2D outputImageBuffer"))
                {
                    return res;
                }

                // Alocate OpenCl input memory buffer
                inputDataBuffer = OpenCL.Net.Cl.CreateBuffer(_context, OpenCL.Net.MemFlags.ReadOnly, (IntPtr)input.Length, out error);

                if (!CheckErr(error, "Cl.CreateBuffer inputImageBuffer"))
                {
                    return res;
                }

                // Allocat input arguments buffer
                inputArguments = OpenCL.Net.Cl.CreateBuffer(_context, OpenCL.Net.MemFlags.ReadOnly, (IntPtr)(sizeOfFloat * args.Length), out error);
                if (!CheckErr(error, "Cl.CreateBuffer inputArguments"))
                {
                    return res;
                }

                /*************
                 * Prepare kernel
                 *************/
                //Pass the memory buffers to our kernel function
                error = OpenCL.Net.Cl.SetKernelArg(kernel, 0, (IntPtr)intPtrSize, inputDataBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 1"))
                {
                    return res;
                }

                error = OpenCL.Net.Cl.SetKernelArg(kernel, 1, (IntPtr)intPtrSize, inputArguments);
                if (!CheckErr(error, "Cl.SetKernelArg 2"))
                {
                    return res;
                }

                error = OpenCL.Net.Cl.SetKernelArg(kernel, 2, (IntPtr)intPtrSize, outputDataBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 3"))
                {
                    return res;
                }

                /*******
                 * load and execute program
                 ******/

                IntPtr[] workGroupSizePtr = new IntPtr[] { (IntPtr)input.Length, (IntPtr)1, (IntPtr)1 };

                // Enqueue write the image data to GPU as byte[] array
                error = OpenCL.Net.Cl.EnqueueWriteBuffer(_cmdQueue, inputDataBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeof(byte) * input.Length), input, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueWriteBuffer image input"))
                {
                    return res;
                }

                // Enqueue write the arguments data to GPU as integer array
                error = OpenCL.Net.Cl.EnqueueWriteBuffer(_cmdQueue, inputArguments, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeOfFloat * args.Length), args, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueWriteBuffer argument input"))
                {
                    return res;
                }

                //Execute  kernel (OpenCL code)
                error = OpenCL.Net.Cl.EnqueueNDRangeKernel(_cmdQueue, kernel, 2, null, workGroupSizePtr, null, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueNDRangeKernel"))
                {
                    return res;
                }

                //Wait for completion of all calculations on the GPU.
                error = OpenCL.Net.Cl.Finish(_cmdQueue);
                CheckErr(error, "Cl.Finish");


                /*IntPtr[] originPtr = new IntPtr[] { (IntPtr)0, (IntPtr)0, (IntPtr)0 };    //x, y, z
                IntPtr[] regionPtr = new IntPtr[] { (IntPtr)input.Length, (IntPtr)1, (IntPtr)1 };    //x, y, z*/

                //Read the processed image from GPU to RGBA
                /*error = OpenCL.Net.Cl.EnqueueReadImage(_cmdQueue, outputDataBuffer, OpenCL.Net.Bool.True,
                   originPtr, regionPtr, (IntPtr)0, (IntPtr)0, outputByteArray, 0, null, out clevent);*/


                error = OpenCL.Net.Cl.EnqueueReadBuffer(_cmdQueue, outputDataBuffer, OpenCL.Net.Bool.True, (IntPtr)0, (IntPtr)input.Length, outputByteArray, 0, null, out clevent);
                if (!CheckErr(error, "Cl.clEnqueueReadImage"))
                {
                    return res;
                }

                res = true;
            }
            catch (Exception e)
            {
                LogData(DEFAULT_EXCEPTION_MSG + e.Message, "GetBmpFromBytes", EG_Utility.LOG_LEVEL.WARNING);
                if (_throwException)
                    throw e;
            }
            finally
            {
                //Clean up memory
                OpenCL.Net.Cl.ReleaseKernel(kernel);

                OpenCL.Net.Cl.ReleaseEvent(clevent);
                if (outputDataBuffer != null)
                    OpenCL.Net.Cl.ReleaseMemObject(outputDataBuffer);
                if (inputDataBuffer != null)
                    OpenCL.Net.Cl.ReleaseMemObject(inputDataBuffer);
                if (inputArguments != null)
                    OpenCL.Net.Cl.ReleaseMemObject(inputArguments);
            }

            return res;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="inputWidth"></param>
        /// <param name="inputHeight"></param>
        /// <param name="outputByteArray"></param>
        /// <returns></returns>
        public bool MedianFilter_5_5(byte[] input, int inputWidth, int inputHeight, out byte[] outputByteArray)
        {
            bool res = false;
            int[] args = new int[] { inputWidth, inputHeight, input.Length };

            outputByteArray = new byte[input.Length];



            OpenCL.Net.Kernel kernel = new OpenCL.Net.Kernel();
            OpenCL.Net.Event clevent = new OpenCL.Net.Event();
            OpenCL.Net.IMem inputDataBuffer = null;
            OpenCL.Net.IMem outputDataBuffer = null;
            OpenCL.Net.IMem inputArguments = null;
            OpenCL.Net.ErrorCode error;

            //Image's RGBA data converted to an unmanaged[] array

            try
            {
                int intPtrSize = 0;
                int readOffset = 0;
                int sizeOfInt = sizeof(int);
                intPtrSize = Marshal.SizeOf(typeof(IntPtr));

                IntPtr[] workGroupSizePtr = new IntPtr[] { (IntPtr)(input.Length), (IntPtr)1, (IntPtr)1 };
                IntPtr[] LocalWorkSizePtr = new IntPtr[] { (IntPtr)(8), (IntPtr)1, (IntPtr)1 };

                /**************
                 * Create Kernel object with 'precompiled' program
                 *************/
                //Create the required kernel (entry function)
                kernel = OpenCL.Net.Cl.CreateKernel(clPrograms[(int)PROGRAMS.IMAGE_COMPUTING], "MedianFilter_5_5", out error);
                if (!CheckErr(error, "Cl.CreateKernel"))
                {
                    return res;
                }

                /**************
                 * Prepare memory to be usable for GPU
                 **************/


                outputDataBuffer = OpenCL.Net.Cl.CreateBuffer(_context, OpenCL.Net.MemFlags.WriteOnly, (IntPtr)(outputByteArray.Length * sizeof(byte)), out error);
                if (!CheckErr(error, "Cl.CreateImage2D outputImageBuffer"))
                {
                    return res;
                }

                // Alocate OpenCl input memory buffer
                inputDataBuffer = OpenCL.Net.Cl.CreateBuffer(_context, OpenCL.Net.MemFlags.ReadOnly, (IntPtr)(input.Length * sizeof(byte)), out error);

                if (!CheckErr(error, "Cl.CreateBuffer inputImageBuffer"))
                {
                    return res;
                }

                // Allocat input arguments buffer
                inputArguments = OpenCL.Net.Cl.CreateBuffer(_context, OpenCL.Net.MemFlags.ReadOnly, (IntPtr)(sizeOfInt * args.Length), out error);
                if (!CheckErr(error, "Cl.CreateBuffer inputArguments"))
                {
                    return res;
                }

                /*************
                 * Prepare kernel
                 *************/
                //Pass the memory buffers to our kernel function
                error = OpenCL.Net.Cl.SetKernelArg(kernel, 0, (IntPtr)intPtrSize, inputDataBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 1"))
                {
                    return res;
                }

                error = OpenCL.Net.Cl.SetKernelArg(kernel, 1, (IntPtr)intPtrSize, inputArguments);
                if (!CheckErr(error, "Cl.SetKernelArg 2"))
                {
                    return res;
                }

                error = OpenCL.Net.Cl.SetKernelArg(kernel, 2, (IntPtr)intPtrSize, outputDataBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 3"))
                {
                    return res;
                }

                /*******
                 * load and execute program
                 ******/

                // Enqueue write the image data to GPU as byte[] array
                error = OpenCL.Net.Cl.EnqueueWriteBuffer(_cmdQueue, inputDataBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeof(byte) * input.Length), input, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueWriteBuffer image input"))
                {
                    return res;
                }

                // Enqueue write the arguments data to GPU as integer array
                error = OpenCL.Net.Cl.EnqueueWriteBuffer(_cmdQueue, inputArguments, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeOfInt * args.Length), args, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueWriteBuffer argument input"))
                {
                    return res;
                }

                //Execute  kernel (OpenCL code)
                error = OpenCL.Net.Cl.EnqueueNDRangeKernel(_cmdQueue, kernel, 1, null, workGroupSizePtr, LocalWorkSizePtr, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueNDRangeKernel"))
                {
                    return res;
                }

                //Wait for completion of all calculations on the GPU.
                error = OpenCL.Net.Cl.Finish(_cmdQueue);
                CheckErr(error, "Cl.Finish");


                error = OpenCL.Net.Cl.EnqueueReadBuffer(_cmdQueue, outputDataBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)0, (IntPtr)input.Length, outputByteArray, 0, null, out clevent);
                if (!CheckErr(error, "Cl.clEnqueueReadImage"))
                {
                    return res;
                }

                res = true;
            }
            catch (Exception e)
            {
                LogData(DEFAULT_EXCEPTION_MSG + e.Message, "GetBmpFromBytes", EG_Utility.LOG_LEVEL.WARNING);
                if (_throwException)
                    throw e;
            }
            finally
            {
                //Clean up memory
                OpenCL.Net.Cl.ReleaseKernel(kernel);

                OpenCL.Net.Cl.ReleaseEvent(clevent);
                if (outputDataBuffer != null)
                    OpenCL.Net.Cl.ReleaseMemObject(outputDataBuffer);
                if (inputDataBuffer != null)
                    OpenCL.Net.Cl.ReleaseMemObject(inputDataBuffer);
                if (inputArguments != null)
                    OpenCL.Net.Cl.ReleaseMemObject(inputArguments);
            }

            return res;
        }

    }
}
