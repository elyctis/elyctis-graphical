﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace Elyctis_Graphical.GPU
{
    public partial class GPU_Processing : EG_Utility.Hardware
    {
        /// <summary>
        /// Get image bytes from a bmp.
        /// </summary>
        /// <remarks>
        /// Before using this method, make sure that GPU kernels are compilled (see: <see cref="CreatePrograms"/>).
        /// </remarks>
        /// <param name="img">Input image.</param>
        /// <param name="result"></param>
        /// <returns>Execution state.</returns>
        public bool GetImageBytes(Bitmap img, out byte[] result)
        {
            bool res = false;

            int inputImgWidth = img.Width;
            int inputImgHeight = img.Height;

            int inputImgBytesSize = 0;

            result = new byte[0];


            //OpenCL memory buffer that will keep our image's byte[] data.
            OpenCL.Net.Kernel kernel = new OpenCL.Net.Kernel();
            OpenCL.Net.Event clevent = new OpenCL.Net.Event();
            OpenCL.Net.IMem inputImage2DBuffer = null;
            OpenCL.Net.IMem outputImageBuffer = null;
            OpenCL.Net.IMem inputArgumentBuffer = null;
            OpenCL.Net.ErrorCode error;

            try
            {
                int intPtrSize = 0;
                int readOffset = 0;
                int[] args = { 0, 0 };
                int sizeOfInt = sizeof(int);
                intPtrSize = Marshal.SizeOf(typeof(IntPtr));
                //Image's RGBA data converted to an unmanaged[] array
                byte[] inputByteArray;


                /**************
                 * Create Kernel object with 'precompiled' program
                 *************/
                //Create the required kernel (entry function)
                kernel = OpenCL.Net.Cl.CreateKernel(clPrograms[(int)PROGRAMS.IMAGE_FORMATING], "GetPictureBytes", out error);
                if (!CheckErr(error, "Cl.CreateKernel"))
                {
                    return res;
                }

                /**************
                 * Read picture and prepare it to be usable for GPU
                 **************/
                OpenCL.Net.ImageFormat clImageFormat = new OpenCL.Net.ImageFormat(OpenCL.Net.ChannelOrder.RGBA, OpenCL.Net.ChannelType.Unsigned_Int8);

                //Get raw pixel data of the bitmap
                //The format should match the format of clImageFormat
                BitmapData bitmapData = img.LockBits(new Rectangle(0, 0, img.Width, img.Height),
                                     ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);

                inputImgBytesSize = bitmapData.Stride * bitmapData.Height;
                // 
                args[0] = img.Width;
                args[1] = img.Width;

                //Copy the raw bitmap data to an unmanaged byte[] array
                inputByteArray = new byte[inputImgBytesSize];
                Marshal.Copy(bitmapData.Scan0, inputByteArray, 0, inputImgBytesSize);

                img.UnlockBits(bitmapData);


                //Allocate OpenCL image memory buffer
                inputImage2DBuffer = OpenCL.Net.Cl.CreateImage2D(_context, OpenCL.Net.MemFlags.CopyHostPtr | OpenCL.Net.MemFlags.ReadOnly, clImageFormat,
                                                    (IntPtr)bitmapData.Width, (IntPtr)bitmapData.Height,
                                                    (IntPtr)0, inputByteArray, out error);

                if (!CheckErr(error, "Cl.CreateImage2D input"))
                {
                    return res;
                }

                // Update result with correct length
                result = new byte[img.Height * img.Width];

                // Alocate OpenCl output memory buffer
                outputImageBuffer = OpenCL.Net.Cl.CreateBuffer(_context, OpenCL.Net.MemFlags.ReadWrite, (IntPtr)result.Length, out error);

                if (!CheckErr(error, "Cl.CreateBuffer output"))
                {
                    return res;
                }

                inputArgumentBuffer = OpenCL.Net.Cl.CreateBuffer(_context, OpenCL.Net.MemFlags.ReadOnly, (IntPtr)(sizeOfInt * args.Length), out error);
                if (!CheckErr(error, "Cl.CreateBuffer argumentStindeInd"))
                {
                    return res;
                }

                /*************
                 * Prepare and load kernel
                 *************/
                //Pass the memory buffers to our kernel function
                error = OpenCL.Net.Cl.SetKernelArg(kernel, 0, (IntPtr)intPtrSize, inputImage2DBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 1"))
                {
                    return res;
                }

                error = OpenCL.Net.Cl.SetKernelArg(kernel, 1, (IntPtr)intPtrSize, outputImageBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 2"))
                {
                    return res;
                }

                error = OpenCL.Net.Cl.SetKernelArg(kernel, 2, (IntPtr)intPtrSize, inputArgumentBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 3"))
                {
                    return res;
                }


                /*******
                 * load and execute program
                 ******/
                //Copy input image from the host to the GPU.
                IntPtr[] originPtr = new IntPtr[] { (IntPtr)0, (IntPtr)0, (IntPtr)0 };    //x, y, z
                IntPtr[] regionPtr = new IntPtr[] { (IntPtr)inputImgWidth, (IntPtr)inputImgHeight, (IntPtr)1 };    //x, y, z
                IntPtr[] workGroupSizePtr = new IntPtr[] { (IntPtr)inputImgWidth, (IntPtr)inputImgHeight, (IntPtr)1 };
                //IntPtr[] localWorkSize = new IntPtr[] { (IntPtr)8, (IntPtr)8, (IntPtr)1 };

                error = OpenCL.Net.Cl.EnqueueWriteImage(_cmdQueue, inputImage2DBuffer, OpenCL.Net.Bool.True,
                   originPtr, regionPtr, (IntPtr)0, (IntPtr)0, inputByteArray, 0, null, out clevent);

                if (!CheckErr(error, "Cl.EnqueueWriteImage"))
                {
                    return res;
                }

                error = OpenCL.Net.Cl.EnqueueWriteBuffer(_cmdQueue, inputArgumentBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeOfInt * args.Length), args, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueWriteBuffer"))
                {
                    return res;
                }

                //Execute  kernel (OpenCL code)
                //error = OpenCL.Net.Cl.EnqueueNDRangeKernel(_cmdQueue, kernel, 2, null, workGroupSizePtr, localWorkSize, 0, null, out clevent);
                error = OpenCL.Net.Cl.EnqueueNDRangeKernel(_cmdQueue, kernel, 2, null, workGroupSizePtr, null, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueNDRangeKernel"))
                {
                    return res;
                }

                //Wait for completion of all calculations on the GPU.
                error = OpenCL.Net.Cl.Finish(_cmdQueue);
                CheckErr(error, "Cl.Finish");

                //Read the processed image from GPU to raw RGBA data byte[] array
                error = OpenCL.Net.Cl.EnqueueReadBuffer(_cmdQueue, outputImageBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeof(byte) * result.Length), result, 0, null, out clevent);
                if (!CheckErr(error, "Cl.clEnqueueReadImage"))
                {
                    return res;
                }

                res = true;

            }
            catch (Exception e)
            {
                LogData(DEFAULT_EXCEPTION_MSG + e.Message, "GetImageBytes", EG_Utility.LOG_LEVEL.WARNING);
                if (_throwException)
                    throw e;
            }
            finally
            {
                //Clean up memory
                OpenCL.Net.Cl.ReleaseKernel(kernel);

                OpenCL.Net.Cl.ReleaseEvent(clevent);
                if (inputImage2DBuffer != null)
                    OpenCL.Net.Cl.ReleaseMemObject(inputImage2DBuffer);
                if (outputImageBuffer != null)
                    OpenCL.Net.Cl.ReleaseMemObject(outputImageBuffer);
                if (inputArgumentBuffer != null)
                    OpenCL.Net.Cl.ReleaseMemObject(inputArgumentBuffer);
            }

            return res;
        }

        /// <summary>
        /// Generate a bitmap object from the provided byte array.
        /// </summary>
        /// <param name="data">Picture data.</param>
        /// <param name="result">Result picture.</param>
        /// <returns>Execution result.</returns>
        public bool GetBmpFromBytes(byte[] data, int imgHeight, int imgWidth, out Bitmap result)
        {
            bool res = false;
            result = null;


            OpenCL.Net.Kernel kernel = new OpenCL.Net.Kernel();
            OpenCL.Net.Event clevent = new OpenCL.Net.Event();
            OpenCL.Net.IMem inputImageBuffer = null;
            OpenCL.Net.IMem outputImage2DBuffer = null;
            OpenCL.Net.IMem inputArguments = null;
            OpenCL.Net.ErrorCode error;

            //Image's RGBA data converted to an unmanaged[] array
            byte[] outputByteArray;

            try
            {
                int intPtrSize = Marshal.SizeOf(typeof(IntPtr));
                int readOffset = 0;
                int sizeOfInt = sizeof(int);

                int[] arguments = { imgWidth };

                outputByteArray = new byte[imgHeight * imgWidth * sizeOfInt];

                /**************
                 * Create Kernel object with 'precompiled' program
                 *************/
                //Create the required kernel (entry function)
                kernel = OpenCL.Net.Cl.CreateKernel(clPrograms[(int)PROGRAMS.IMAGE_FORMATING], "GetBmpFromBytes", out error);
                if (!CheckErr(error, "Cl.CreateKernel"))
                {
                    return res;
                }

                /**************
                 * Prepare memory to be usable for GPU
                 **************/
                OpenCL.Net.ImageFormat clImageFormat = new OpenCL.Net.ImageFormat(OpenCL.Net.ChannelOrder.RGBA, OpenCL.Net.ChannelType.Unsigned_Int8);


                //Allocate OpenCL output image memory buffer
                outputImage2DBuffer = OpenCL.Net.Cl.CreateImage2D(_context, OpenCL.Net.MemFlags.CopyHostPtr |
                    OpenCL.Net.MemFlags.WriteOnly, clImageFormat, (IntPtr)imgWidth,
                    (IntPtr)imgHeight, (IntPtr)0, outputByteArray, out error);

                if (!CheckErr(error, "Cl.CreateImage2D outputImage2DBuffer"))
                {
                    return res;
                }

                // Alocate OpenCl input memory buffer
                inputImageBuffer = OpenCL.Net.Cl.CreateBuffer(_context, OpenCL.Net.MemFlags.ReadOnly, (IntPtr)data.Length, out error);

                if (!CheckErr(error, "Cl.CreateBuffer inputImageBuffer"))
                {
                    return res;
                }

                // Allocat input arguments buffer
                inputArguments = OpenCL.Net.Cl.CreateBuffer(_context, OpenCL.Net.MemFlags.ReadOnly, (IntPtr)(sizeOfInt * arguments.Length), out error);
                if (!CheckErr(error, "Cl.CreateBuffer inputArguments"))
                {
                    return res;
                }

                /*************
                 * Prepare kernel
                 *************/
                //Pass the memory buffers to our kernel function
                error = OpenCL.Net.Cl.SetKernelArg(kernel, 0, (IntPtr)intPtrSize, inputImageBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 1"))
                {
                    return res;
                }

                error = OpenCL.Net.Cl.SetKernelArg(kernel, 1, (IntPtr)intPtrSize, inputArguments);
                if (!CheckErr(error, "Cl.SetKernelArg 2"))
                {
                    return res;
                }

                error = OpenCL.Net.Cl.SetKernelArg(kernel, 2, (IntPtr)intPtrSize, outputImage2DBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 3"))
                {
                    return res;
                }

                /*******
                 * load and execute program
                 ******/

                IntPtr[] workGroupSizePtr = new IntPtr[] { (IntPtr)imgWidth, (IntPtr)imgHeight, (IntPtr)1 };

                // Enqueue write the image data to GPU as byte[] array
                error = OpenCL.Net.Cl.EnqueueWriteBuffer(_cmdQueue, inputImageBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeof(byte) * data.Length), data, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueWriteBuffer image input"))
                {
                    return res;
                }

                // Enqueue write the arguments data to GPU as integer array
                error = OpenCL.Net.Cl.EnqueueWriteBuffer(_cmdQueue, inputArguments, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeOfInt * arguments.Length), arguments, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueWriteBuffer argument input"))
                {
                    return res;
                }

                //Execute  kernel (OpenCL code)
                error = OpenCL.Net.Cl.EnqueueNDRangeKernel(_cmdQueue, kernel, 2, null, workGroupSizePtr, null, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueNDRangeKernel"))
                {
                    return res;
                }

                //Wait for completion of all calculations on the GPU.
                error = OpenCL.Net.Cl.Finish(_cmdQueue);
                CheckErr(error, "Cl.Finish");


                IntPtr[] originPtr = new IntPtr[] { (IntPtr)0, (IntPtr)0, (IntPtr)0 };    //x, y, z
                IntPtr[] regionPtr = new IntPtr[] { (IntPtr)imgWidth, (IntPtr)imgHeight, (IntPtr)1 };    //x, y, z

                //Read the processed image from GPU to RGBA
                error = OpenCL.Net.Cl.EnqueueReadImage(_cmdQueue, outputImage2DBuffer, OpenCL.Net.Bool.True,
                   originPtr, regionPtr, (IntPtr)0, (IntPtr)0, outputByteArray, 0, null, out clevent);
                if (!CheckErr(error, "Cl.clEnqueueReadImage"))
                {
                    return res;
                }

                /**************
                 * Generate the picture.
                 **************/

                result = new Bitmap(imgWidth, imgHeight, PixelFormat.Format32bppArgb);

                BitmapData bitmapData = result.LockBits(new Rectangle(0, 0, imgWidth, imgHeight),
                                     ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);

                Marshal.Copy(outputByteArray, 0, bitmapData.Scan0, outputByteArray.Length);

                result.UnlockBits(bitmapData);

                res = true;
            }
            catch (Exception e)
            {
                LogData(DEFAULT_EXCEPTION_MSG + e.Message, "GetBmpFromBytes", EG_Utility.LOG_LEVEL.WARNING);
                if (_throwException)
                    throw e;
            }
            finally
            {
                //Clean up memory
                OpenCL.Net.Cl.ReleaseKernel(kernel);

                OpenCL.Net.Cl.ReleaseEvent(clevent);
                if (outputImage2DBuffer != null)
                    OpenCL.Net.Cl.ReleaseMemObject(outputImage2DBuffer);
                if (inputImageBuffer != null)
                    OpenCL.Net.Cl.ReleaseMemObject(inputImageBuffer);
                if (inputArguments != null)
                    OpenCL.Net.Cl.ReleaseMemObject(inputArguments);
            }

            return res;
        }
    }
}
