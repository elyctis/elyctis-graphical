﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace Elyctis_Graphical.GPU
{
    /// <summary>
    /// Partial class for Canny Filter
    /// </summary>
    public partial class GPU_Processing : EG_Utility.Hardware
    {

        /// <summary>
        /// Perform the canny filter on image data.
        /// </summary>
        /// <param name="img"></param>
        /// <param name="args"></param>
        /// <param name="result"></param>
        /// <returns>Execution status.</returns>
        public bool CannyFilter(Bitmap img, int[] args, out Bitmap result)
        {
            bool res = false;

            OpenCL.Net.Kernel[] kernels = new OpenCL.Net.Kernel[]{
                new OpenCL.Net.Kernel(),    // Canny_Convert_1_1 
                new OpenCL.Net.Kernel(),    // Canny_Sobel_2_1
                new OpenCL.Net.Kernel(),     // Canny_nonMaxSupp_3_1
                new OpenCL.Net.Kernel()     // Canny_EdgeTracking_4_1
            };
            OpenCL.Net.Event clevent = new OpenCL.Net.Event();
            OpenCL.Net.IMem inputImage2DBuffer = null;
            OpenCL.Net.IMem outputImage2DBuffer = null;
            OpenCL.Net.IMem inputArgumentBuffer = null;
            OpenCL.Net.ErrorCode error;

            result = null;

            try
            {
                int intPtrSize = 0;
                int readOffset = 0;
                int imageStride = 0;
                int imageSize = 0;
                int sizeOfInt = sizeof(int);
                intPtrSize = Marshal.SizeOf(typeof(IntPtr));
                //Image's RGBA data converted to an unmanaged[] array
                byte[] inputByteArray;
                byte[] outputByteArray;

                IntPtr[] originPtr = new IntPtr[] { (IntPtr)0, (IntPtr)0, (IntPtr)0 };    //x, y, z
                IntPtr[] regionPtr = new IntPtr[] { (IntPtr)img.Width, (IntPtr)img.Height, (IntPtr)1 };    //x, y, z
                IntPtr[] workGroupSizePtr = new IntPtr[] { (IntPtr)img.Width, (IntPtr)img.Height, (IntPtr)1 };

                /*****************
                *Create Kernel object with 'precompiled' program
                * ************/
                //Create the required kernel (entry function)
                kernels[0] = OpenCL.Net.Cl.CreateKernel(clPrograms[(int)PROGRAMS.CANNY_FILTER], "Canny_Convert_1_1", out error);
                if (!CheckErr(error, "Cl.CreateKernel.Canny_Convert_1_1"))
                {
                    return res;
                }
                //Create the required kernel (entry function)
                kernels[1] = OpenCL.Net.Cl.CreateKernel(clPrograms[(int)PROGRAMS.CANNY_FILTER], "Canny_Sobel_2_1", out error);
                if (!CheckErr(error, "Cl.CreateKernel.Canny_Sobel_2_1"))
                {
                    return res;
                }
                //Create the required kernel (entry function)
                kernels[2] = OpenCL.Net.Cl.CreateKernel(clPrograms[(int)PROGRAMS.CANNY_FILTER], "Canny_nonMaxSupp_3_1", out error);
                if (!CheckErr(error, "Cl.CreateKernel.Canny_NON_MAX_SUPP_3_1"))
                {
                    return res;
                }
                //Create the required kernel (entry function)
                kernels[3] = OpenCL.Net.Cl.CreateKernel(clPrograms[(int)PROGRAMS.CANNY_FILTER], "Canny_EdgeTracking_4_1", out error);
                if (!CheckErr(error, "Cl.CreateKernel.Canny_EdgeTracking_4_1"))
                {
                    return res;
                }

                /**************
                 * Read picture and prepare it to be usable for GPU
                 **************/
                OpenCL.Net.ImageFormat clImageFormat = new OpenCL.Net.ImageFormat(OpenCL.Net.ChannelOrder.RGBA, OpenCL.Net.ChannelType.Unsigned_Int8);

                //Get raw pixel data of the bitmap
                //The format should match the format of clImageFormat
                BitmapData bitmapData = img.LockBits(new Rectangle(0, 0, img.Width, img.Height),
                                     ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
                imageStride = bitmapData.Stride;

                imageSize = bitmapData.Stride * bitmapData.Height;
                //Copy the raw bitmap data to an unmanaged byte[] array
                inputByteArray = new byte[imageSize];
                outputByteArray = new byte[imageSize];
                Marshal.Copy(bitmapData.Scan0, inputByteArray, 0, imageSize);

                img.UnlockBits(bitmapData);

                // Allocate OpenCL image memory buffer
                inputImage2DBuffer = OpenCL.Net.Cl.CreateImage2D(_context, OpenCL.Net.MemFlags.CopyHostPtr | OpenCL.Net.MemFlags.ReadOnly, clImageFormat,
                                                    (IntPtr)img.Width, (IntPtr)img.Height,
                                                    (IntPtr)0, inputByteArray, out error);

                if (!CheckErr(error, "Cl.CreateImage2D input"))
                {
                    return res;
                }
                // Allocate OpenCl input argument buffer
                inputArgumentBuffer = OpenCL.Net.Cl.CreateBuffer(_context, OpenCL.Net.MemFlags.ReadOnly, (IntPtr)(sizeOfInt * args.Length), out error);
                if (!CheckErr(error, "Cl.CreateBuffer argumentStindeInd"))
                {
                    return res;
                }

                // Allocate OpenCL output image memory buffer
                outputImage2DBuffer = OpenCL.Net.Cl.CreateImage2D(_context, OpenCL.Net.MemFlags.CopyHostPtr | OpenCL.Net.MemFlags.WriteOnly, clImageFormat, (IntPtr)img.Width,
                    (IntPtr)img.Height, (IntPtr)0, outputByteArray, out error);

                if (!CheckErr(error, "Cl.CreateImage2D outputImage2DBuffer"))
                {
                    return res;
                }

                /*************
                 * Prepare kernel
                 *************/
                // Set the memory buffers to the kernel function
                error = OpenCL.Net.Cl.SetKernelArg(kernels[0], 0, (IntPtr)intPtrSize, inputImage2DBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 1"))
                {
                    return res;
                }

                error = OpenCL.Net.Cl.SetKernelArg(kernels[0], 1, (IntPtr)intPtrSize, inputArgumentBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 2"))
                {
                    return res;
                }

                error = OpenCL.Net.Cl.SetKernelArg(kernels[0], 2, (IntPtr)intPtrSize, outputImage2DBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 3"))
                {
                    return res;
                }

                /*******
                 * load and execute program
                 ******/

                error = OpenCL.Net.Cl.EnqueueWriteImage(_cmdQueue, inputImage2DBuffer, OpenCL.Net.Bool.True,
                   originPtr, regionPtr, (IntPtr)0, (IntPtr)0, inputByteArray, 0, null, out clevent);

                if (!CheckErr(error, "Cl.EnqueueWriteImage"))
                {
                    return res;
                }

                // Enqueue write the arguments data to GPU as integer array
                error = OpenCL.Net.Cl.EnqueueWriteBuffer(_cmdQueue, inputArgumentBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeOfInt * args.Length), args, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueWriteBuffer argument input"))
                {
                    return res;
                }

                //Execute  kernel (OpenCL code)
                error = OpenCL.Net.Cl.EnqueueNDRangeKernel(_cmdQueue, kernels[0], 2, null, workGroupSizePtr, null, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueNDRangeKernel"))
                {
                    return res;
                }
                //Wait for completion of all calculations on the GPU.
                error = OpenCL.Net.Cl.Finish(_cmdQueue);
                CheckErr(error, "Cl.Finish");

                //Read the processed image from GPU to RGBA
                error = OpenCL.Net.Cl.EnqueueReadImage(_cmdQueue, outputImage2DBuffer, OpenCL.Net.Bool.True,
                   originPtr, regionPtr, (IntPtr)0, (IntPtr)0, outputByteArray, 0, null, out clevent);
                if (!CheckErr(error, "Cl.clEnqueueReadImage"))
                {
                    return res;
                }

                // Test
                /*result = new Bitmap(img.Width, img.Height, PixelFormat.Format32bppArgb);
                bitmapData = result.LockBits(new Rectangle(0, 0, img.Width, img.Height),
                                     ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
                Marshal.Copy(outputByteArray, 0, bitmapData.Scan0, outputByteArray.Length);
                result.UnlockBits(bitmapData);

                res = true;
                return res;*/

                //****************************************************************************************************************************************************************
                // Execute 2nd kernel

                // Copy output to input
                Array.Copy(outputByteArray, inputByteArray, inputByteArray.Length);

                // Allocate OpenCL image memory buffer
                /*inputImage2DBuffer = OpenCL.Net.Cl.CreateImage2D(_context, OpenCL.Net.MemFlags.CopyHostPtr | OpenCL.Net.MemFlags.ReadOnly, clImageFormat,
                                                    (IntPtr)img.Width, (IntPtr)img.Height,
                                                    (IntPtr)0, inputByteArray, out error);

                if (!CheckErr(error, "Cl.CreateImage2D input"))
                {
                    return res;
                }
                // Allocate OpenCl input argument buffer
                inputArgumentBuffer = OpenCL.Net.Cl.CreateBuffer(_context, OpenCL.Net.MemFlags.ReadOnly, (IntPtr)(sizeOfInt * args.Length), out error);
                if (!CheckErr(error, "Cl.CreateBuffer argumentStindeInd"))
                {
                    return res;
                }

                // Allocate OpenCL output image memory buffer
                outputImage2DBuffer = OpenCL.Net.Cl.CreateImage2D(_context, OpenCL.Net.MemFlags.CopyHostPtr | OpenCL.Net.MemFlags.WriteOnly, clImageFormat, (IntPtr)img.Width,
                    (IntPtr)img.Height, (IntPtr)0, outputByteArray, out error);

                if (!CheckErr(error, "Cl.CreateImage2D outputImage2DBuffer"))
                {
                    return res;
                }*/

                /*************
                 * Prepare kernel
                 *************/
                // Set the memory buffers to the kernel function
                error = OpenCL.Net.Cl.SetKernelArg(kernels[1], 0, (IntPtr)intPtrSize, inputImage2DBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 1"))
                {
                    return res;
                }

                error = OpenCL.Net.Cl.SetKernelArg(kernels[1], 1, (IntPtr)intPtrSize, inputArgumentBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 2"))
                {
                    return res;
                }

                error = OpenCL.Net.Cl.SetKernelArg(kernels[1], 2, (IntPtr)intPtrSize, outputImage2DBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 3"))
                {
                    return res;
                }

                /*******
                 * load and execute program
                 ******/

                error = OpenCL.Net.Cl.EnqueueWriteImage(_cmdQueue, inputImage2DBuffer, OpenCL.Net.Bool.True,
                   originPtr, regionPtr, (IntPtr)0, (IntPtr)0, inputByteArray, 0, null, out clevent);

                if (!CheckErr(error, "Cl.EnqueueWriteImage"))
                {
                    return res;
                }

                // Enqueue write the arguments data to GPU as integer array
                error = OpenCL.Net.Cl.EnqueueWriteBuffer(_cmdQueue, inputArgumentBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeOfInt * args.Length), args, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueWriteBuffer argument input"))
                {
                    return res;
                }

                //Execute  kernel (OpenCL code)
                error = OpenCL.Net.Cl.EnqueueNDRangeKernel(_cmdQueue, kernels[1], 2, null, workGroupSizePtr, null, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueNDRangeKernel"))
                {
                    return res;
                }
                //Wait for completion of all calculations on the GPU.
                error = OpenCL.Net.Cl.Finish(_cmdQueue);
                CheckErr(error, "Cl.Finish");

                //Read the processed image from GPU to RGBA
                error = OpenCL.Net.Cl.EnqueueReadImage(_cmdQueue, outputImage2DBuffer, OpenCL.Net.Bool.True,
                   originPtr, regionPtr, (IntPtr)0, (IntPtr)0, outputByteArray, 0, null, out clevent);
                if (!CheckErr(error, "Cl.clEnqueueReadImage"))
                {
                    return res;
                }

                // Test
                /*result = new Bitmap(img.Width, img.Height, PixelFormat.Format32bppArgb);
                bitmapData = result.LockBits(new Rectangle(0, 0, img.Width, img.Height),
                                     ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
                Marshal.Copy(outputByteArray, 0, bitmapData.Scan0, outputByteArray.Length);
                result.UnlockBits(bitmapData);

                res = true;
                return res;*/

                //****************************************************************************************************************************************************************
                // Execute 3rd kernel

                // Copy output to input
                Array.Copy(outputByteArray, inputByteArray, inputByteArray.Length);


                /*************
                 * Prepare kernel
                 *************/
                // Set the memory buffers to the kernel function
                error = OpenCL.Net.Cl.SetKernelArg(kernels[2], 0, (IntPtr)intPtrSize, inputImage2DBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 1"))
                {
                    return res;
                }

                error = OpenCL.Net.Cl.SetKernelArg(kernels[2], 1, (IntPtr)intPtrSize, inputArgumentBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 2"))
                {
                    return res;
                }

                error = OpenCL.Net.Cl.SetKernelArg(kernels[2], 2, (IntPtr)intPtrSize, outputImage2DBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 3"))
                {
                    return res;
                }

                /*******
                 * load and execute program
                 ******/

                error = OpenCL.Net.Cl.EnqueueWriteImage(_cmdQueue, inputImage2DBuffer, OpenCL.Net.Bool.True,
                   originPtr, regionPtr, (IntPtr)0, (IntPtr)0, inputByteArray, 0, null, out clevent);

                if (!CheckErr(error, "Cl.EnqueueWriteImage"))
                {
                    return res;
                }

                // Enqueue write the arguments data to GPU as integer array
                error = OpenCL.Net.Cl.EnqueueWriteBuffer(_cmdQueue, inputArgumentBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeOfInt * args.Length), args, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueWriteBuffer argument input"))
                {
                    return res;
                }

                //Execute  kernel (OpenCL code)
                error = OpenCL.Net.Cl.EnqueueNDRangeKernel(_cmdQueue, kernels[2], 2, null, workGroupSizePtr, null, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueNDRangeKernel"))
                {
                    return res;
                }
                //Wait for completion of all calculations on the GPU.
                error = OpenCL.Net.Cl.Finish(_cmdQueue);
                CheckErr(error, "Cl.Finish");

                //Read the processed image from GPU to RGBA
                error = OpenCL.Net.Cl.EnqueueReadImage(_cmdQueue, outputImage2DBuffer, OpenCL.Net.Bool.True,
                   originPtr, regionPtr, (IntPtr)0, (IntPtr)0, outputByteArray, 0, null, out clevent);
                if (!CheckErr(error, "Cl.clEnqueueReadImage"))
                {
                    return res;
                }

                //test
                result = new Bitmap(img.Width, img.Height, PixelFormat.Format32bppArgb);
                bitmapData = result.LockBits(new Rectangle(0, 0, img.Width, img.Height),
                                     ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
                Marshal.Copy(outputByteArray, 0, bitmapData.Scan0, outputByteArray.Length);
                result.UnlockBits(bitmapData);

                res = true;

                //****************************************************************************************************************************************************************
                //****************************************************************************************************************************************************************
                // Execute 4rd kernel

                // Copy output to input
                Array.Copy(outputByteArray, inputByteArray, inputByteArray.Length);


                /*************
                 * Prepare kernel
                 *************/
                // Set the memory buffers to the kernel function
                error = OpenCL.Net.Cl.SetKernelArg(kernels[3], 0, (IntPtr)intPtrSize, inputImage2DBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 1"))
                {
                    return res;
                }

                error = OpenCL.Net.Cl.SetKernelArg(kernels[3], 1, (IntPtr)intPtrSize, inputArgumentBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 2"))
                {
                    return res;
                }

                error = OpenCL.Net.Cl.SetKernelArg(kernels[3], 2, (IntPtr)intPtrSize, outputImage2DBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 3"))
                {
                    return res;
                }

                /*******
                 * load and execute program
                 ******/

                error = OpenCL.Net.Cl.EnqueueWriteImage(_cmdQueue, inputImage2DBuffer, OpenCL.Net.Bool.True,
                   originPtr, regionPtr, (IntPtr)0, (IntPtr)0, inputByteArray, 0, null, out clevent);

                if (!CheckErr(error, "Cl.EnqueueWriteImage"))
                {
                    return res;
                }

                // Enqueue write the arguments data to GPU as integer array
                error = OpenCL.Net.Cl.EnqueueWriteBuffer(_cmdQueue, inputArgumentBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeOfInt * args.Length), args, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueWriteBuffer argument input"))
                {
                    return res;
                }

                //Execute  kernel (OpenCL code)
                error = OpenCL.Net.Cl.EnqueueNDRangeKernel(_cmdQueue, kernels[3], 2, null, workGroupSizePtr, null, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueNDRangeKernel"))
                {
                    return res;
                }
                //Wait for completion of all calculations on the GPU.
                error = OpenCL.Net.Cl.Finish(_cmdQueue);
                CheckErr(error, "Cl.Finish");

                //Read the processed image from GPU to RGBA
                error = OpenCL.Net.Cl.EnqueueReadImage(_cmdQueue, outputImage2DBuffer, OpenCL.Net.Bool.True,
                   originPtr, regionPtr, (IntPtr)0, (IntPtr)0, outputByteArray, 0, null, out clevent);
                if (!CheckErr(error, "Cl.clEnqueueReadImage"))
                {
                    return res;
                }

                //****************************************************************************************************************************************************************



                /**************
                 * Generate the picture.
                 **************/

                result = new Bitmap(img.Width, img.Height, PixelFormat.Format32bppArgb);
                bitmapData = result.LockBits(new Rectangle(0, 0, img.Width, img.Height),
                                     ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
                Marshal.Copy(outputByteArray, 0, bitmapData.Scan0, outputByteArray.Length);
                result.UnlockBits(bitmapData);

                res = true;
            }
            catch (Exception e)
            {
                LogData(DEFAULT_EXCEPTION_MSG + e.Message, "HilightColor", EG_Utility.LOG_LEVEL.WARNING);
                if (_throwException)
                    throw e;
            }
            finally
            {
                //Clean up memory
                for (int i = 0; i < kernels.Length; i++)
                {
                    OpenCL.Net.Cl.ReleaseKernel(kernels[i]);
                }

                OpenCL.Net.Cl.ReleaseEvent(clevent);
                if (inputImage2DBuffer != null)
                    OpenCL.Net.Cl.ReleaseMemObject(inputImage2DBuffer);
                if (outputImage2DBuffer != null)
                    OpenCL.Net.Cl.ReleaseMemObject(outputImage2DBuffer);
                if (inputArgumentBuffer != null)
                    OpenCL.Net.Cl.ReleaseMemObject(inputArgumentBuffer);
            }


            return res;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="img"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public bool CannyFilter(byte[] img, int width, int height, out byte[] result)
        {
            bool res = false;

            int[] args = new int[] { width, height, img.Length };

            byte[] temp = new byte[img.Length];
            byte[] angles = new byte[img.Length];


            OpenCL.Net.Kernel[] kernels = null;// = new OpenCL.Net.Kernel()[3];
            OpenCL.Net.Event clevent = new OpenCL.Net.Event();

            OpenCL.Net.IMem inputImageBuffer = null;
            OpenCL.Net.IMem outputImageBuffer = null;
            OpenCL.Net.IMem angleImageBuffer = null;
            OpenCL.Net.IMem inputArgumentsBuffer = null;           
            OpenCL.Net.ErrorCode error;

            result = null;
            try
            {
                int intPtrSize = 0;
                int readOffset = 0;
                int sizeOfInt = sizeof(int);
                intPtrSize = Marshal.SizeOf(typeof(IntPtr));

                IntPtr[] workGroupSizePtr = new IntPtr[] { (IntPtr)(img.Length), (IntPtr)1, (IntPtr)1 };
                IntPtr[] LocalWorkSizePtr = new IntPtr[] { (IntPtr)(128), (IntPtr)1, (IntPtr)1 };

                result = new byte[img.Length];

                kernels = new OpenCL.Net.Kernel[]
                {
                    OpenCL.Net.Cl.CreateKernel(clPrograms[(int)PROGRAMS.CANNY_FILTER], "CannyFilterOp_1", out error),
                    OpenCL.Net.Cl.CreateKernel(clPrograms[(int)PROGRAMS.CANNY_FILTER], "CannyFilterOp_2", out error),
                    OpenCL.Net.Cl.CreateKernel(clPrograms[(int)PROGRAMS.CANNY_FILTER], "CannyFilterOp_3", out error),
                    OpenCL.Net.Cl.CreateKernel(clPrograms[(int)PROGRAMS.CANNY_FILTER], "CannyFilterOp_4", out error),
                };

                //////////////////
                // Create Kernel object with 'precompiled' program
                //////////////////
                //Create the required kernel (entry function)
                if (!CheckErr(error, "Cl.CreateKernel"))
                {
                    return res;
                }

                // Alocate OpenCl input memory buffer
                inputImageBuffer = OpenCL.Net.Cl.CreateBuffer(_context, OpenCL.Net.MemFlags.ReadOnly, (IntPtr)(img.Length * sizeof(byte)), out error);
                if (!CheckErr(error, "Cl.CreateBuffer inputImageBuffer"))
                {
                    return res;
                }
                // Allocate OpenCl input argument buffer
                inputArgumentsBuffer = OpenCL.Net.Cl.CreateBuffer(_context, OpenCL.Net.MemFlags.ReadOnly, (IntPtr)(sizeOfInt * args.Length), out error);
                if (!CheckErr(error, "Cl.CreateBuffer argumentStindeInd"))
                {
                    return res;
                }

                // Allocare OpenCL angle temporary buffer
                angleImageBuffer = OpenCL.Net.Cl.CreateBuffer(_context, OpenCL.Net.MemFlags.ReadWrite, (IntPtr)(img.Length * sizeof(byte)), out error);
                if (!CheckErr(error, "Cl.CreateBuffer angle temp bufer"))
                {
                    return res;
                }

                // Allocare OpenCL temporary buffer
                outputImageBuffer = OpenCL.Net.Cl.CreateBuffer(_context, OpenCL.Net.MemFlags.ReadWrite, (IntPtr)(result.Length * sizeof(byte)), out error);
                if (!CheckErr(error, "Cl.CreateBuffer temp bufer"))
                {
                    return res;
                }


                ////////////////////
                // Prepare kernels
                ///////////////////
                
                // Set the input image data to kernel 'CannyFilterOp_1'. 
                error = OpenCL.Net.Cl.SetKernelArg(kernels[0], 0, inputImageBuffer);
                if (!CheckErr(error, "[0]Cl.SetKernelArg 1"))
                {
                    return res;
                }
                // Set the arguments data to kernel 'CannyFilterOp_1'.
                error = OpenCL.Net.Cl.SetKernelArg(kernels[0], 1, inputArgumentsBuffer);
                if (!CheckErr(error, "[0]Cl.SetKernelArg 2"))
                {
                    return res;
                }

                // Set the output data to kernel 'CannyFilterOp_1'.
                error = OpenCL.Net.Cl.SetKernelArg(kernels[0], 2, outputImageBuffer);
                if (!CheckErr(error, "[0]Cl.SetKernelArg 3"))
                {
                    return res;
                }

                ///////////////
                // kernel 2
                ///////////////

                // Set the input image data to kernel 'CannyFilterOp_2'. 
                error = OpenCL.Net.Cl.SetKernelArg(kernels[1], 0, inputImageBuffer);
                if (!CheckErr(error, "[1]Cl.SetKernelArg 1"))
                {
                    return res;
                }
                // Set the arguments data to kernel 'CannyFilterOp_2'.
                error = OpenCL.Net.Cl.SetKernelArg(kernels[1], 1, inputArgumentsBuffer);
                if (!CheckErr(error, "[1]Cl.SetKernelArg 2"))
                {
                    return res;
                }

                // Set the output data to kernel 'CannyFilterOp_2'.
                error = OpenCL.Net.Cl.SetKernelArg(kernels[1], 2, outputImageBuffer);
                if (!CheckErr(error, "[1]Cl.SetKernelArg 3"))
                {
                    return res;
                }

                // Set the angle output data to kernel 'CannyFilterOp_2'.
                error = OpenCL.Net.Cl.SetKernelArg(kernels[1], 3, angleImageBuffer);
                if (!CheckErr(error, "[1]Cl.SetKernelArg 3"))
                {
                    return res;
                }


                ///////////////
                // kernel 3
                ///////////////

                // Set the input image data to kernel 'CannyFilterOp_3'. 
                error = OpenCL.Net.Cl.SetKernelArg(kernels[2], 0, inputImageBuffer);
                if (!CheckErr(error, "[2]Cl.SetKernelArg 1"))
                {
                    return res;
                }

                // Set the angle output data to kernel 'CannyFilterOp_3'.
                error = OpenCL.Net.Cl.SetKernelArg(kernels[2], 1, angleImageBuffer);
                if (!CheckErr(error, "[2]Cl.SetKernelArg 3"))
                {
                    return res;
                }

                // Set the arguments data to kernel 'CannyFilterOp_3'.
                error = OpenCL.Net.Cl.SetKernelArg(kernels[2], 2, inputArgumentsBuffer);
                if (!CheckErr(error, "[2]Cl.SetKernelArg 2"))
                {
                    return res;
                }

                // Set the output data to kernel 'CannyFilterOp_3'.
                error = OpenCL.Net.Cl.SetKernelArg(kernels[2], 3, outputImageBuffer);
                if (!CheckErr(error, "[2]Cl.SetKernelArg 3"))
                {
                    return res;
                }

                ///////////////
                // kernel 4
                ///////////////

                // Set the input image data to kernel 'CannyFilterOp_1'. 
                error = OpenCL.Net.Cl.SetKernelArg(kernels[3], 0, inputImageBuffer);
                if (!CheckErr(error, "[3]Cl.SetKernelArg 1"))
                {
                    return res;
                }
                // Set the arguments data to kernel 'CannyFilterOp_1'.
                error = OpenCL.Net.Cl.SetKernelArg(kernels[3], 1, inputArgumentsBuffer);
                if (!CheckErr(error, "[3]Cl.SetKernelArg 2"))
                {
                    return res;
                }

                // Set the output data to kernel 'CannyFilterOp_1'.
                error = OpenCL.Net.Cl.SetKernelArg(kernels[3], 2, outputImageBuffer);
                if (!CheckErr(error, "[3]Cl.SetKernelArg 3"))
                {
                    return res;
                }

                ///////////////////
                // load and execute CannyFilterOp_1
                ///////////////////

                // Enqueue write the image data to GPU as byte[] array
                error = OpenCL.Net.Cl.EnqueueWriteBuffer(_cmdQueue, inputImageBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeof(byte) * img.Length), img, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueWriteBuffer image input"))
                {
                    return res;
                }

                // Enqueue write the temp data to GPU as integer array
                error = OpenCL.Net.Cl.EnqueueWriteBuffer(_cmdQueue, inputArgumentsBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)(readOffset), (IntPtr)(sizeOfInt * args.Length), args, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueWriteBuffer args"))
                {
                    return res;
                }

                //Execute  kernel (OpenCL code)
                error = OpenCL.Net.Cl.EnqueueNDRangeKernel(_cmdQueue, kernels[0], 1, null, workGroupSizePtr, LocalWorkSizePtr, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueNDRangeKernel"))
                {
                    return res;
                }
                //Wait for completion of all calculations on the GPU.
                error = OpenCL.Net.Cl.Finish(_cmdQueue);
                CheckErr(error, "Cl.Finish");

                //Read the processed image from GPU to raw RGBA data byte[] array
                error = OpenCL.Net.Cl.EnqueueReadBuffer(_cmdQueue, outputImageBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeof(byte) * result.Length), result, 0, null, out clevent);
                if (!CheckErr(error, "Cl.clEnqueueReadImage"))
                {
                    return res;
                }

                

                /*******
                 * load and execute CannyFilterOp_2
                 ******/
                

                // Enqueue write the image data to GPU as byte[] array
                error = OpenCL.Net.Cl.EnqueueWriteBuffer(_cmdQueue, inputImageBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeof(byte) * result.Length), result, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueWriteBuffer image input"))
                {
                    return res;
                }

                // Enqueue write the temp data to GPU as integer array
                error = OpenCL.Net.Cl.EnqueueWriteBuffer(_cmdQueue, inputArgumentsBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)(readOffset), (IntPtr)(sizeOfInt * args.Length), args, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueWriteBuffer args"))
                {
                    return res;
                }

                //Execute  kernel (OpenCL code)
                error = OpenCL.Net.Cl.EnqueueNDRangeKernel(_cmdQueue, kernels[1], 1, null, workGroupSizePtr, LocalWorkSizePtr, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueNDRangeKernel"))
                {
                    return res;
                }
                //Wait for completion of all calculations on the GPU.
                error = OpenCL.Net.Cl.Finish(_cmdQueue);
                CheckErr(error, "Cl.Finish");

                //Read the processed image from GPU to raw RGBA data byte[] array
                error = OpenCL.Net.Cl.EnqueueReadBuffer(_cmdQueue, outputImageBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeof(byte) * img.Length), img, 0, null, out clevent);
                if (!CheckErr(error, "Cl.clEnqueueReadImage"))
                {
                    return res;
                }

                //Read the angle processed image from GPU to raw RGBA data byte[] array
                error = OpenCL.Net.Cl.EnqueueReadBuffer(_cmdQueue, angleImageBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeof(byte) * angles.Length), angles, 0, null, out clevent);
                if (!CheckErr(error, "Cl.clEnqueueReadImage"))
                {
                    return res;
                }

                //test
                /*Array.Copy(angles, result, result.Length);
                return true;*/

                /*******
                 * load and execute CannyFilterOp_3
                 ******/

                // Enqueue write the image data to GPU as byte[] array
                error = OpenCL.Net.Cl.EnqueueWriteBuffer(_cmdQueue, inputImageBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeof(byte) * img.Length), img, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueWriteBuffer image input"))
                {
                    return res;
                }

                // Enqueue write the anglr image data to GPU as byte[] array
                error = OpenCL.Net.Cl.EnqueueWriteBuffer(_cmdQueue, angleImageBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeof(byte) * angles.Length), angles, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueWriteBuffer image input"))
                {
                    return res;
                }

                args[1] = 14;
                args[2] = 3;

                // Enqueue write the temp data to GPU as integer array
                error = OpenCL.Net.Cl.EnqueueWriteBuffer(_cmdQueue, inputArgumentsBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)(readOffset), (IntPtr)(sizeOfInt * args.Length), args, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueWriteBuffer args"))
                {
                    return res;
                }

                //Execute  kernel (OpenCL code)
                error = OpenCL.Net.Cl.EnqueueNDRangeKernel(_cmdQueue, kernels[2], 1, null, workGroupSizePtr, LocalWorkSizePtr, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueNDRangeKernel"))
                {
                    return res;
                }
                //Wait for completion of all calculations on the GPU.
                error = OpenCL.Net.Cl.Finish(_cmdQueue);
                CheckErr(error, "Cl.Finish");

                //Read the processed image from GPU to raw RGBA data byte[] array
                error = OpenCL.Net.Cl.EnqueueReadBuffer(_cmdQueue, outputImageBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeof(byte) * result.Length), result, 0, null, out clevent);
                if (!CheckErr(error, "Cl.clEnqueueReadImage"))
                {
                    return res;
                }

                ///////////////////
                // load and execute CannyFilterOp_4
                ///////////////////

                Array.Copy(result, img, img.Length);

                // Enqueue write the image data to GPU as byte[] array
                error = OpenCL.Net.Cl.EnqueueWriteBuffer(_cmdQueue, inputImageBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeof(byte) * img.Length), img, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueWriteBuffer image input"))
                {
                    return res;
                }

                // Enqueue write the temp data to GPU as integer array
                error = OpenCL.Net.Cl.EnqueueWriteBuffer(_cmdQueue, inputArgumentsBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)(readOffset), (IntPtr)(sizeOfInt * args.Length), args, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueWriteBuffer args"))
                {
                    return res;
                }

                //Execute  kernel (OpenCL code)
                error = OpenCL.Net.Cl.EnqueueNDRangeKernel(_cmdQueue, kernels[3], 1, null, workGroupSizePtr, LocalWorkSizePtr, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueNDRangeKernel"))
                {
                    return res;
                }
                //Wait for completion of all calculations on the GPU.
                error = OpenCL.Net.Cl.Finish(_cmdQueue);
                CheckErr(error, "Cl.Finish");

                //Read the processed image from GPU to raw RGBA data byte[] array
                error = OpenCL.Net.Cl.EnqueueReadBuffer(_cmdQueue, outputImageBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeof(byte) * result.Length), result, 0, null, out clevent);
                if (!CheckErr(error, "Cl.clEnqueueReadImage"))
                {
                    return res;
                }


                res = true;
            }
            catch (Exception e)
            {
                LogData(DEFAULT_EXCEPTION_MSG + e.Message, "GetBmpFromBytes", EG_Utility.LOG_LEVEL.WARNING);
                if (_throwException)
                    throw e;
            }
            finally
            {
                //Clean up memory
                for(int i =0; i < kernels.Length; i++)
                {
                    OpenCL.Net.Cl.ReleaseKernel(kernels[i]);
                }
                

                OpenCL.Net.Cl.ReleaseEvent(clevent);
                if (outputImageBuffer != null)
                    OpenCL.Net.Cl.ReleaseMemObject(outputImageBuffer);
                if (inputImageBuffer != null)
                    OpenCL.Net.Cl.ReleaseMemObject(inputImageBuffer);
                if (inputArgumentsBuffer != null)
                    OpenCL.Net.Cl.ReleaseMemObject(inputArgumentsBuffer);
                if (angleImageBuffer != null)
                    OpenCL.Net.Cl.ReleaseMemObject(angleImageBuffer);
            }
            return res;
        }

    }
}
