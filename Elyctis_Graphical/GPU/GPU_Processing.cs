﻿
#define EMBEDED ///< Define use to tell if ressources are embeded

using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Imaging;
using System.Diagnostics;

namespace Elyctis_Graphical.GPU
{

    /// <summary>
    /// Class used to perform GPU operation.
    /// </summary>
    public partial class GPU_Processing : EG_Utility.Hardware
    {        

        #region Const

        /// <summary>
        /// OpenCL programs.
        /// </summary>
        [Flags]
        public enum PROGRAMS
        {
            /// <summary>
            /// Program used to change image format.
            /// </summary>
            IMAGE_FORMATING,
            /// <summary>
            /// Program used to perform operation on image.
            /// </summary>
            IMAGE_PROCESSING,
            /// <summary>
            /// Program use to perform calculation on image.
            /// </summary>
            IMAGE_COMPUTING,
            /// <summary>
            /// Program performing the canny filter.
            /// </summary>
            CANNY_FILTER,
            /// <summary>
            /// Value corresponding to the enumeration count. Mind to keep it at last index.
            /// </summary>
            COUNT
        }

        #endregion        

        #region Field

        private OpenCL.Net.Context _context; // Hardware context.
        private OpenCL.Net.Device _device;   // Hardware device that will be used. 
        private OpenCL.Net.CommandQueue _cmdQueue; // Command queue that will be used for hardware communication.

        private OpenCL.Net.Program[] clPrograms;    // List of compiled programs.

        #endregion

        #region Constructor

        /// <summary>
        /// Standard constructor for <see cref="GPU_Processing"/>.
        /// </summary>
        /// <remarks>
        /// Default log level is <see cref="EG_Utility.LOG_LEVEL.WARNING"/>.
        /// </remarks>
        /// <param name="delegateLogData">Delegate used to log data.</param>
        /// <param name="delegateInitialization">Delegate used to notify initialization events.</param>
        /// <param name="writeConsole">Does the logs should be write to the standard output.</param>
        /// <param name="throwException">Does exception should be thrown.</param>
        public GPU_Processing(EG_Utility.DelegateLogData delegateLogData = null, EG_Utility.DelegateInitialization delegateInitialization = null,
            bool writeConsole = false, bool throwException = false)
        {
            _isInitialized = false;

            _writeConsole = writeConsole;            
            _throwException = throwException;

            _logLevel = (int)EG_Utility.LOG_LEVEL.WARNING;

            _className = GetType().Name;

            _delegateLogData = delegateLogData;
            _delegateInitialization = delegateInitialization;

            _context = new OpenCL.Net.Context();
            _device = new OpenCL.Net.Device();
            _cmdQueue = new OpenCL.Net.CommandQueue();

            clPrograms = new OpenCL.Net.Program[(int)PROGRAMS.COUNT];
        }

        /// <summary>
        /// Finalizer for <see cref="GPU_Processing"/>.
        /// </summary>
        ~GPU_Processing()
        {
            try
            {
                // release programs
                for (int i = 0; i < clPrograms.Length; i++)
                {
                    try
                    {
                        clPrograms[i].Dispose();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }

                // Release command queue
                OpenCL.Net.Cl.ReleaseCommandQueue(_cmdQueue);
                // Device is managed ?
                // Release context
                _context.Dispose();

            }
            finally
            {
                Console.WriteLine("~OpenCl()");
            }
        }

        #endregion

        #region Method

        //TODO: Add possibility to select the device to use.

        /// <summary>
        /// Get the curent context in order to use Open CL. Select the first GPU found.
        /// </summary>
        public bool Setup()
        {
            bool res = false;
            OpenCL.Net.ErrorCode error;
            OpenCL.Net.Platform[] platforms = OpenCL.Net.Cl.GetPlatformIDs(out error);
            List<OpenCL.Net.Device> devicesList = new List<OpenCL.Net.Device>();

            try
            {
                LogData("'Setup' starts", "Setup", EG_Utility.LOG_LEVEL.VERBOSE);

                if (!CheckErr(error, "Cl.GetPlatformIDs"))
                {
                    // TODO: notify user
                    return res;
                }
                foreach (OpenCL.Net.Platform platform in platforms)
                {
                    string platformName = OpenCL.Net.Cl.GetPlatformInfo(platform, OpenCL.Net.PlatformInfo.Name, out error).ToString();
                    
                    LogData("Platform found: " + platformName, "Setup", EG_Utility.LOG_LEVEL.DEBUG);
                    CheckErr(error, "Cl.GetPlatformInfo");
                    //We will be looking only for GPU devices
                    foreach (OpenCL.Net.Device device in OpenCL.Net.Cl.GetDeviceIDs(platform, OpenCL.Net.DeviceType.Gpu, out error))
                    {
                        CheckErr(error, "Cl.GetDeviceIDs");
                        LogData("Device found: " + device.ToString(), "Setup", EG_Utility.LOG_LEVEL.DEBUG);
                        devicesList.Add(device);
                    }
                }

                if (devicesList.Count <= 0)
                {
                    LogData("No device found.", "Setup", EG_Utility.LOG_LEVEL.WARNING);
                    return res;
                }

                _device = devicesList[0];

                if (OpenCL.Net.Cl.GetDeviceInfo(_device, OpenCL.Net.DeviceInfo.ImageSupport,
                          out error).CastTo<OpenCL.Net.Bool>() == OpenCL.Net.Bool.False)
                {
                    LogData("No image support.", "Setup", EG_Utility.LOG_LEVEL.WARNING);
                    return res;
                }
                //Second parameter is amount of devices
                _context = OpenCL.Net.Cl.CreateContext(null, 1, new[] { _device }, ContextNotify, IntPtr.Zero, out error);
                if (!CheckErr(error, "Cl.CreateContext"))
                {
                    return res;
                }

                //Create a command queue, where all of the commands for execution will be added
                _cmdQueue = OpenCL.Net.Cl.CreateCommandQueue(_context, _device, (OpenCL.Net.CommandQueueProperties)0, out error);
                if (!CheckErr(error, "Cl.CreateCommandQueue"))
                {
                    return res;
                }

                res = true;
            }
            catch (Exception e)
            {
                LogData(DEFAULT_EXCEPTION_MSG + e.Message, "Setup", EG_Utility.LOG_LEVEL.ERROR);
                if (_throwException)
                    throw e;
            }
            finally
            {
                InitilizationUpdate(EG_Utility.INI_GPU_SETUP, res);                
            }
            return res;
        }

        /// <summary>
        /// Compile all kernel programs (see: <see cref="PROGRAMS"/>).
        /// </summary>
        /// <returns>Execution state.</returns>
        public bool CreatePrograms()
        {
            bool res = false;
            int i = 0;
            Thread[] thCompilation = new Thread[(int)PROGRAMS.COUNT];
            Assembly assembly = Assembly.GetExecutingAssembly();

            try
            {
                LogData("Program compilation starts", "CreatePrograms", EG_Utility.LOG_LEVEL.VERBOSE);

                // Create thread to compile the Image formating program
                thCompilation[(int)PROGRAMS.IMAGE_FORMATING] = new Thread(() =>{
                    // Compiling image formating program
                    string msg = "";
                    bool compRes = false;
                    Stopwatch st = new Stopwatch();
                    try
                    {
                        // Get the kernel code
                        string programPath = "";
                        string programSource = "";
                        OpenCL.Net.ErrorCode error;

                        st.Start();
                        LogData("'ImageFormating' compilation starts", "CreatePrograms [ImageFormating]", EG_Utility.LOG_LEVEL.VERBOSE);

#if EMBEDED
                        programPath = assembly.GetManifestResourceNames().Single(str => str.EndsWith("FormatImage.cl"));
                        using (Stream stream = assembly.GetManifestResourceStream(programPath))
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            programSource = reader.ReadToEnd();
                        }
#else
                        programPath = @"Kernels\FormatImage.cl";
                        if (!System.IO.File.Exists(programPath))
                        {
                            LogData("Program doesn't exist at path " + programPath, "CreatePrograms [FormatImage]", Utility.LOG_LEVEL.ERROR);
                            return res;
                        }
                        programSource = System.IO.File.ReadAllText(programPath);
#endif

                        // Create kernel programm (compile)
                        clPrograms[(int)PROGRAMS.IMAGE_FORMATING] = OpenCL.Net.Cl.CreateProgramWithSource(_context, 1, new[] { programSource }, null, out error);

                        CheckErr(error, "Cl.CreateProgramWithSource", "CreatePrograms [FormatImage]");
                        //Compile kernel source
                        error = OpenCL.Net.Cl.BuildProgram(clPrograms[(int)PROGRAMS.IMAGE_FORMATING], 1, new[] { _device }, string.Empty, null, IntPtr.Zero);
                        CheckErr(error, "Cl.BuildProgram", "CreatePrograms [FormatImage]");

                        //Check for any compilation errors
                        if (OpenCL.Net.Cl.GetProgramBuildInfo(clPrograms[(int)PROGRAMS.IMAGE_FORMATING], _device, OpenCL.Net.ProgramBuildInfo.Status, out error).CastTo<OpenCL.Net.BuildStatus>()
                                   != OpenCL.Net.BuildStatus.Success)
                        {
                            string temp = "Cl.GetProgramBuildInfo FAILED: " + OpenCL.Net.Cl.GetProgramBuildInfo(clPrograms[(int)PROGRAMS.IMAGE_FORMATING], _device, OpenCL.Net.ProgramBuildInfo.Log, out error).ToString();
                            CheckErr(error, temp, "CreatePrograms [FormatImage]");
                            msg = temp;
                            return;
                        }
                        compRes = true;
                        // Notify the result.
                        msg = "Compilation time: " + st.ElapsedMilliseconds + "ms";

                    }
                    catch (Exception e)
                    {
                        msg = DEFAULT_EXCEPTION_MSG + e.Message;
                        if (_throwException)
                            throw e;
                    }
                    finally
                    {
                        InitilizationUpdate(EG_Utility.INI_GPU_IMG_FORMATING, compRes, msg);
                        st.Stop();
                    }
                });
                // Create thread to compile the Image formating processing
                thCompilation[(int)PROGRAMS.IMAGE_PROCESSING] = new Thread(() =>
                {
                // Compiling image processing program
                try
                 {
                    // Get the kernel code
                    string programPath = "";
                    string programSource = "";
                    OpenCL.Net.ErrorCode error;

                    LogData("'ImageProcessing' compilation starts", "CreatePrograms [ImageProcessing]", EG_Utility.LOG_LEVEL.VERBOSE);

#if EMBEDED
                        programPath = assembly.GetManifestResourceNames().Single(str => str.EndsWith("ImageProcessing.cl"));
                    using (Stream stream = assembly.GetManifestResourceStream(programPath))
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        programSource = reader.ReadToEnd();
                    }
#else
                    programPath = @"Kernels\ImageProcessing.cl";
                    if (!System.IO.File.Exists(programPath))
                    {
                        LogData("Program doesn't exist at path " + programPath, "CreatePrograms [ImageProcessing]", Utility.LOG_LEVEL.ERROR);
                        return res;
                    }
                    programSource = System.IO.File.ReadAllText(programPath);
#endif

                    // Create kernel programm (compile)
                    clPrograms[(int)PROGRAMS.IMAGE_PROCESSING] = OpenCL.Net.Cl.CreateProgramWithSource(_context, 1, new[] { programSource }, null, out error);

                    CheckErr(error, "Cl.CreateProgramWithSource", "CreatePrograms [ImageProcessing]");
                    //Compile kernel source
                    error = OpenCL.Net.Cl.BuildProgram(clPrograms[(int)PROGRAMS.IMAGE_PROCESSING], 1, new[] { _device }, string.Empty, null, IntPtr.Zero);
                        CheckErr(error, "Cl.BuildProgram", "CreatePrograms [ImageProcessing]");
                    //Check for any compilation errors
                    if (OpenCL.Net.Cl.GetProgramBuildInfo(clPrograms[(int)PROGRAMS.IMAGE_PROCESSING], _device, OpenCL.Net.ProgramBuildInfo.Status, out error).CastTo<OpenCL.Net.BuildStatus>()
                            != OpenCL.Net.BuildStatus.Success)
                    {
                        string temp = OpenCL.Net.Cl.GetProgramBuildInfo(clPrograms[(int)PROGRAMS.IMAGE_PROCESSING], _device, OpenCL.Net.ProgramBuildInfo.Log, out error).ToString();
                        CheckErr(error, "Cl.GetProgramBuildInfo FAILED: " + temp, "CreatePrograms [ImageProcessing]");
                        InitilizationUpdate(EG_Utility.INI_GPU_IMG_PROCESSING, false, "Cl.GetProgramBuildInfo FAILED: " + temp);
                        return;
                    }
                        InitilizationUpdate(EG_Utility.INI_GPU_IMG_PROCESSING, true);
                    }
                    catch (Exception e)
                    {
                        InitilizationUpdate(EG_Utility.INI_GPU_IMG_PROCESSING, false, DEFAULT_EXCEPTION_MSG + e.Message);
                        if (_throwException)
                            throw e;
                    }
                    finally
                    {

                    }
                });
                // Create thread to compile the Image computing processing
                thCompilation[(int)PROGRAMS.IMAGE_COMPUTING] = new Thread(() =>
                {
                    // Compiling image processing program
                    try
                    {
                        // Get the kernel code
                        string programPath = "";
                        string programSource = "";
                        OpenCL.Net.ErrorCode error;

                        LogData("'ImageProcessing' compilation starts", "CreatePrograms [Imagecomputing]", EG_Utility.LOG_LEVEL.VERBOSE);

#if EMBEDED
                        programPath = assembly.GetManifestResourceNames().Single(str => str.EndsWith("ComputeImage.cl"));
                        using (Stream stream = assembly.GetManifestResourceStream(programPath))
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            programSource = reader.ReadToEnd();
                        }
#else
                    programPath = @"Kernels\ComputeImage.cl";
                    if (!System.IO.File.Exists(programPath))
                    {
                        LogData("Program doesn't exist at path " + programPath, "CreatePrograms [Imagecomputing]", Utility.LOG_LEVEL.ERROR);
                        return res;
                    }
                    programSource = System.IO.File.ReadAllText(programPath);
#endif

                        // Create kernel programm (compile)
                        clPrograms[(int)PROGRAMS.IMAGE_COMPUTING] = OpenCL.Net.Cl.CreateProgramWithSource(_context, 1, new[] { programSource }, null, out error);

                        CheckErr(error, "Cl.CreateProgramWithSource", "CreatePrograms [Imagecomputing]");
                        //Compile kernel source
                        error = OpenCL.Net.Cl.BuildProgram(clPrograms[(int)PROGRAMS.IMAGE_COMPUTING], 1, new[] { _device }, string.Empty, null, IntPtr.Zero);
                        CheckErr(error, "Cl.BuildProgram", "CreatePrograms [Imagecomputing]");
                        //Check for any compilation errors
                        if (OpenCL.Net.Cl.GetProgramBuildInfo(clPrograms[(int)PROGRAMS.IMAGE_COMPUTING], _device, OpenCL.Net.ProgramBuildInfo.Status, out error).CastTo<OpenCL.Net.BuildStatus>()
                                != OpenCL.Net.BuildStatus.Success)
                        {
                            string temp = OpenCL.Net.Cl.GetProgramBuildInfo(clPrograms[(int)PROGRAMS.IMAGE_COMPUTING], _device, OpenCL.Net.ProgramBuildInfo.Log, out error).ToString();
                            CheckErr(error, "Cl.GetProgramBuildInfo FAILED: " + temp, "CreatePrograms [Imagecomputing]");
                            InitilizationUpdate(EG_Utility.INI_GPU_IMG_COMPUTING, false, "Cl.GetProgramBuildInfo FAILED: " + temp);
                            return;
                        }

                        InitilizationUpdate(EG_Utility.INI_GPU_IMG_COMPUTING, true);
                    }
                    catch (Exception e)
                    {
                        InitilizationUpdate(EG_Utility.INI_GPU_IMG_COMPUTING, false, DEFAULT_EXCEPTION_MSG + e.Message);
                        if (_throwException)
                            throw e;
                    }
                    finally
                    {

                    }
                });
                // CANNY_FILTER
                thCompilation[(int)PROGRAMS.CANNY_FILTER] = new Thread(() =>
                {
                    // Compiling image processing program
                    try
                    {
                        // Get the kernel code
                        string programPath = "";
                        string programSource = "";
                        OpenCL.Net.ErrorCode error;

                        LogData("'ImageProcessing' compilation starts", "CreatePrograms [CannyFilter]", EG_Utility.LOG_LEVEL.VERBOSE);

#if EMBEDED
                        programPath = assembly.GetManifestResourceNames().Single(str => str.EndsWith("CannyFilter.cl"));
                        using (Stream stream = assembly.GetManifestResourceStream(programPath))
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            programSource = reader.ReadToEnd();
                        }
#else
                    programPath = @"Kernels\CannyFilter.cl";
                    if (!System.IO.File.Exists(programPath))
                    {
                        LogData("Program doesn't exist at path " + programPath, "CreatePrograms [CannyFilter]", Utility.LOG_LEVEL.ERROR);
                        return res;
                    }
                    programSource = System.IO.File.ReadAllText(programPath);
#endif

                        // Create kernel programm (compile)
                        clPrograms[(int)PROGRAMS.CANNY_FILTER] = OpenCL.Net.Cl.CreateProgramWithSource(_context, 1, new[] { programSource }, null, out error);

                        CheckErr(error, "Cl.CreateProgramWithSource", "CreatePrograms [CannyFilter]");
                        //Compile kernel source
                        error = OpenCL.Net.Cl.BuildProgram(clPrograms[(int)PROGRAMS.CANNY_FILTER], 1, new[] { _device }, string.Empty, null, IntPtr.Zero);
                        CheckErr(error, "Cl.BuildProgram", "CreatePrograms [CannyFilter]");
                        //Check for any compilation errors
                        if (OpenCL.Net.Cl.GetProgramBuildInfo(clPrograms[(int)PROGRAMS.CANNY_FILTER], _device, OpenCL.Net.ProgramBuildInfo.Status, out error).CastTo<OpenCL.Net.BuildStatus>()
                                != OpenCL.Net.BuildStatus.Success)
                        {
                            string temp = OpenCL.Net.Cl.GetProgramBuildInfo(clPrograms[(int)PROGRAMS.CANNY_FILTER], _device, OpenCL.Net.ProgramBuildInfo.Log, out error).ToString();
                            CheckErr(error, "Cl.GetProgramBuildInfo FAILED: " + temp, "CreatePrograms [CannyFilter]");
                            InitilizationUpdate(EG_Utility.INI_GPU_CANNY_FILTER, false, "Cl.GetProgramBuildInfo FAILED: " + temp);
                            return;
                        }

                        InitilizationUpdate(EG_Utility.INI_GPU_CANNY_FILTER, true);
                    }
                    catch (Exception e)
                    {
                        InitilizationUpdate(EG_Utility.INI_GPU_CANNY_FILTER, false, DEFAULT_EXCEPTION_MSG + e.Message);
                        if (_throwException)
                            throw e;
                    }
                    finally
                    {

                    }
                });
                // Start threads
                for (; i < thCompilation.Length; i++)
                {
                    thCompilation[i].Start();
                }

                // Wait threads end.
                for (i = 0; i < thCompilation.Length; i++)
                {
                    thCompilation[i].Join();
                }

                res = true;
                _isInitialized = true;                

            }
            catch (Exception e)
            {
                LogData(DEFAULT_EXCEPTION_MSG + e.Message, "CreatePrograms", EG_Utility.LOG_LEVEL.ERROR);
                if (_throwException)
                    throw e;
            }
            finally
            {
                InitilizationUpdate(EG_Utility.INI_GPU_INITIALIZATION_END, _isInitialized);
                // Set thread to null threads
                for (i = 0; i < thCompilation.Length; i++)
                {
                    thCompilation[i] = null;
                }
            }

            return res;
        }


        /// <summary>
        /// Check for OpenCL errors. 
        /// </summary>
        /// <param name="err">Error code.</param>
        /// <param name="msg">Message to display.</param>
        /// <param name="method">Method that triggered the checking.</param>
        /// <returns></returns>
        private bool CheckErr(OpenCL.Net.ErrorCode err, string msg, string method = "")
        {
            bool res = err == OpenCL.Net.ErrorCode.Success;
            if (!res)
            {
                LogData(msg, method, (int)EG_Utility.LOG_LEVEL.ERROR);
            }
            else
            {
                LogData("SUCCESS: " + msg, method, (int)EG_Utility.LOG_LEVEL.DEBUG);
            }
            return res;
        }

        /// <summary>
        /// Delegate method fo the context feedback.
        /// </summary>
        /// <param name="errInfo"></param>
        /// <param name="data"></param>
        /// <param name="cb"></param>
        /// <param name="userData"></param>
        protected virtual void ContextNotify(string errInfo, byte[] data, IntPtr cb, IntPtr userData)
        {
            LogData(errInfo, "ContextNotify", (int)EG_Utility.LOG_LEVEL.DEBUG);
        }

        #endregion
    }
}
