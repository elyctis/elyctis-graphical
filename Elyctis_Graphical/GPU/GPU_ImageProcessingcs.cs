﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace Elyctis_Graphical.GPU
{
    public partial class GPU_Processing : EG_Utility.Hardware
    {
        /// <summary>
        /// Perform a contrast scaling around provided 'mean' value on range of 'deepth'.
        /// </summary>
        /// <param name="data">Data to compute.</param>
        /// <param name="mean">Value to scale arround.</param>
        /// <param name="deepth">Range of scaling.</param>
        /// <param name="result">Generated result.</param>
        /// <returns>Execution result.</returns>
        public bool ContrastScaling(byte[] data, int mean, int deepth, out byte[] result)
        {
            bool res = false;
            result = new byte[data.Length];

            OpenCL.Net.Kernel kernel = new OpenCL.Net.Kernel();
            OpenCL.Net.Event clevent = new OpenCL.Net.Event();
            OpenCL.Net.IMem inputImageBuffer = null;
            OpenCL.Net.IMem outputImageBuffer = null;
            OpenCL.Net.IMem inputArguments = null;
            OpenCL.Net.ErrorCode error;

            try
            {
                int intPtrSize = Marshal.SizeOf(typeof(IntPtr));
                int readOffset = 0;
                int sizeOfFloat = sizeof(float);

                float[] arguments = { (mean - deepth), (float)(256.0 / (2 * deepth)) };

                /**************
                 * Create Kernel object with 'precompiled' program
                 *************/
                //Create the required kernel (entry function)
                kernel = OpenCL.Net.Cl.CreateKernel(clPrograms[(int)PROGRAMS.IMAGE_PROCESSING], "ContrastScaling", out error);
                if (!CheckErr(error, "Cl.CreateKernel"))
                {
                    return res;
                }

                // Alocate OpenCl input memory buffer
                inputImageBuffer = OpenCL.Net.Cl.CreateBuffer(_context, OpenCL.Net.MemFlags.ReadWrite, (IntPtr)data.Length, out error);

                if (!CheckErr(error, "Cl.CreateBuffer inputImageBuffer"))
                {
                    return res;
                }

                // Allocat input arguments buffer
                inputArguments = OpenCL.Net.Cl.CreateBuffer(_context, OpenCL.Net.MemFlags.ReadWrite, (IntPtr)(sizeOfFloat * arguments.Length), out error);
                if (!CheckErr(error, "Cl.CreateBuffer inputArguments"))
                {
                    return res;
                }

                // Allocat input arguments buffer
                outputImageBuffer = OpenCL.Net.Cl.CreateBuffer(_context, OpenCL.Net.MemFlags.ReadWrite, (IntPtr)(result.Length), out error);
                if (!CheckErr(error, "Cl.CreateBuffer outputImageBuffer"))
                {
                    return res;
                }

                /*************
                 * Prepare kernel
                 *************/
                //Pass the memory buffers to our kernel function
                error = OpenCL.Net.Cl.SetKernelArg(kernel, 0, (IntPtr)intPtrSize, inputImageBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 1"))
                {
                    return res;
                }

                error = OpenCL.Net.Cl.SetKernelArg(kernel, 1, (IntPtr)intPtrSize, inputArguments);
                if (!CheckErr(error, "Cl.SetKernelArg 2"))
                {
                    return res;
                }

                error = OpenCL.Net.Cl.SetKernelArg(kernel, 2, (IntPtr)intPtrSize, outputImageBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 3"))
                {
                    return res;
                }

                /*******
                 * load and execute program
                 ******/

                IntPtr[] workGroupSizePtr = new IntPtr[] { (IntPtr)(data.Length), (IntPtr)1, (IntPtr)1 };

                // Enqueue write the image data to GPU as byte[] array
                error = OpenCL.Net.Cl.EnqueueWriteBuffer(_cmdQueue, inputImageBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeof(byte) * data.Length), data, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueWriteBuffer image input"))
                {
                    return res;
                }

                // Enqueue write the argument data to GPU as float[] array
                error = OpenCL.Net.Cl.EnqueueWriteBuffer(_cmdQueue, inputArguments, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeOfFloat * arguments.Length), arguments, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueWriteBuffer argument input"))
                {
                    return res;
                }

                //Execute  kernel (OpenCL code)
                error = OpenCL.Net.Cl.EnqueueNDRangeKernel(_cmdQueue, kernel, 1, null, workGroupSizePtr, null, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueNDRangeKernel"))
                {
                    return res;
                }

                //Wait for completion of all calculations on the GPU.
                error = OpenCL.Net.Cl.Finish(_cmdQueue);
                CheckErr(error, "Cl.Finish");

                //Read the processed image from GPU to raw RGBA data byte[] array
                error = OpenCL.Net.Cl.EnqueueReadBuffer(_cmdQueue, outputImageBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeof(byte) * result.Length), result, 0, null, out clevent);
                if (!CheckErr(error, "Cl.clEnqueueReadImage"))
                {
                    return res;
                }

                res = true;
            }
            catch (Exception e)
            {
                LogData(DEFAULT_EXCEPTION_MSG + e.Message, "ContrastScaling", EG_Utility.LOG_LEVEL.WARNING);
                if (_throwException)
                    throw e;
            }
            finally
            {
                //Clean up memory
                OpenCL.Net.Cl.ReleaseKernel(kernel);

                OpenCL.Net.Cl.ReleaseEvent(clevent);
                if (outputImageBuffer != null)
                    OpenCL.Net.Cl.ReleaseMemObject(outputImageBuffer);
                if (inputImageBuffer != null)
                    OpenCL.Net.Cl.ReleaseMemObject(inputImageBuffer);
                if (inputArguments != null)
                    OpenCL.Net.Cl.ReleaseMemObject(inputArguments);
            }

            return res;
        }

        /// <summary>
        /// Perform a backgroup correction of the picture with the 'white' reference.
        /// </summary>
        /// <param name="reference">White picture reference.</param>
        /// <param name="picture">Picture to correct.</param>
        /// <param name="whiteTarget">Correction white value (= maximum value of 'reference')</param>
        /// <param name="result">Generated result.</param>
        /// <param name="offset">Histogram shifting.</param>
        /// <returns>Execution result.</returns>
        public bool BackGroundCorrection(byte[] reference, byte[] picture, byte whiteTarget, out byte[] result, byte offset = 0)
        {
            bool res = false;
            result = new byte[picture.Length];

            OpenCL.Net.Kernel kernel = new OpenCL.Net.Kernel();
            OpenCL.Net.Event clevent = new OpenCL.Net.Event();
            OpenCL.Net.IMem inputReferenceBuffer = null;
            OpenCL.Net.IMem inputImageBuffer = null;
            OpenCL.Net.IMem outputImageBuffer = null;
            OpenCL.Net.IMem inputArguments = null;
            OpenCL.Net.ErrorCode error;

            if (picture.Length != reference.Length)
            {
                return res;
            }

            try
            {
                int intPtrSize = Marshal.SizeOf(typeof(IntPtr));
                int readOffset = 0;
                int sizeOfInt = sizeof(int);

                // Generate argument array
                int[] arguments = { whiteTarget, offset };

                /**************
                * Create Kernel object with 'precompiled' program
                *************/
                //Create the required kernel (entry function)
                kernel = OpenCL.Net.Cl.CreateKernel(clPrograms[(int)PROGRAMS.IMAGE_PROCESSING], "BackGroundCorrection", out error);
                if (!CheckErr(error, "Cl.CreateKernel"))
                {
                    return res;
                }

                // Alocate OpenCl input memory buffer for reference image
                inputReferenceBuffer = OpenCL.Net.Cl.CreateBuffer(_context, OpenCL.Net.MemFlags.ReadWrite, (IntPtr)reference.Length, out error);

                if (!CheckErr(error, "Cl.CreateBuffer inputReferenceBuffer"))
                {
                    return res;
                }

                // Alocate OpenCl input memory buffer for input image
                inputImageBuffer = OpenCL.Net.Cl.CreateBuffer(_context, OpenCL.Net.MemFlags.ReadWrite, (IntPtr)picture.Length, out error);

                if (!CheckErr(error, "Cl.CreateBuffer inputImageBuffer"))
                {
                    return res;
                }

                // Allocat input arguments buffer
                inputArguments = OpenCL.Net.Cl.CreateBuffer(_context, OpenCL.Net.MemFlags.ReadWrite, (IntPtr)(sizeOfInt * arguments.Length), out error);
                if (!CheckErr(error, "Cl.CreateBuffer inputArguments"))
                {
                    return res;
                }

                // Allocat input arguments buffer
                outputImageBuffer = OpenCL.Net.Cl.CreateBuffer(_context, OpenCL.Net.MemFlags.ReadWrite, (IntPtr)(result.Length), out error);
                if (!CheckErr(error, "Cl.CreateBuffer outputImageBuffer"))
                {
                    return res;
                }

                /*************
                 * Prepare kernel
                 *************/
                //Pass the memory buffers to our kernel function
                error = OpenCL.Net.Cl.SetKernelArg(kernel, 0, (IntPtr)intPtrSize, inputReferenceBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 0"))
                {
                    return res;
                }

                error = OpenCL.Net.Cl.SetKernelArg(kernel, 1, (IntPtr)intPtrSize, inputImageBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 1"))
                {
                    return res;
                }

                error = OpenCL.Net.Cl.SetKernelArg(kernel, 2, (IntPtr)intPtrSize, inputArguments);
                if (!CheckErr(error, "Cl.SetKernelArg 2"))
                {
                    return res;
                }

                error = OpenCL.Net.Cl.SetKernelArg(kernel, 3, (IntPtr)intPtrSize, outputImageBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 3"))
                {
                    return res;
                }

                /*******
                 * load and execute program
                 ******/

                IntPtr[] workGroupSizePtr = new IntPtr[] { (IntPtr)(reference.Length), (IntPtr)1, (IntPtr)1 };

                // Enqueue write the image data to GPU as byte[] array
                error = OpenCL.Net.Cl.EnqueueWriteBuffer(_cmdQueue, inputReferenceBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeof(byte) * reference.Length), reference, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueWriteBuffer reference input"))
                {
                    return res;
                }

                // Enqueue write the image data to GPU as byte[] array
                error = OpenCL.Net.Cl.EnqueueWriteBuffer(_cmdQueue, inputImageBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeof(byte) * picture.Length), picture, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueWriteBuffer image input"))
                {
                    return res;
                }

                // Enqueue write the argument data to GPU as float[] array
                error = OpenCL.Net.Cl.EnqueueWriteBuffer(_cmdQueue, inputArguments, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeOfInt * arguments.Length), arguments, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueWriteBuffer argument input"))
                {
                    return res;
                }

                //Execute  kernel (OpenCL code)
                error = OpenCL.Net.Cl.EnqueueNDRangeKernel(_cmdQueue, kernel, 1, null, workGroupSizePtr, null, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueNDRangeKernel"))
                {
                    return res;
                }

                //Wait for completion of all calculations on the GPU.
                error = OpenCL.Net.Cl.Finish(_cmdQueue);
                CheckErr(error, "Cl.Finish");

                //Read the processed image from GPU to raw RGBA data byte[] array
                error = OpenCL.Net.Cl.EnqueueReadBuffer(_cmdQueue, outputImageBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeof(byte) * result.Length), result, 0, null, out clevent);
                if (!CheckErr(error, "Cl.clEnqueueReadImage"))
                {
                    return res;
                }

                res = true;

            }
            catch (Exception e)
            {
                LogData(DEFAULT_EXCEPTION_MSG + e.Message, "BackGroundCorrection", EG_Utility.LOG_LEVEL.WARNING);
                if (_throwException)
                    throw e;
            }
            finally
            {
                //Clean up memory
                OpenCL.Net.Cl.ReleaseKernel(kernel);

                OpenCL.Net.Cl.ReleaseEvent(clevent);
                if (inputReferenceBuffer != null)
                    OpenCL.Net.Cl.ReleaseMemObject(inputReferenceBuffer);
                if (outputImageBuffer != null)
                    OpenCL.Net.Cl.ReleaseMemObject(outputImageBuffer);
                if (inputImageBuffer != null)
                    OpenCL.Net.Cl.ReleaseMemObject(inputImageBuffer);
                if (inputArguments != null)
                    OpenCL.Net.Cl.ReleaseMemObject(inputArguments);
            }

            return res;
        }

        /// <summary>
        /// Do an adapatative threshold.
        /// </summary>
        /// <param name="data">Byte array of the picture.</param>
        /// <param name="width">Width of the picture.</param>
        /// <param name="height">Height of the picture.</param>
        /// <param name="percentage">Threshold percentage [0-100].</param>
        /// <param name="size_by2">Half size of the square comparaison matrix surounding the working pixel.</param>
        /// <param name="result">Generated result.</param>
        /// <returns>Execution result.</returns>
        public bool AdaptiveThreshold(byte[] data, int width, int height, int percentage, int size_by2, out byte[] result)
        {
            bool res = false;
            result = new byte[data.Length];

            OpenCL.Net.Kernel kernel = new OpenCL.Net.Kernel();
            OpenCL.Net.Event clevent = new OpenCL.Net.Event();
            OpenCL.Net.IMem inputImageBuffer = null;
            OpenCL.Net.IMem outputImageBuffer = null;
            OpenCL.Net.IMem inputArguments = null;
            OpenCL.Net.ErrorCode error;

            try
            {
                int intPtrSize = Marshal.SizeOf(typeof(IntPtr));
                int readOffset = 0;
                int sizeOfInt = sizeof(int);

                /*[0] Picture width [px]. |\
                * [1] Picture height [px]. |\
                * [2] size_by2 (used to get the square surounding pixel). |\
                * [3] Percentage (100 - percentageValue[0-100]). |\
                * [4] SizeLenght (2 * size_by2) * (2 * size_by2) * 100 (used to normalize result).
                */
                int[] arguments = { width, height, size_by2, 100 - percentage, (2 * size_by2) * (2 * size_by2) * 100 };

                /**************
                 * Create Kernel object with 'precompiled' program
                 *************/
                //Create the required kernel (entry function)
                kernel = OpenCL.Net.Cl.CreateKernel(clPrograms[(int)PROGRAMS.IMAGE_PROCESSING], "AdaptiveThresholding", out error);
                if (!CheckErr(error, "Cl.CreateKernel"))
                {
                    return res;
                }

                // Alocate OpenCl input memory buffer
                inputImageBuffer = OpenCL.Net.Cl.CreateBuffer(_context, OpenCL.Net.MemFlags.ReadOnly, (IntPtr)data.Length, out error);
                if (!CheckErr(error, "Cl.CreateBuffer inputImageBuffer"))
                {
                    return res;
                }

                // Allocat input arguments buffer
                inputArguments = OpenCL.Net.Cl.CreateBuffer(_context, OpenCL.Net.MemFlags.ReadOnly, (IntPtr)(sizeOfInt * arguments.Length), out error);
                if (!CheckErr(error, "Cl.CreateBuffer inputArguments"))
                {
                    return res;
                }

                // Allocat input arguments buffer
                outputImageBuffer = OpenCL.Net.Cl.CreateBuffer(_context, OpenCL.Net.MemFlags.ReadWrite, (IntPtr)(result.Length), out error);
                if (!CheckErr(error, "Cl.CreateBuffer outputImageBuffer"))
                {
                    return res;
                }

                /*************
                 * Prepare kernel
                 *************/
                //Pass the memory buffers to our kernel function
                error = OpenCL.Net.Cl.SetKernelArg(kernel, 0, (IntPtr)intPtrSize, inputImageBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 1"))
                {
                    return res;
                }

                error = OpenCL.Net.Cl.SetKernelArg(kernel, 1, (IntPtr)intPtrSize, inputArguments);
                if (!CheckErr(error, "Cl.SetKernelArg 2"))
                {
                    return res;
                }

                error = OpenCL.Net.Cl.SetKernelArg(kernel, 2, (IntPtr)intPtrSize, outputImageBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 3"))
                {
                    return res;
                }

                /*******
                 * load and execute program
                 ******/

                IntPtr[] workGroupSizePtr = new IntPtr[] { (IntPtr)(result.Length), (IntPtr)1, (IntPtr)1 };

                // Enqueue write the image data to GPU as byte[] array
                error = OpenCL.Net.Cl.EnqueueWriteBuffer(_cmdQueue, inputImageBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeof(byte) * data.Length), data, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueWriteBuffer image input"))
                {
                    return res;
                }

                // Enqueue write the argument data to GPU as float[] array
                error = OpenCL.Net.Cl.EnqueueWriteBuffer(_cmdQueue, inputArguments, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeOfInt * arguments.Length), arguments, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueWriteBuffer argument input"))
                {
                    return res;
                }

                //Execute  kernel (OpenCL code)
                error = OpenCL.Net.Cl.EnqueueNDRangeKernel(_cmdQueue, kernel, 1, null, workGroupSizePtr, null, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueNDRangeKernel"))
                {
                    return res;
                }

                //Wait for completion of all calculations on the GPU.
                error = OpenCL.Net.Cl.Finish(_cmdQueue);
                CheckErr(error, "Cl.Finish");

                //Read the processed image from GPU to raw RGBA data byte[] array
                error = OpenCL.Net.Cl.EnqueueReadBuffer(_cmdQueue, outputImageBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeof(byte) * result.Length), result, 0, null, out clevent);
                if (!CheckErr(error, "Cl.clEnqueueReadImage"))
                {
                    return res;
                }

                res = true;
            }
            catch (Exception e)
            {
                LogData(DEFAULT_EXCEPTION_MSG + e.Message, "AdaptiveThreshold", EG_Utility.LOG_LEVEL.WARNING);
                if (_throwException)
                    throw e;
            }
            finally
            {
                //Clean up memory
                OpenCL.Net.Cl.ReleaseKernel(kernel);

                OpenCL.Net.Cl.ReleaseEvent(clevent);
                if (outputImageBuffer != null)
                    OpenCL.Net.Cl.ReleaseMemObject(outputImageBuffer);
                if (inputImageBuffer != null)
                    OpenCL.Net.Cl.ReleaseMemObject(inputImageBuffer);
                if (inputArguments != null)
                    OpenCL.Net.Cl.ReleaseMemObject(inputArguments);
            }

            return res;
        }

        /// <summary>
        /// Do an adapatative threshold using pseudo integral.
        /// </summary>
        /// <remarks>
        /// The pseudo integral is computed with the CPU before sending data to GPU.
        /// </remarks>
        /// <param name="data">Byte array of the picture.</param>
        /// <param name="width">Width of the picture.</param>
        /// <param name="height">Height of the picture.</param>
        /// <param name="percentage">Threshold percentage [0-100].</param>
        /// <param name="size_by2">Half size of the square comparaison matrix surounding the working pixel.</param>
        /// <param name="result">Generated result.</param>
        /// <returns>Execution result.</returns>
        public bool AdaptiveThresholdI(byte[] data, int width, int height, int percentage, int size_by2, out byte[] result)
        {
            bool res = false;
            result = new byte[data.Length];
            int[] integral = new int[data.Length];

            OpenCL.Net.Kernel kernel = new OpenCL.Net.Kernel();
            OpenCL.Net.Event clevent = new OpenCL.Net.Event();
            OpenCL.Net.IMem inputImageBuffer = null;
            OpenCL.Net.IMem inputIntegralBuffer = null;
            OpenCL.Net.IMem outputImageBuffer = null;
            OpenCL.Net.IMem inputArguments = null;
            OpenCL.Net.ErrorCode error;

            try
            {
                int intPtrSize = Marshal.SizeOf(typeof(IntPtr));
                int readOffset = 0;
                int sizeOfInt = sizeof(int);

                // Compute the integral buffer
                int[] arguments = { 0, 0 }; // [0] => sum | [1] array position.
                // Create the integral matrix
                for (int x = 0; x < width; x++)
                {
                    arguments[0] = 0;
                    for (int y = 0; y < height; y++)
                    {
                        arguments[1] = x + y * width;
                        arguments[0] += data[arguments[1]];
                        if (x == 0)
                        {
                            integral[arguments[1]] = arguments[0];
                        }
                        else
                        {
                            integral[arguments[1]] = integral[arguments[1] - 1] + arguments[0];
                        }
                    }
                }

                /*[0] Picture width [px]. |\
                * [1] Picture height. |\
                * [2] size_by2 (used to get the square surounding pixel). |\
                * [3] Percentage (100 - percentageValue[0-100]). |\
                * [4] SizeLenght (2 * size_by2) * (2 * size_by2) * 100 (used to normalize result).
                */
                arguments = new int[] { width, height, size_by2, 100 - percentage, (2 * size_by2) * (2 * size_by2) * 100 };

                /**************
                 * Create Kernel object with 'precompiled' program
                 *************/
                //Create the required kernel (entry function)
                kernel = OpenCL.Net.Cl.CreateKernel(clPrograms[(int)PROGRAMS.IMAGE_PROCESSING], "AdaptiveThresholdingI", out error);
                if (!CheckErr(error, "Cl.CreateKernel"))
                {
                    return res;
                }

                // Alocate OpenCl input memory buffer
                inputImageBuffer = OpenCL.Net.Cl.CreateBuffer(_context, OpenCL.Net.MemFlags.ReadOnly, (IntPtr)data.Length, out error);

                if (!CheckErr(error, "Cl.CreateBuffer inputImageBuffer"))
                {
                    return res;
                }

                // Alocate OpenCl integral input memory buffer
                inputIntegralBuffer = OpenCL.Net.Cl.CreateBuffer(_context, OpenCL.Net.MemFlags.ReadOnly, (IntPtr)(sizeOfInt * integral.Length), out error);

                if (!CheckErr(error, "Cl.CreateBuffer inputImageBuffer"))
                {
                    return res;
                }

                // Allocat input arguments buffer
                inputArguments = OpenCL.Net.Cl.CreateBuffer(_context, OpenCL.Net.MemFlags.ReadOnly, (IntPtr)(sizeOfInt * arguments.Length), out error);
                if (!CheckErr(error, "Cl.CreateBuffer inputArguments"))
                {
                    return res;
                }

                // Allocat input arguments buffer
                outputImageBuffer = OpenCL.Net.Cl.CreateBuffer(_context, OpenCL.Net.MemFlags.ReadWrite, (IntPtr)(result.Length), out error);
                if (!CheckErr(error, "Cl.CreateBuffer outputImageBuffer"))
                {
                    return res;
                }

                /*************
                 * Prepare kernel
                 *************/
                //Pass the memory buffers to our kernel function
                error = OpenCL.Net.Cl.SetKernelArg(kernel, 0, (IntPtr)intPtrSize, inputImageBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 0"))
                {
                    return res;
                }

                error = OpenCL.Net.Cl.SetKernelArg(kernel, 1, (IntPtr)intPtrSize, inputIntegralBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 1"))
                {
                    return res;
                }

                error = OpenCL.Net.Cl.SetKernelArg(kernel, 2, (IntPtr)intPtrSize, inputArguments);
                if (!CheckErr(error, "Cl.SetKernelArg 2"))
                {
                    return res;
                }

                error = OpenCL.Net.Cl.SetKernelArg(kernel, 3, (IntPtr)intPtrSize, outputImageBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 3"))
                {
                    return res;
                }

                /*******
                 * load and execute program
                 ******/

                IntPtr[] workGroupSizePtr = new IntPtr[] { (IntPtr)(data.Length), (IntPtr)1, (IntPtr)1 };

                // Enqueue write the image data to GPU as byte[] array
                error = OpenCL.Net.Cl.EnqueueWriteBuffer(_cmdQueue, inputImageBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeof(byte) * data.Length), data, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueWriteBuffer image input"))
                {
                    return res;
                }

                // Enqueue write the integral image data to GPU as int[] array
                error = OpenCL.Net.Cl.EnqueueWriteBuffer(_cmdQueue, inputIntegralBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)((sizeOfInt * integral.Length)), integral, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueWriteBuffer image input"))
                {
                    return res;
                }

                // Enqueue write the argument data to GPU as int[] array
                error = OpenCL.Net.Cl.EnqueueWriteBuffer(_cmdQueue, inputArguments, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeOfInt * arguments.Length), arguments, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueWriteBuffer argument input"))
                {
                    return res;
                }

                //Execute  kernel (OpenCL code)
                error = OpenCL.Net.Cl.EnqueueNDRangeKernel(_cmdQueue, kernel, 1, null, workGroupSizePtr, null, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueNDRangeKernel"))
                {
                    return res;
                }

                //Wait for completion of all calculations on the GPU.
                error = OpenCL.Net.Cl.Finish(_cmdQueue);
                CheckErr(error, "Cl.Finish");

                //Read the processed image from GPU to raw RGBA data byte[] array
                error = OpenCL.Net.Cl.EnqueueReadBuffer(_cmdQueue, outputImageBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeof(byte) * result.Length), result, 0, null, out clevent);
                if (!CheckErr(error, "Cl.clEnqueueReadImage"))
                {
                    return res;
                }

                res = true;
            }
            catch (Exception e)
            {
                LogData(DEFAULT_EXCEPTION_MSG + e.Message, "AdaptiveThresholdI", EG_Utility.LOG_LEVEL.WARNING);
                if (_throwException)
                    throw e;
            }
            finally
            {
                //Clean up memory
                OpenCL.Net.Cl.ReleaseKernel(kernel);

                OpenCL.Net.Cl.ReleaseEvent(clevent);
                if (outputImageBuffer != null)
                    OpenCL.Net.Cl.ReleaseMemObject(outputImageBuffer);
                if (inputIntegralBuffer != null)
                    OpenCL.Net.Cl.ReleaseMemObject(inputIntegralBuffer);
                if (inputImageBuffer != null)
                    OpenCL.Net.Cl.ReleaseMemObject(inputImageBuffer);
                if (inputArguments != null)
                    OpenCL.Net.Cl.ReleaseMemObject(inputArguments);
            }

            return res;
        }

        /// <summary>
        /// Set similar color to each pixel in range of provided 'grayIndexes' value.
        /// </summary>
        /// <param name="img">Input image.</param>
        /// <param name="range">Range around each values of 'grayIndexes'.</param>
        /// <param name="grayIndexes">Gray level to hilight.</param>
        /// <param name="result">Result image.</param>
        /// <returns>Execution result.</returns>
        public bool HilightColor(Bitmap img, int range, byte[] grayIndexes, out Bitmap result)
        {
            bool res = false;

            OpenCL.Net.Kernel kernel = new OpenCL.Net.Kernel();
            OpenCL.Net.Event clevent = new OpenCL.Net.Event();
            OpenCL.Net.IMem inputImage2DBuffer = null;
            OpenCL.Net.IMem outputImage2DBuffer = null;
            OpenCL.Net.IMem inputArgumentBuffer = null;
            OpenCL.Net.ErrorCode error;

            result = null;

            try
            {
                int intPtrSize = 0;
                int readOffset = 0;
                int imageStride = 0;
                int imageSize = 0;
                int[] args = new int[2 + grayIndexes.Length];
                int sizeOfInt = sizeof(int);
                intPtrSize = Marshal.SizeOf(typeof(IntPtr));
                //Image's RGBA data converted to an unmanaged[] array
                byte[] inputByteArray;
                byte[] outputByteArray;

                /*****************
                *Create Kernel object with 'precompiled' program
                * ************/
               //Create the required kernel (entry function)
               kernel = OpenCL.Net.Cl.CreateKernel(clPrograms[(int)PROGRAMS.IMAGE_PROCESSING], "HilightColors", out error);
                if (!CheckErr(error, "Cl.CreateKernel"))
                {
                    return res;
                }

                /**************
                 * Read picture and prepare it to be usable for GPU
                 **************/
                OpenCL.Net.ImageFormat clImageFormat = new OpenCL.Net.ImageFormat(OpenCL.Net.ChannelOrder.RGBA, OpenCL.Net.ChannelType.Unsigned_Int8);

                //Get raw pixel data of the bitmap
                //The format should match the format of clImageFormat
                BitmapData bitmapData = img.LockBits(new Rectangle(0, 0, img.Width, img.Height),
                                     ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
                imageStride = bitmapData.Stride;

                imageSize = bitmapData.Stride * bitmapData.Height;
                //Copy the raw bitmap data to an unmanaged byte[] array
                inputByteArray = new byte[imageSize];
                outputByteArray = new byte[imageSize];
                Marshal.Copy(bitmapData.Scan0, inputByteArray, 0, imageSize);

                img.UnlockBits(bitmapData);

                // Perpare arguments
                args[0] = range/2;
                args[1] = grayIndexes.Length;
                for(int i = 0; i < grayIndexes.Length; i++)
                {
                    args[2 + i] = grayIndexes[i];
                }

                // Allocate OpenCL image memory buffer
                inputImage2DBuffer = OpenCL.Net.Cl.CreateImage2D(_context, OpenCL.Net.MemFlags.CopyHostPtr | OpenCL.Net.MemFlags.ReadOnly, clImageFormat,
                                                    (IntPtr)img.Width, (IntPtr)img.Height,
                                                    (IntPtr)0, inputByteArray, out error);

                if (!CheckErr(error, "Cl.CreateImage2D input"))
                {
                    return res;
                }
                // Allocate OpenCl input argument buffer
                inputArgumentBuffer = OpenCL.Net.Cl.CreateBuffer(_context, OpenCL.Net.MemFlags.ReadOnly, (IntPtr)(sizeOfInt * args.Length), out error);
                if (!CheckErr(error, "Cl.CreateBuffer argumentStindeInd"))
                {
                    return res;
                }

                // Allocate OpenCL output image memory buffer
                outputImage2DBuffer = OpenCL.Net.Cl.CreateImage2D(_context, OpenCL.Net.MemFlags.CopyHostPtr |
                    OpenCL.Net.MemFlags.WriteOnly, clImageFormat, (IntPtr)img.Width,
                    (IntPtr)img.Height, (IntPtr)0, outputByteArray, out error);

                if (!CheckErr(error, "Cl.CreateImage2D outputImage2DBuffer"))
                {
                    return res;
                }

                /*************
                 * Prepare kernel
                 *************/
                // Set the memory buffers to the kernel function
                error = OpenCL.Net.Cl.SetKernelArg(kernel, 0, (IntPtr)intPtrSize, inputImage2DBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 1"))
                {
                    return res;
                }

                error = OpenCL.Net.Cl.SetKernelArg(kernel, 1, (IntPtr)intPtrSize, inputArgumentBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 2"))
                {
                    return res;
                }

                error = OpenCL.Net.Cl.SetKernelArg(kernel, 2, (IntPtr)intPtrSize, outputImage2DBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 3"))
                {
                    return res;
                }

                /*******
                 * load and execute program
                 ******/

                IntPtr[] originPtr = new IntPtr[] { (IntPtr)0, (IntPtr)0, (IntPtr)0 };    //x, y, z
                IntPtr[] regionPtr = new IntPtr[] { (IntPtr)img.Width, (IntPtr)img.Height, (IntPtr)1 };    //x, y, z
                IntPtr[] workGroupSizePtr = new IntPtr[] { (IntPtr)img.Width, (IntPtr)img.Height, (IntPtr)1 };
                error = OpenCL.Net.Cl.EnqueueWriteImage(_cmdQueue, inputImage2DBuffer, OpenCL.Net.Bool.True,
                   originPtr, regionPtr, (IntPtr)0, (IntPtr)0, inputByteArray, 0, null, out clevent);

                if (!CheckErr(error, "Cl.EnqueueWriteImage"))
                {
                    return res;
                }

                // Enqueue write the arguments data to GPU as integer array
                error = OpenCL.Net.Cl.EnqueueWriteBuffer(_cmdQueue, inputArgumentBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeOfInt * args.Length), args, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueWriteBuffer argument input"))
                {
                    return res;
                }



                //Execute  kernel (OpenCL code)
                error = OpenCL.Net.Cl.EnqueueNDRangeKernel(_cmdQueue, kernel, 2, null, workGroupSizePtr, null, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueNDRangeKernel"))
                {
                    return res;
                }

                //Wait for completion of all calculations on the GPU.
                error = OpenCL.Net.Cl.Finish(_cmdQueue);
                CheckErr(error, "Cl.Finish");


                //Read the processed image from GPU to RGBA
                error = OpenCL.Net.Cl.EnqueueReadImage(_cmdQueue, outputImage2DBuffer, OpenCL.Net.Bool.True,
                   originPtr, regionPtr, (IntPtr)0, (IntPtr)0, outputByteArray, 0, null, out clevent);
                if (!CheckErr(error, "Cl.clEnqueueReadImage"))
                {
                    return res;
                }

                /**************
                 * Generate the picture.
                 **************/

                result = new Bitmap(img.Width, img.Height, PixelFormat.Format32bppArgb);

                bitmapData = result.LockBits(new Rectangle(0, 0, img.Width, img.Height),
                                     ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);

                Marshal.Copy(outputByteArray, 0, bitmapData.Scan0, outputByteArray.Length);

                result.UnlockBits(bitmapData);

                res = true;
            }
            catch (Exception e)
            {
                LogData(DEFAULT_EXCEPTION_MSG + e.Message, "HilightColor", EG_Utility.LOG_LEVEL.WARNING);
                if (_throwException)
                    throw e;
            }
            finally
            {
                //Clean up memory
                OpenCL.Net.Cl.ReleaseKernel(kernel);

                OpenCL.Net.Cl.ReleaseEvent(clevent);
                if (inputImage2DBuffer != null)
                    OpenCL.Net.Cl.ReleaseMemObject(inputImage2DBuffer);
                if (outputImage2DBuffer != null)
                    OpenCL.Net.Cl.ReleaseMemObject(outputImage2DBuffer);
                if (inputArgumentBuffer != null)
                    OpenCL.Net.Cl.ReleaseMemObject(inputArgumentBuffer);
            }
            return res;
        }

        /// <summary>
        /// Perform teh convolution product to image with provided 3*3 float mask.
        /// </summary>
        /// <param name="img">Input image.</param>
        /// <param name="mask">Mask to apply as 3*3 matrix (ex: float[]{0f, 0f , 0f,   0f, 1f, 0f,   0f, 0f, 0f }).</param>
        /// <param name="result">Result image.</param>
        /// <returns>Execution result.</returns>
        public bool ConvolutionProduct_3x3(Bitmap img, float[] mask, out Bitmap result)
        {
            bool res = false;

            OpenCL.Net.Kernel kernel = new OpenCL.Net.Kernel();
            OpenCL.Net.Event clevent = new OpenCL.Net.Event();
            OpenCL.Net.IMem inputImage2DBuffer = null;
            OpenCL.Net.IMem outputImage2DBuffer = null;
            OpenCL.Net.IMem inputArgumentBuffer = null;
            OpenCL.Net.ErrorCode error;

            result = null;

            try
            {
                int intPtrSize = 0;
                int readOffset = 0;
                int imageStride = 0;
                int imageSize = 0;
                int sizeOfFloat = sizeof(float);
                intPtrSize = Marshal.SizeOf(typeof(IntPtr));
                //Image's RGBA data converted to an unmanaged[] array
                byte[] inputByteArray;
                byte[] outputByteArray;

                /*****************
                *Create Kernel object with 'precompiled' program
                * ************/
                //Create the required kernel (entry function)
                kernel = OpenCL.Net.Cl.CreateKernel(clPrograms[(int)PROGRAMS.IMAGE_COMPUTING], "ConvolutionImage", out error);
                if (!CheckErr(error, "Cl.CreateKernel"))
                {
                    return res;
                }

                /**************
                 * Read picture and prepare it to be usable for GPU
                 **************/
                OpenCL.Net.ImageFormat clImageFormat = new OpenCL.Net.ImageFormat(OpenCL.Net.ChannelOrder.RGBA, OpenCL.Net.ChannelType.Unsigned_Int8);

                //Get raw pixel data of the bitmap
                //The format should match the format of clImageFormat
                BitmapData bitmapData = img.LockBits(new Rectangle(0, 0, img.Width, img.Height),
                                     ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
                imageStride = bitmapData.Stride;

                imageSize = bitmapData.Stride * bitmapData.Height;
                //Copy the raw bitmap data to an unmanaged byte[] array
                inputByteArray = new byte[imageSize];
                outputByteArray = new byte[imageSize];
                Marshal.Copy(bitmapData.Scan0, inputByteArray, 0, imageSize);

                img.UnlockBits(bitmapData);

                // Allocate OpenCL image memory buffer
                inputImage2DBuffer = OpenCL.Net.Cl.CreateImage2D(_context, OpenCL.Net.MemFlags.CopyHostPtr | OpenCL.Net.MemFlags.ReadOnly, clImageFormat,
                                                    (IntPtr)img.Width, (IntPtr)img.Height,
                                                    (IntPtr)0, inputByteArray, out error);

                if (!CheckErr(error, "Cl.CreateImage2D input"))
                {
                    return res;
                }
                // Allocate OpenCl input argument buffer
                inputArgumentBuffer = OpenCL.Net.Cl.CreateBuffer(_context, OpenCL.Net.MemFlags.ReadOnly, (IntPtr)(sizeOfFloat * mask.Length), out error);
                if (!CheckErr(error, "Cl.CreateBuffer argumentStindeInd"))
                {
                    return res;
                }

                // Allocate OpenCL output image memory buffer
                outputImage2DBuffer = OpenCL.Net.Cl.CreateImage2D(_context, OpenCL.Net.MemFlags.CopyHostPtr |
                    OpenCL.Net.MemFlags.WriteOnly, clImageFormat, (IntPtr)img.Width,
                    (IntPtr)img.Height, (IntPtr)0, outputByteArray, out error);

                if (!CheckErr(error, "Cl.CreateImage2D outputImage2DBuffer"))
                {
                    return res;
                }

                /*************
                 * Prepare kernel
                 *************/
                // Set the memory buffers to the kernel function
                error = OpenCL.Net.Cl.SetKernelArg(kernel, 0, (IntPtr)intPtrSize, inputImage2DBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 1"))
                {
                    return res;
                }

                error = OpenCL.Net.Cl.SetKernelArg(kernel, 1, (IntPtr)intPtrSize, inputArgumentBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 2"))
                {
                    return res;
                }

                error = OpenCL.Net.Cl.SetKernelArg(kernel, 2, (IntPtr)intPtrSize, outputImage2DBuffer);
                if (!CheckErr(error, "Cl.SetKernelArg 3"))
                {
                    return res;
                }

                /*******
                 * load and execute program
                 ******/

                IntPtr[] originPtr = new IntPtr[] { (IntPtr)0, (IntPtr)0, (IntPtr)0 };    //x, y, z
                IntPtr[] regionPtr = new IntPtr[] { (IntPtr)img.Width, (IntPtr)img.Height, (IntPtr)1 };    //x, y, z
                IntPtr[] workGroupSizePtr = new IntPtr[] { (IntPtr)img.Width, (IntPtr)img.Height, (IntPtr)1 };
                error = OpenCL.Net.Cl.EnqueueWriteImage(_cmdQueue, inputImage2DBuffer, OpenCL.Net.Bool.True,
                   originPtr, regionPtr, (IntPtr)0, (IntPtr)0, inputByteArray, 0, null, out clevent);

                if (!CheckErr(error, "Cl.EnqueueWriteImage"))
                {
                    return res;
                }

                // Enqueue write the arguments data to GPU as integer array
                error = OpenCL.Net.Cl.EnqueueWriteBuffer(_cmdQueue, inputArgumentBuffer, OpenCL.Net.Bool.True,
                    (IntPtr)readOffset, (IntPtr)(sizeOfFloat * mask.Length), mask, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueWriteBuffer argument input"))
                {
                    return res;
                }



                //Execute  kernel (OpenCL code)
                error = OpenCL.Net.Cl.EnqueueNDRangeKernel(_cmdQueue, kernel, 2, null, workGroupSizePtr, null, 0, null, out clevent);
                if (!CheckErr(error, "Cl.EnqueueNDRangeKernel"))
                {
                    return res;
                }

                //Wait for completion of all calculations on the GPU.
                error = OpenCL.Net.Cl.Finish(_cmdQueue);
                CheckErr(error, "Cl.Finish");


                //Read the processed image from GPU to RGBA
                error = OpenCL.Net.Cl.EnqueueReadImage(_cmdQueue, outputImage2DBuffer, OpenCL.Net.Bool.True,
                   originPtr, regionPtr, (IntPtr)0, (IntPtr)0, outputByteArray, 0, null, out clevent);
                if (!CheckErr(error, "Cl.clEnqueueReadImage"))
                {
                    return res;
                }

                /**************
                 * Generate the picture.
                 **************/

                result = new Bitmap(img.Width, img.Height, PixelFormat.Format32bppArgb);

                bitmapData = result.LockBits(new Rectangle(0, 0, img.Width, img.Height),
                                     ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);

                Marshal.Copy(outputByteArray, 0, bitmapData.Scan0, outputByteArray.Length);

                result.UnlockBits(bitmapData);

                res = true;
            }
            catch (Exception e)
            {
                LogData(DEFAULT_EXCEPTION_MSG + e.Message, "HilightColor", EG_Utility.LOG_LEVEL.WARNING);
                if (_throwException)
                    throw e;
            }
            finally
            {
                //Clean up memory
                OpenCL.Net.Cl.ReleaseKernel(kernel);

                OpenCL.Net.Cl.ReleaseEvent(clevent);
                if (inputImage2DBuffer != null)
                    OpenCL.Net.Cl.ReleaseMemObject(inputImage2DBuffer);
                if (outputImage2DBuffer != null)
                    OpenCL.Net.Cl.ReleaseMemObject(outputImage2DBuffer);
                if (inputArgumentBuffer != null)
                    OpenCL.Net.Cl.ReleaseMemObject(inputArgumentBuffer);
            }


            return res;
        }
        
    }
}
